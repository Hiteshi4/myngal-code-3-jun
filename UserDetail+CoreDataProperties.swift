//
//  UserDetail+CoreDataProperties.swift
//  
//
//  Created by Gulshan Agrawal on 27/02/20.
//
//

import Foundation
import CoreData


extension UserDetail {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UserDetail> {
        return NSFetchRequest<UserDetail>(entityName: "UserDetail")
    }

    @NSManaged public var id: String?
    @NSManaged public var userID: String?
    @NSManaged public var interest: String?
    @NSManaged public var gender: String?
    @NSManaged public var dob: String?
    @NSManaged public var token: String?
    @NSManaged public var city: String?
    @NSManaged public var lat: String?
    @NSManaged public var long: String?
    @NSManaged public var profession: String?
    @NSManaged public var commitment: String?
    @NSManaged public var address: String?
    @NSManaged public var aboutMe: String?
    @NSManaged public var height: String?
    @NSManaged public var school: String?
    @NSManaged public var education: String?
    @NSManaged public var jobTitle: String?
    @NSManaged public var company: String?
    @NSManaged public var coverImage: String?
    @NSManaged public var image1: String?
    @NSManaged public var image2: String?
    @NSManaged public var image3: String?
    @NSManaged public var image4: String?
    @NSManaged public var referralCode: String?
    @NSManaged public var referralPoints: String?
    @NSManaged public var referBy: String?
    @NSManaged public var viewLike: String?

}
