//
//  ExampleActivity.swift
//  ActionSheetPicker-3.0
//
//  Created by SONU on 08/02/20.
//

import Foundation

class ExampleActivity:UIActivity{
    
    var _activityTitle: String
    var _activityImage: UIImage?
    var activityItems = [Any]()
    var action: ([Any]) -> Void
    
    init(title: String, image: UIImage?, performAction: @escaping ([Any]) -> Void) {
        _activityTitle = title
        _activityImage = image
        action = performAction
        super.init()
    }
    override var activityTitle: String? {
        return _activityTitle
    }

    override var activityImage: UIImage? {
        return _activityImage
    }
    override var activityType: UIActivity.ActivityType? {
        return UIActivity.ActivityType(rawValue: "com.yoursite.yourapp.activity")
    }

    override class var activityCategory: UIActivity.Category {
        return .action
    }
     func share() {
            let textToShare = "Get ₹ 50 instantly on downloading the magicpin app! It helps you to discover incredible offers on restaurants, beauty & fashion outlets around you! No kidding, I've already saved Rs. 1053 through magicpin. Use link -http://magicpin.in/getapp/RJSK3223 or apply my referral code: RJSK3223"
          
            let customItem = ExampleActivity(title: "Tap me!", image: UIImage(named: "YourImageName")) { sharedItems in
                guard let sharedStrings = sharedItems as? [String] else { return }

                for string in sharedStrings {
                    print("Here's the string: \(string)")
                }
            }

            let items = ["Hello, custom activity!"]
            let ac = UIActivityViewController(activityItems: items, applicationActivities: [customItem])
            ac.excludedActivityTypes = [.postToFacebook]
        

    //        if let image :UIImage = UIImage(named: "logo")! {
    //
    //            let objectsToShare: [Any] = [image,textToShare]
    //            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
    //
    //         //   activityVC.popoverPresentationController?.sourceView = sender
    //            self.present(activityVC, animated: true, completion: nil)
    //        }
        }
}
