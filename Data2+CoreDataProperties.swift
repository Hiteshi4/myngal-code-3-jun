//
//  Data2+CoreDataProperties.swift
//  
//
//  Created by Gulshan Agrawal on 27/02/20.
//
//

import Foundation
import CoreData


extension Data2 {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Data2> {
        return NSFetchRequest<Data2>(entityName: "Data2")
    }

    @NSManaged public var id: Int32
    @NSManaged public var name: String?
    @NSManaged public var email: String?
    @NSManaged public var number: String?
    @NSManaged public var profileImage: String?

}
