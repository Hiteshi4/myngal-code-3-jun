//
//  AppSetting+CoreDataProperties.swift
//  
//
//  Created by Gulshan Agrawal on 05/03/20.
//
//

import Foundation
import CoreData


extension AppSetting {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<AppSetting> {
        return NSFetchRequest<AppSetting>(entityName: "AppSetting")
    }

    @NSManaged public var activeDate: String?
    @NSManaged public var adminMessage: String?
    @NSManaged public var chatMessageReceived: String?
    @NSManaged public var hide_my_age: String?
    @NSManaged public var id: Int32
    @NSManaged public var makeDistanceInvisible: String?
    @NSManaged public var newMatches: String?
    @NSManaged public var onBeingLiked: String?
    @NSManaged public var onBeingShortlisted: String?
    @NSManaged public var user_id: String?

}
