//
//  FilterData+CoreDataProperties.swift
//  
//
//  Created by Gulshan Agrawal on 27/02/20.
//
//

import Foundation
import CoreData


extension FilterData {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<FilterData> {
        return NSFetchRequest<FilterData>(entityName: "FilterData")
    }

    @NSManaged public var id: String?
    @NSManaged public var userID: String?
    @NSManaged public var lat: String?
    @NSManaged public var long: String?
    @NSManaged public var city: String?
    @NSManaged public var gender: String?
    @NSManaged public var maxDistance: String?
    @NSManaged public var ageRangeStart: String?
    @NSManaged public var ageRangeEnd: String?
    @NSManaged public var heightRangeStart: String?
    @NSManaged public var heightRangeEnd: String?
    @NSManaged public var status: String?
    @NSManaged public var trash: String?
    @NSManaged public var commitment: String?
    @NSManaged public var profession: String?

}
