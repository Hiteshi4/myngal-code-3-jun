//
//  LoginResponse.swift
//  Myngal
//
//  Created by SONU on 07/01/20.
//  Copyright © 2020 SONU. All rights reserved.
//



import Foundation

struct LoginRespnse  {
    var status : String?
    var message : String?
    var data : Data22?
    var user_data : User_data?
    var filter_data : Filter_data?
    var app_setting_data : App_setting_data?
    
    

     init(dictionary: NSDictionary) {

        
     
        status = dictionary["status"] as? String ?? ""
        message = dictionary["message"] as? String ?? ""
        data = Data22(dictionary: ((dictionary["data"] as? NSDictionary as? [String : Any] ?? [:]) ))
        user_data = User_data(dictionary: ((dictionary["user_data"] as? NSDictionary as? [String : Any] ?? [:]) ))
        filter_data = Filter_data(dictionary: ((dictionary["filter_data"] as? NSDictionary  as? [String : Any] ?? [:]) ))
        app_setting_data = App_setting_data(dictionary: ((dictionary["app_setting_data"] as? NSDictionary as? [String : Any] ?? [:]) ))
        
    }
}

struct App_setting_data  {
    var id : String
    var user_id : String?
    var hide_my_age : String?
    var active_date : String?
    var make_distance_invisible : String?
    var chat_message_received : String?
    var new_matches : String?
    var on_being_shortlisted : String?
    var on_being_liked : String?
    var admin_message : String?
   

    init(dictionary: [String:Any]) {
     
    id = dictionary["id"] as? String ?? ""
    user_id = dictionary["user_id"] as? String ?? ""
    hide_my_age = dictionary["hide_my_age"] as? String ?? ""
    active_date = dictionary["active_date"] as? String ?? ""
    make_distance_invisible = dictionary["make_distance_invisible"] as? String ?? ""
         chat_message_received = dictionary["chat_message_received"] as? String ?? ""
         new_matches = dictionary["new_matches"] as? String ?? ""
         on_being_shortlisted = dictionary["on_being_shortlisted"] as? String ?? ""
          on_being_liked = dictionary["on_being_liked"] as? String ?? ""
        admin_message = dictionary["admin_message"] as? String ?? ""
        
    }
}

struct Data22  {
    var id : Int
    var name : String?
    var email : String?
    var contact_number : String?
    var profile_image : String?
    
       init(dictionary: [String:Any]) {
        
       id = dictionary["id"] as? Int ?? 0
       name = dictionary["name"] as? String ?? ""
       email = dictionary["email"] as? String ?? ""
       contact_number = dictionary["contact_number"] as? String ?? ""
       profile_image = dictionary["profile_image"] as? String ?? ""
        
   }
   }

    




struct Filter_data  {
    var id : String?
    var user_id : String?
    var latitude : String?
    var longitude : String?
    var city : String?
    var gender : String?
    var max_distance : String?
    var age_range_start : String?
    var age_range_end : String?
    var height_range_start : String?
    var height_range_end : String?
    var status : String?
    var trash : String?
   
 init(dictionary: [String:Any]) {
     
    id = dictionary["id"] as? String ?? ""
    user_id = dictionary["user_id"] as? String ?? ""
    max_distance = dictionary["max_distance"] as? String ?? ""
    gender = dictionary["gender"] as? String ?? ""
    age_range_start = dictionary["age_range_start"] as? String ?? ""
    
    age_range_end = dictionary["age_range_end"] as? String ?? ""
    height_range_start = dictionary["height_range_start"] as? String ?? ""
    height_range_end = dictionary["height_range_end"] as? String ?? ""
    status = dictionary["status"] as? String ?? ""
    trash = dictionary["trash"] as? String ?? ""
    latitude = dictionary["latitude"] as? String ?? ""
    longitude = dictionary["longitude"] as? String ?? ""
    city = dictionary["city"] as? String ?? ""
}
    }

struct User_data {
    var id : String
    var user_id : String?
    var interest : String?
    var gender : String?
    var dob : String?
    var token : String?
    var city : String?
    var latitude : String?
    var longitude : String?
    var profession : String?
    var commitment_type : String?
    var address : String?
    var aboutme : String?
    var height : String?
    var school : String?
    var education : String?
    var job_title : String?
    var company : String?
    var cover_image : String?
    var image1 : String?
    var image2 : String?
    var image3 : String?
    var image4 : String?
    var referal_code : String?
    var referal_points : String?
    var refer_by : String?
    var view_like : String?
//    let created_at : String?
//    let updated_at : String?

    init(dictionary: [String:Any]) {
     
    id = dictionary["id"] as? String ?? ""
    user_id = dictionary["user_id"] as? String ?? ""
    interest = dictionary["interest"] as? String ?? ""
    gender = dictionary["gender"] as? String ?? ""
    dob = dictionary["dob"] as? String ?? ""
        
        token = dictionary["token"] as? String ?? ""
        city = dictionary["city"] as? String ?? ""
       // interest = dictionary["interest"] as? String ?? ""
        latitude = dictionary["latitude"] as? String ?? ""
        longitude = dictionary["longitude"] as? String ?? ""
profession = dictionary["token"] as? String ?? ""
        
         commitment_type = dictionary["commitment_type"] as? String ?? ""
                aboutme = dictionary["aboutme"] as? String ?? ""
                height = dictionary["height"] as? String ?? ""
                school = dictionary["school"] as? String ?? ""
                education = dictionary["education"] as? String ?? ""
        job_title = dictionary["job_title"] as? String ?? ""
        address = dictionary["address"] as? String ?? ""
        
         company = dictionary["company"] as? String ?? ""
                cover_image = dictionary["cover_image"] as? String ?? ""
                image1 = dictionary["image1"] as? String ?? ""
                image2 = dictionary["image2"] as? String ?? ""
                image3 = dictionary["image3"] as? String ?? ""
        image4 = dictionary["image4"] as? String ?? ""
        
        referal_code = dictionary["referal_code"] as? String ?? ""
                referal_points = dictionary["referal_points"] as? String ?? ""
                refer_by = dictionary["refer_by"] as? String ?? ""
                view_like = dictionary["view_like"] as? String ?? ""
              
                
}
}



