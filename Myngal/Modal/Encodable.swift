//
//  Encodable.swift
//  Myngal
//
//  Created by SONU on 07/01/20.
//  Copyright © 2020 SONU. All rights reserved.
//

import Foundation

struct LoginParameter: Encodable {
    let contact_number: String
    //let email: String
}


struct ForgotPasswordParameter: Encodable {
    let email: String
}

struct SignUpParameter: Encodable {
    let contact_number: String
    let email: String
    let name: String
    let interest: String
    let gender: String
    let dob: String
    let profession: String
    let commitment_type: String
    let profile_image: String
    
    
   
    
//    private enum CodingKeys: String, CodingKey {
//        case email, password, country
//        case clinic = "school_name", learnerCount = "no_learner", mobileNumber = "mobile_no"
//    }
}
