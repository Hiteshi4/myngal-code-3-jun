//
//  ChatModal.swift
//  Myngal
//
//  Created by Gulshan Agrawal on 15/02/20.
//  Copyright © 2020 SONU. All rights reserved.
//

import Foundation
struct chat {
    var image:String!
    var message:String!
    var time:String!
    var type:String!
    var sender:String!
    
    
    init(chatDic:NSDictionary) {
        image = chatDic["image"] as! String
        message = chatDic["message"] as! String
        time = chatDic["time"] as! String
        type = chatDic["type"] as! String
        sender = chatDic["sander"] as! String
    }
}
