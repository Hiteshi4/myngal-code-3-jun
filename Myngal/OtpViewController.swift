//
//  OtpViewController.swift
//  Myngal
//
//  Created by SONU on 16/11/19.
//  Copyright © 2019 SONU. All rights reserved.
//

import UIKit
import Alamofire

import Foundation
let userdefaults = UserDefaults.standard
class OtpViewController: BaseViewController,UITextFieldDelegate{
    @IBOutlet weak var mobileNumber: UILabel!
    let common = Common()
    
    @IBOutlet weak var text1: UITextField!
    @IBOutlet weak var text2: UITextField!
    @IBOutlet weak var text3: UITextField!
    @IBOutlet weak var text4: UITextField!
    @IBOutlet weak var text5: UITextField!
    @IBOutlet weak var text6: UITextField!
    @IBOutlet weak var OtpTextField: UITextField!
    @IBOutlet weak var getOtpBtn: GradientButton!
    
    var propertyArray:LoginRespnse?
    var dicResponse      = Dictionary<String,Any>()
    let apiMapper = APIMapper()
     private var lesson:User_data!
    var mobilenumber = ""
    let singleTon  = UserParams()
    var dicParent            :Dictionary<String, Any>?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //getOtpBtn.showsTouchWhenHighlighted = true
        
       // OtpTextField.text = mobilenumber
        mobileNumber.text = "+91 \(mobilenumber)"
        print("dadadadadadaada\(mobilenumber)")
 navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view.
    }
 
    override func viewDidAppear(_ animated: Bool) {
    text1.becomeFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        text1.textContentType = .oneTimeCode
        text2.textContentType = .oneTimeCode
        text3.textContentType = .oneTimeCode
        text4.textContentType = .oneTimeCode
        text5.textContentType = .oneTimeCode
        text6.textContentType = .oneTimeCode
       
          NotificationCenter.default.addObserver(
              self,
              selector: #selector(keyboardWillShow(_:)),
              name: UIResponder.keyboardWillShowNotification,
              object: nil
          )
      }
      
      override func viewWillDisappear(_ animated: Bool) {
          NotificationCenter.default.removeObserver(self)
      }
      
      
      //MARK: Keyboard Notifications
      
      @objc func keyboardWillShow(_ notification: Notification) {
          
          if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
              
              let keyboardRectangle = keyboardFrame.cgRectValue
              let keyboardHeight = keyboardRectangle.height
              
             // constraintYNextButton.constant = keyboardHeight+20
              
              UIView.animate(withDuration: 0.3) {
                  self.view.layoutIfNeeded()
              }
              
          }
          
      }
      
      //MARK: TextField Delegate
      func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
          
          //        let inverseSet = NSCharacterSet(charactersIn:DIGITS).inverted
          //        let components = string.components(separatedBy: inverseSet)
          //        let filtered = components.joined(separator: "")
          //        return string == filtered
          
          let isBackSpace         = strcmp(string, "\\b")
        let currruntResponder   = self.view.curruntFirstResponder() as! UITextField
          
          if string == " "{
              return false
          }
          
          if (isBackSpace == -92)
          {
              if let nextResponder: UITextField = self.view.viewWithTag(currruntResponder.tag-1) as? UITextField
              {
                  textField.text = string
                  nextResponder.becomeFirstResponder()
                  return false
              }
              return true
          }
          
          
          if string.count > 0{
              
              if textField.text?.count == 0 || textField.text?.count == 1{
                  textField.text = ""
                  textField.text = string
              }
              if let nextResponder: UITextField = self.view.viewWithTag(currruntResponder.tag+1) as? UITextField {
                  nextResponder.becomeFirstResponder()
              }
              return false
          }
          
          if (textField.text?.count)! >= 2 || ((textField.text?.count)! >= 1 && isBackSpace != -92 ) {
              return false
          }
          
          return true
      }
      
      
      func textFieldShouldReturn(_ textField: UITextField) -> Bool {
          
          self.view.endEditing(true)
          return true
      }
    func saveUserDetailInDefaults()  {
        //let userDefault = UserDefaults.standard
        let encodeData = NSKeyedArchiver.archivedData(withRootObject: self)
        userdefaults.set(encodeData, forKey: "data")
        userdefaults.synchronize()
    }
    func saveUserData(userDetails: NSDictionary) {



      
        if let userDetailsBody = userDetails.value(forKey: "data") as? NSDictionary {

            common.setName(name:userDetailsBody.value(forKey: "name") as? String ?? "")
            common.setEmail(email: userDetailsBody.value(forKey: "email") as? String ?? "")
            // default type = 4 for customer
          
            common.setUserId(userId: userDetailsBody.value(forKey: "id") as? Int ?? 0)

            
     }
    }
    
    func loginApiCall(){
        self.showActivity()
        let singleTon = UserParams()
            
               let Email = singleTon.sharedInstance.userEmail
               let number = singleTon.sharedInstance.contact_No
    
        let param : [String:Any] = [
            "contact_number"  : number  as Any,
            "email"   : Email  as Any

        ]
        
        apiMapper.userLogin(param, success: { (successDict) in
           var view: HomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeVc") as! HomeViewController
            
        self.saveUserData(userDetails: successDict)
           
            if let dictionary = successDict as? [String : Any]{
               
              print(dictionary)
               
                var propertyList = LoginRespnse(dictionary: dictionary as NSDictionary)
                self.propertyArray = propertyList
                   
                if  propertyList.status == "true"{
                          self.hideActivity()
                    self.showToast(message: propertyList.message ?? "")
                    view.userInfo = self.propertyArray
              if CoreDataManager.shared.entityIsEmpty(entity: "UserDetail")
              {
                CoreDataManager.shared.saveDatainUserDetail(data:  dictionary["user_data"] as! NSDictionary)
                                   }
                    
                    if CoreDataManager.shared.entityIsEmpty(entity: "FilterData")
                    {
                        CoreDataManager.shared.saveDatainFilterData(data: dictionary["filter_data"] as! NSDictionary)
                    }
                    if CoreDataManager.shared.entityIsEmpty(entity: "Data2")
                    {
                        CoreDataManager.shared.saveDatainData2(data:  dictionary["data"] as! NSDictionary)
                       
                    }
                    if CoreDataManager.shared.entityIsEmpty(entity: "AppSetting")
                    {
                        CoreDataManager.shared.saveDatainAppsetting(data:  dictionary["app_setting_data"] as! NSDictionary)
                       
                    }
                    
                    
                
                    Router.loadMainInterface()
                    
                    }else{
                        self.hideActivity()
                         self.navigateTo(.Main, .SignUp){ (vc) in
                  //  guard let viewProfileVC = vc as? SignUpViewController else{return}
                            
                                                          }

                    }
                }
                
            print("User Defaults Id...\(self.common.getUserId())")
            print("User Defaults Name...\(self.common.getName())")
            print("User Defaults Name...\(self.common.getEmail())")
          
            
            
            }) { (errorDict) in
                self.hideActivity()
                self.showAlert(errorDict)
               // print(errorDict)
        }
        
}
        
    

    
    func VerifyOtp(){
               self.showActivity()
                      //http://api.msg91.com/api/sendotp.php?authkey=306149Ao9WPWLJ5de0b501&mobile=919999999990&message=Your%20otp%20is%202786&sender=senderid&otp=2786
        
        
                let number =  mobilenumber
                print(number)
               // let otpp = text1.text+text2.text
                let otp = "\("\(text1.text!)\(text2.text!)\(text3.text!)\(text4.text!)\(text5.text!)\(text6.text!)")"
                print(otp)
                  let updateParticipantURL = URL(string: "http://api.msg91.com/api/verifyRequestOTP.php?authkey=306149Ao9WPWLJ5de0b501&mobile=\(number)&otp=\(otp)")
                print(updateParticipantURL)
        
                                   do {
        
                                    let jsonData = try JSONSerialization.data(withJSONObject: [:], options: .prettyPrinted)
        
                                var request = URLRequest(url: updateParticipantURL!)
        
                                      request.httpMethod = HTTPMethod.get.rawValue
        
        
                                      AF.request(request).responseJSON { response in
        
        
                                       self.hideActivity()
                                      print()
                                          switch response.result {
        
                                          case .success(let value ) :
                                            let json = response.value as? [String:Any] ?? [:]
                                            let result = json["message"] as? String ?? ""
                                            if result == "otp_verified"{
                                                self.loginApiCall()
        
                                            }else{
//                                                self.navigateTo(.Main, .SignUp){ (vc) in
//                                                                 }
                                                self.showAlert(result)
                                            }
        
                                            print("Response====----14\(response.value)")
        
        
                             case .failure(let error): break
        
                             }
                         }
                      }
                      catch {
                          print(error.localizedDescription)
                      }


    }
    
    //MARK:- Otp button action
    
    @IBAction func otpBTnPressed(_ sender: UIButton) {
        
        sender.buttonEffect()
        //let strOTP : String = "\(text1.text!)\(text2.text!)\(text3.text!)\(text4.text!)"
        
        if text1.text == "" || text2.text == "" || text3.text == ""  || text4.text == "" || text5.text == ""  || text6.text == "" {
            self.showAlert("Please enter 6 digit otp")
        }
        else{
             VerifyOtp()
        }
        
    }
       
    @IBAction func backClicked(_ sender: UIButton) {
        sender.pulsate(sender: sender)
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func resendOtp(_ sender: UIButton) {
        //http://api.msg91.com/api/retryotp.php?authkey=YourAuthKey&mobile=919999999990&retrytype=voice
        
        
         self.showActivity()
                             //http://api.msg91.com/api/sendotp.php?authkey=306149Ao9WPWLJ5de0b501&mobile=919999999990&message=Your%20otp%20is%202786&sender=senderid&otp=2786
                let number = mobilenumber



        let updateParticipantURL = URL(string: "http://api.msg91.com/api/retryotp.php?authkey=306149Ao9WPWLJ5de0b501&mobile=\(number)&retrytype=text")
                      print(updateParticipantURL)
                
                             do {
                          var request = URLRequest(url: updateParticipantURL!)
                            //   request.httpBody = jsonData
                                request.httpMethod = HTTPMethod.get.rawValue
                             //   request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                                 // request.setValue("Token 2034faf7a7e76fe8b3581814422b18f52205ef0b", forHTTPHeaderField: "Authorization")z
                                let jsonData = try JSONSerialization.data(withJSONObject: [:], options: .prettyPrinted)
                                AF.request(request).responseJSON { response in
                                  // Loader.shared.hideHUDLoader()
                                 self.hideActivity()

                                    switch response.result {
                                    case .success(let _):
                                        print ("finish")
                                        print(response)
                                       let json = response.value as? [String:Any] ?? [:]
                                        let result = json["message"] as? String ?? ""
                                                                                 
                                              
                                      self.showAlert(result)
                                       

        //
                                    case .failure(let error):
                                         let alert = UIAlertController(title: "", message: "Update Successfull", preferredStyle: UIAlertController.Style.alert)
                                                                                  alert.addAction(UIAlertAction(title: "Myngal", style: UIAlertAction.Style.default, handler: nil))
                                                                                  self.present(alert, animated: true, completion: nil)
                                    }
                                }
                             }
                             catch {
                                 print(error.localizedDescription)
                             }
        
        
    }
    
    
}
    






extension UIView
{
    func curruntFirstResponder() -> UIResponder? {
        
        if self.isFirstResponder {
            return self
        }
        
        for view in self.subviews {
            if let responder  = view.curruntFirstResponder() {
                return responder
            }
        }
        return nil;
    }
}

extension UIButton{
    func buttonEffect(){
        self.transform = CGAffineTransform(scaleX: 0.4, y: 0.4)

              UIView.animate(withDuration: 2.0,
                                         delay: 0,
                                         usingSpringWithDamping: CGFloat(0.20),
                                         initialSpringVelocity: CGFloat(2.0),
                                         options: UIView.AnimationOptions.allowUserInteraction,
                                         animations: {
                                          self.transform = CGAffineTransform.identity
                  },
                                         completion: { Void in()  }
              )
    }
}
extension UIView{
    func ViewEffect(){
        self.transform = CGAffineTransform(scaleX: 0.4, y: 0.4)

              UIView.animate(withDuration: 2.0,
                                         delay: 0,
                                         usingSpringWithDamping: CGFloat(0.20),
                                         initialSpringVelocity: CGFloat(2.0),
                                         options: UIView.AnimationOptions.allowUserInteraction,
                                         animations: {
                                          self.transform = CGAffineTransform.identity
                  },
                                         completion: { Void in()  }
              )
    }
}

extension UIButton{
    func pulsate(sender:UIButton){
  sender.layer.cornerRadius = 20
     let colorAnimation = CABasicAnimation(keyPath: "backgroundColor")
     colorAnimation.fromValue = UIColor.lightGray.cgColor
     
     
       colorAnimation.duration = 5  // animation duration
        colorAnimation.autoreverses = true // optional in my case
     colorAnimation.repeatCount = FLT_MAX // optional in my case
   sender.layer.add(colorAnimation, forKey: "ColorPulse")
   }
   func flash() {
   let flash = CABasicAnimation(keyPath: "opacity")
   flash.duration = 0.3
   flash.fromValue = 1
   flash.toValue = 0.1
   flash.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
   flash.autoreverses = true
   flash.repeatCount = 2
   layer.add(flash, forKey: nil)
   }
    
    func tapEffect(){
      //  sender.layer.cornerRadius = 20
               let colorAnimation = CABasicAnimation(keyPath: "backgroundColor")
               colorAnimation.fromValue = UIColor.lightGray.cgColor
               
               
                 colorAnimation.duration = 5  // animation duration
                  colorAnimation.autoreverses = true // optional in my case
               colorAnimation.repeatCount = FLT_MAX // optional in my case
             //  sender.layer.add(colorAnimation, forKey: "ColorPulse")
    }
}





extension UserDefaults {
    open func setStruct<T: Codable>(_ value: T?, forKey defaultName: String){
        let data = try? JSONEncoder().encode(value)
        set(data, forKey: defaultName)
    }
    
    open func structData<T>(_ type: T.Type, forKey defaultName: String) -> T? where T : Decodable {
        guard let encodedData = data(forKey: defaultName) else {
            return nil
        }
        
        return try! JSONDecoder().decode(type, from: encodedData)
    }
    
    open func setStructArray<T: Codable>(_ value: [T], forKey defaultName: String){
        let data = value.map { try? JSONEncoder().encode($0) }
        
        set(data, forKey: defaultName)
    }
    
    open func structArrayData<T>(_ type: T.Type, forKey defaultName: String) -> [T] where T : Decodable {
        guard let encodedData = array(forKey: defaultName) as? [Data] else {
            return []
        }
        
        return encodedData.map { try! JSONDecoder().decode(type, from: $0) }
    }
}
