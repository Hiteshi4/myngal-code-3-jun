import Foundation
import UIKit

enum ViewControllerIdenfier:String{
    case ProfessionVc,HomeVc,MyProfileVC,CommitmentVc,viewProfile1,TutorSignUp,LoginVc,OtpVc,SignUp,IntrestVc,GenderVc,UplodProfile,PrivacyVc,SettingVc,MessageVc,ChatViewController,SubscriptionPopUpVc,HelpVc,OtherUserProfileVC,LikeVc,BlockedUsersVc,SavedUserVc,FilterVc,InviteVc,editProfileVc,MatchesVc,ImageSliderVc,LocationPermissionVc,WalletVc,PaymentVc
    
}

extension UIViewController{
    
    func navigateTo(_ storyboard:AppStoryBoard, _ identifier:ViewControllerIdenfier, _ callback:(_ viewController:UIViewController)->Void){
        
        let viewController = storyboard.instance.instantiateViewController(withIdentifier: identifier.rawValue)
        
        // callback to set any data for this controller
        
        callback(viewController)
        
        navigationController!.pushViewController(viewController, animated: true)
         
    }
    func PresentTo(_ storyboard:AppStoryBoard, _ identifier:ViewControllerIdenfier, _ callback:(_ viewController:UIViewController)->Void){
           
           let viewController = storyboard.instance.instantiateViewController(withIdentifier: identifier.rawValue)
           
           // callback to set any data for this controller
        viewController.modalPresentationStyle = .overFullScreen
        viewController.modalTransitionStyle = .crossDissolve
        
           callback(viewController)
           
          // self.navigationController!.pushViewController(viewController, animated: true)
        self.present(viewController, animated: true, completion: nil)
       }
    
}
enum AppStoryBoard:String {
    case Main,SideMenu,Login_Signup
    var instance:UIStoryboard{
        return UIStoryboard(name: self.rawValue, bundle: nil)
    }
}
