//
//  MenuViewController.swift
//  GuillotineMenuExample
//
//  Created by Maksym Lazebnyi on 10/8/15.
//  Copyright © 2015 Yalantis. All rights reserved.


import UIKit
import GuillotineMenu

class MenuViewController: BaseViewController, GuillotineMenu {
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var headerView: UIView!
  
    var ResponseData: Data2?
  
    
    @IBOutlet weak var TblView: UITableView!
    var sideMenuList = SideMenuTableViewList1()
    var dismissButton: UIButton?
    var titleLabel: UILabel?
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
       

        navigationController?.navigationBar.isHidden = false
        let moreButton = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 23))
        moreButton.setBackgroundImage(UIImage(named: "ic_menu"), for: .normal)
        moreButton.addTarget(self, action: #selector(dismissButtonTapped), for: .touchUpInside)
        moreButton.tintColor = .red
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: moreButton)
        TblView.estimatedRowHeight = 60
        
        TblView.rowHeight = UITableView.automaticDimension
        
        
        TblView.register(UINib(nibName: "SideMenuTableViewCell", bundle: nil), forCellReuseIdentifier: "SideMenuTableViewCell")
        
        dismissButton = {
            
            let button = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 23))
            button.setBackgroundImage(UIImage(named: "ic_menu"), for: .normal)
            button.addTarget(self, action: #selector(dismissButtonTapped), for: .touchUpInside)
            button.tintColor = .red
            return button
        }()
        
        
        titleLabel = {
            let label = UILabel()
            label.numberOfLines = 1;
            label.text = ""
            label.font = UIFont.boldSystemFont(ofSize: 17)
            label.textColor = UIColor.black
            label.sizeToFit()
            return label
        }()
    }
    
    
    
    //to show new view controller
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let fetchdata = CoreDataManager.shared.fetchDataFromData2()
               for item in fetchdata!
               {
                   self.ResponseData = item
                   
               }
           let username = ResponseData?.name
           let profile = ResponseData?.profileImage ?? ""
           userName.text = username ?? ""
               profileImage.setImage(from: AppUrls.imagebaseUrl+"\(profile)", placeholderImage: #imageLiteral(resourceName: "male_selected"))
        print("Menu: viewWillAppear")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("Menu: viewDidAppear")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("Menu: viewWillDisappear")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("Menu: viewDidDisappear")
    }
    
    @objc func dismissButtonTapped(_ sender: UIButton) {
       
        dismiss(animated: true, completion: nil)
      
    }
    // MARK:- Table Delegates
}
extension MenuViewController: GuillotineAnimationDelegate {
    func animatorDidFinishPresentation(_ animator: GuillotineTransitionAnimation) {
        
        
        // animator.supportView?.isHidden = true
    }
    
    func animatorDidFinishDismissal(_ animator: GuillotineTransitionAnimation) {
        print("menuDidFinishDismissal")
    }
    
    func animatorWillStartPresentation(_ animator: GuillotineTransitionAnimation) {
        print("willStartPresentation")
    }
    
    func animatorWillStartDismissal(_ animator: GuillotineTransitionAnimation) {
        print("willStartDismissal")
    }
}

extension MenuViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sideMenuList.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTableViewCell") as! SideMenuTableViewCell
        cell.selectionStyle = .none
        cell.MenuIcon.tintColor = .black
        let imageView = cell.MenuIcon!
        let labelTitle = cell.Menutitle!
        //let circleImage = cell.viewWithTag(4) as! UIImageView
        //                  let backGroundView = cell.viewWithTag(3)!
        //                  backGroundView.backgroundColor = .clear
        
        let item = sideMenuList.items[indexPath.row]
        
        imageView.image = item.image
        labelTitle.text = item.title
        
        //              if indexPath.row == 1 {
        //                  circleImage.isHidden = isHideCircle
        //              } else {
        //                  circleImage.isHidden = true
        //              }
        
        guard let lastSelectedIndex = sideMenuList.selectedIndex else {
            return cell
        }
        
        //                  if indexPath.row == lastSelectedIndex.row{
        //                    backGroundView.backgroundColor = UIColor.red
        //                  }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        sideMenuList.selectedIndex = indexPath
        tableView.reloadData()
        switch indexPath.row {
        case 0:
            var MyProfileVC: MyProfileViewController = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileViewController
           // MyProfileVC.ResponseData = self.ResponseData
            MyProfileVC.modalPresentationStyle = .overFullScreen
            MyProfileVC.modalTransitionStyle = .crossDissolve
            present(MyProfileVC, animated: true, completion: nil)
            //                        PresentTo(.Main, .MyProfileVC){ (vc) in
            //
            //                        }
            
        case 1:
            PresentTo(.Main, .MessageVc){ (vc) in

                         }
            
            
        case 2:
            PresentTo(.Main, .LikeVc){ (vc) in
                
            }
            
        case 3:
            PresentTo(.Main, .BlockedUsersVc){ (vc) in
                
            }
            
            
        case 4:
            PresentTo(.Main, .SavedUserVc){ (vc) in
                
            }
            
            
        case 5:
            PresentTo(.Main, .MatchesVc){ (vc) in
                
            }
            
            
        case 6:
            
            var FilterVC: FilterViewController = self.storyboard?.instantiateViewController(withIdentifier: "FilterVc") as! FilterViewController
           // FilterVC.ResponseData = self.ResponseData?.filter_data
            FilterVC.modalPresentationStyle = .overFullScreen
            FilterVC.modalTransitionStyle = .crossDissolve
            present(FilterVC, animated: true, completion: nil)
            //                        PresentTo(.Main, .FilterVc){ (vc) in
            //
            //                                                                     }
            
            
            
        case 7:
            
            PresentTo(.Main, .InviteVc){ (vc) in
                
            }
            
            
        case 8 :
            let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SubscriptionPopUpVc") as! SubscriptionPopUpVc
                      viewController.modalPresentationStyle = .overCurrentContext
                      viewController.modalTransitionStyle = .crossDissolve
                      self.present(viewController, animated: true, completion: nil)
         
        //
        case 9 :
          
            PresentTo(.Main, .PrivacyVc){ (vc) in
                         
                     }
            
        case 10 :
            PresentTo(.Main, .HelpVc){ (vc) in
                
            }
            
            
            
        case 11 :
            
            var FilterVC: SettingViewController = self.storyboard?.instantiateViewController(withIdentifier: "SettingVc") as! SettingViewController
            //FilterVC.ResponseData = self.ResponseData?.app_setting_data
            FilterVC.modalPresentationStyle = .overFullScreen
            FilterVC.modalTransitionStyle = .crossDissolve
            present(FilterVC, animated: true, completion: nil)
            
            //                        PresentTo(.Main, .SettingVc){ (vc) in
            //
        //                        }
        case 12 :
            showLogoutAlert()
            
        default:
            break
        }
        
    }
    
    func showLogoutAlert(){
        
        
        
        self.showAlert("Logout?", "Are you sure to logout?", .alert, ["Logout"]) { (tappedIndex) in
            if tappedIndex == 0 {
                
                
             
                CoreDataManager.shared.deleteDataFromData2()
                CoreDataManager.shared.deleteDataFromFilterData()
                CoreDataManager.shared.deleteDataFromUserDetail()
                CoreDataManager.shared.deleteDataFromAppSetting()
                Router.loadOnboardingInterface()
                
              
                
                
                
            }
        }
        
    }
    
    
}



