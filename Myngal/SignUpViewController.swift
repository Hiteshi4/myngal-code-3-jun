//
//  SignUpViewController.swift
//  Myngal
//
//  Created by SONU on 12/11/19.
//  Copyright © 2019 SONU. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0


class SignUpViewController: BaseViewController {
    @IBOutlet weak var reffralImg: UIImageView!
    
    @IBOutlet weak var reffralCodetxtField: UITextField!
    @IBOutlet weak var UsernameTextfield: UITextField!
    @IBOutlet weak var phoneImage: UIImageView!
    @IBOutlet weak var UserEmailTextfield: UITextField!
    @IBOutlet weak var emailImage: UIImageView!
    @IBOutlet weak var calendraImage: UIImageView!
    @IBOutlet weak var nextBtn: GradientButton!
    let common = Common()
    var dicFacebook                                     = Dictionary<String,Any>()
    let singleTon  = UserParams()
    
    @IBOutlet weak var DOBtextfield: UITextField!
    var selectedStartDate = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpData()
        TextFieldtintChange()
        nextBtn.isEnabled = true
        
    }
    
    
    
    func getDateStringForDate(date : Date, andDateFormat dateformatter: String) -> String{
        let formatter = DateFormatter()
        formatter.dateFormat = dateformatter
        //print(formatter.string(from: date))
        return formatter.string(from: date)
        
    }
    func TextFieldtintChange(){
        UsernameTextfield.addTarget(self, action: #selector(nameTint(_:)), for: .editingChanged)
        UserEmailTextfield.addTarget(self, action: #selector(emaliTint(_:)), for: .editingChanged)
        DOBtextfield.addTarget(self, action: #selector(selectDob(_sender:)), for: .editingDidBegin)
        reffralCodetxtField.addTarget(self, action: #selector(reffralTint(_:)), for: .editingChanged)
        
    }
    
    @objc func nameTint(_ sender: UIButton){
        
        if UsernameTextfield.text!.count > 1{
            phoneImage.tintColor = #colorLiteral(red: 1, green: 0.07843137255, blue: 0, alpha: 1)
        }else{
            phoneImage.tintColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        }
        //        else
        //        else
        
    }
    
    @objc func emaliTint(_ sender: UIButton){
        if common.isValidEmail(emailString: UserEmailTextfield.text!){
            emailImage.tintColor = #colorLiteral(red: 1, green: 0.07843137255, blue: 0, alpha: 1)
        }else{
            emailImage.tintColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        }
        
    }
    func dobtint(){
        if DOBtextfield.text!.count > 1{
            calendraImage.tintColor = #colorLiteral(red: 1, green: 0.07843137255, blue: 0, alpha: 1)
        }else{
            calendraImage.tintColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        }
    }
    @objc func reffralTint(_ sender: UIButton){
        if reffralCodetxtField.text!.count > 1{
            reffralImg.tintColor = #colorLiteral(red: 1, green: 0.07843137255, blue: 0, alpha: 1)
        }else{
            reffralImg.tintColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        }
    }
    
    func setUpData(){
        
        self.reffralCodetxtField.text = singleTon.sharedInstance.ReffralCode
        if singleTon.sharedInstance.username != ""{
            self.UsernameTextfield.text = singleTon.sharedInstance.username as? String ?? ""
            self.UsernameTextfield.isEnabled = false
            self.phoneImage.tintColor = #colorLiteral(red: 1, green: 0.07843137255, blue: 0, alpha: 1)
            
        } else{
            self.UsernameTextfield.isEnabled = true
        }
        if singleTon.sharedInstance.userEmail != ""{
            self.UserEmailTextfield.text =  singleTon.sharedInstance.userEmail as? String ?? ""
            self.UserEmailTextfield.isEnabled = false
            self.emailImage.tintColor = #colorLiteral(red: 1, green: 0.07843137255, blue: 0, alpha: 1)
            
        }else{
            self.UserEmailTextfield.isEnabled = true
        }
        if singleTon.sharedInstance.dob != ""{
            self.DOBtextfield.text =  singleTon.sharedInstance.dob as? String ?? ""
            self.DOBtextfield.isEnabled = false
            self.calendraImage.tintColor = #colorLiteral(red: 1, green: 0.07843137255, blue: 0, alpha: 1)
            
        }else{
            self.DOBtextfield.isEnabled = true
        }
    }
    
    @objc func selectDob(_sender:UITextField) {
        self.view.endEditing(true)
        ActionSheetDatePicker.show(withTitle: "Select DOB ", datePickerMode: .date, selectedDate: Date(), minimumDate: nil, maximumDate: nil, doneBlock: { (picker, date, selectedValue) in
            
            self.DOBtextfield.text = self.getDateStringForDate(date: date as! Date, andDateFormat: DateConstants.dateFormat)
            
            self.calendraImage.tintColor = #colorLiteral(red: 1, green: 0.07843137255, blue: 0, alpha: 1)
            
            self.selectedStartDate = self.DOBtextfield.text!
            self.ageLimit(age: self.DOBtextfield.text!)
            self.view.endEditing(false)
            print("Date Select")
        }, cancel: { (picker) in
        }, origin: DOBtextfield)
    }
    
    func passData(){
        let singleTon = UserParams()
        singleTon.sharedInstance.username =  UsernameTextfield.text!
        singleTon.sharedInstance.userEmail = UserEmailTextfield.text!
        singleTon.sharedInstance.dob = DOBtextfield.text!
        if singleTon.sharedInstance.Selctedgender == "" {
            
            navigateTo(.Main, .GenderVc){ (vc) in
            }
        } else{
            navigateTo(.Main, .IntrestVc){ (vc) in
            }
        }
   
    }
    func valid(){
        
        if UsernameTextfield.text?.count == 0{
            return  self.showAlert("Please enter your name")
        } else if  UserEmailTextfield.text?.count == 0{
            return self.showAlert("Please enter your Email")
        }else if !common.isValidEmail(emailString: UserEmailTextfield.text!){
            return self.showAlert("Please enter your valid Email")
        }else if  DOBtextfield.text?.count == 0{
            return self.showAlert("Please enter your Birthday")
        }
        return nextBtn.isEnabled = true
    }
    
    @IBAction func nextPressed(_ sender: UIButton) {
        
        valid()
        
        passData()
        
        
    }
    
    func ageLimit(age:String){

        let myFormatte = DateFormatter()
                      myFormatte.dateFormat = "yyyy-MM-DD"
               let finalDate : Date = myFormatte.date(from:age) ?? Date()
                      let now = Date()
                      let calendar = Calendar.current
                      let ageComponents = calendar.dateComponents([.year], from: finalDate, to: now)
        
        if (ageComponents.year! < 18){
                   self.showAlert("You must be 18 years of age or older.")
            self.DOBtextfield.text = ""


        }else{
              print("Eligible")
        }

    }

        
        
    
    @IBAction func backClicked(_ sender: UIButton) {
        sender.pulsate(sender: sender)
        navigationController?.popViewController(animated: true)
    }
    
}

