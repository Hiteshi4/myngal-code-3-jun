//
//  PaymentViewController.swift
//  Myngal
//
//  Created by i4 consulate on 27/03/20.
//  Copyright © 2020 SONU. All rights reserved.
//

import UIKit

class PaymentViewController: BaseViewController{
    @IBOutlet weak var footerVw: UIView!
    @IBOutlet weak var tv_PaymentInfo: UITableView!
    var SelectedPlanPrice:Double = 0.0
    var EncashedRupeeFromPOints:Double = 0.0
    var userPoints = 20
    var NetPayableAmmount:Double = 0.0
    var isChecked:Bool = false
    var arrTitle = ["Subscription Charges","My Available Points","u","Value of Uncashed Points","Net Ammount Paybale"]
    
    var updatedData:NSMutableArray = ["","0","","0",""]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
tv_PaymentInfo.register(UINib(nibName: "UsePointTableViewCell", bundle: nil), forCellReuseIdentifier: "UsePointCell")
        tv_PaymentInfo.tableFooterView = footerVw
        updatedData.replaceObject(at: 0, with: SelectedPlanPrice)
        updatedData.replaceObject(at: 1, with: userPoints)
        updatedData.replaceObject(at: 4, with: SelectedPlanPrice)

    }
    @IBAction func backBtnClk(_ sender: UIButton){
        dismiss(animated: true, completion: nil)
    }
    @objc func ReedimPoint(_ txfld:UITextField){
        
        let newText = NSString(string: txfld.text!)
        let num = Int(newText as String)
        if num! < 50{
            self.showAlert("Please enter minimum 50")
             txfld.text = ""
        }else if !num!.isMultiple(of: 50){
            txfld.text = ""
            self.showAlert("Please enter Value Multiple of 50 Only")
        }
        else{
        
        let points = Double(txfld.text ?? "") ?? 0.0
        EncashedRupeeFromPOints = points/2
        updatedData.replaceObject(at: 3, with: EncashedRupeeFromPOints)
        NetPayableAmmount = SelectedPlanPrice - EncashedRupeeFromPOints
        updatedData.replaceObject(at: 4, with: NetPayableAmmount)
        print("EncashedRupeeFromPOints",EncashedRupeeFromPOints)
        }
        
//        if let last = txfld.text?.last {
//            let zero: Character = "0"
//            let num: Int = Int(UnicodeScalar(String(last))!.value - UnicodeScalar(String(zero))!.value)
//            if (num < 0 || num > 3) {
//                //remove the last character as it is invalid
//                txfld.text?.removeLast()
//            }
//        }
        
        
        tv_PaymentInfo.reloadData()
    }
    @objc func CheckBoxClk(_ sender:UIButton){
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            isChecked = true
            tv_PaymentInfo.reloadData()
        }else{
            isChecked = false
            EncashedRupeeFromPOints = 0
            updatedData.replaceObject(at: 3, with: EncashedRupeeFromPOints)
            updatedData.replaceObject(at: 4, with: SelectedPlanPrice)
            tv_PaymentInfo.reloadData()
        }
        
    }
}
extension PaymentViewController:UITableViewDelegate,UITableViewDataSource{
    
    //MARK:- Table Delegates
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "UsePointCell")  as! UsePointTableViewCell
            cell.baseVw.applyViewShadow(shadowOffset: CGSize(width: 1, height: 1), shadowColor: .lightGray, shadowOpacity: 10)
            if isChecked == true {
                cell.pointsInputTxtFld.isEnabled = true
                cell.pointsInputTxtFld.addTarget(self, action: #selector(ReedimPoint(_:)), for: .editingDidEnd)
               
               
            }else{
                cell.pointsInputTxtFld.text = ""
                cell.pointsInputTxtFld.isEnabled = false
                
            }
            
            cell.checkBoxPoints.addTarget(self, action: #selector(CheckBoxClk(_:)), for: .touchUpInside)
            
            return cell
            
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
                   let labelTitle = cell.viewWithTag(101) as! UILabel
                   let baseVw = cell.viewWithTag(102) as! UIView
                   let labelPrice = cell.viewWithTag(103) as! UILabel
        
           labelTitle.text = arrTitle[indexPath.row]
            if indexPath.row == 0 ||  indexPath.row == 3 || indexPath.row == 4 {
                 labelPrice.text = "\u{20B9}" + "\(updatedData[indexPath.row])"
            }else{
                labelPrice.text = "\(updatedData[indexPath.row])"

            }
//            if indexPath.row == 3{
//                if self.isChecked == true{
//                    labelPrice.text = "0"
//
//                }
//            }
        baseVw.applyViewShadow(shadowOffset: CGSize(width: 1, height: 1), shadowColor: .lightGray, shadowOpacity: 10)
        return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 2{
            return 100
        }else{
        
        return 80
        }
    }
}

extension UsePointTableViewCell:UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
      let newText = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        
      if newText.isEmpty {
        return true
      }
      else if let num = Int(newText), num <= 400 {
         return true
      }
      return false
    }
}
