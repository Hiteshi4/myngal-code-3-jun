//
//  GenderViewController.swift
//  Myngal
//
//  Created by SONU on 16/11/19.
//  Copyright © 2019 SONU. All rights reserved.
//

import UIKit

class GenderSelection: UIViewController {
    @IBOutlet weak var maleRadioBtn: UIButton!
    @IBOutlet weak var femaleRadioBtn: UIButton!
    @IBOutlet weak var maleBt: GradientButton!
      @IBOutlet weak var femaleBtn:  GradientButton!
    @IBOutlet weak var maleLbl: UILabel!
    @IBOutlet weak var femaleLbl: UILabel!
    @IBOutlet weak var WelcomeLbl: UILabel!
    
    let singelton = UserParams()
    override func viewDidLoad() {
        super.viewDidLoad()
        WelcomeLbl.text = "Hello! \(singelton.sharedInstance.username) \n Please Select Your Gender"
      maleRadioClicked(maleRadioBtn)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func MaleSelected(_ sender: UIButton) {
        
         // maleBt.backgroundColor = UIColor.red
         // femaleBtn.backgroundColor = .white
          maleBt.setImage(UIImage(named: "male_unselected"), for: .normal)
        femaleBtn.setImage(UIImage(named: "female_unselected"), for: .normal)
        //femaleBtn.setImage(UIImage(named: ""), for: .normal)
        maleBt.topGradientColor = #colorLiteral(red: 1, green: 0.07843137255, blue: 0, alpha: 1)
        maleBt.bottomGradientColor = #colorLiteral(red: 1, green: 0.7137254902, blue: 0.1098039216, alpha: 1)
        femaleBtn.topGradientColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        femaleBtn.bottomGradientColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        femaleRadioBtn.tintColor = .lightGray
        maleRadioBtn.tintColor = #colorLiteral(red: 1, green: 0.07843137255, blue: 0, alpha: 1)
        femaleRadioBtn.setImage(UIImage(named: "circle-outline"), for: .normal)
        maleRadioBtn.setImage(UIImage(named: "radio-on-button"), for: .normal)
        maleRadioBtn.setTitleColor( #colorLiteral(red: 1, green: 0.07843137255, blue: 0, alpha: 1), for: .normal)
        femaleRadioBtn.setTitleColor( .lightGray, for: .normal)
 
      }
      @IBAction func FemaleSelected(_ sender: UIButton) {
        //  femaleBtn.backgroundColor = UIColor.red
        femaleBtn.topGradientColor = #colorLiteral(red: 1, green: 0.07843137255, blue: 0, alpha: 1)
        femaleBtn.bottomGradientColor = #colorLiteral(red: 1, green: 0.7137254902, blue: 0.1098039216, alpha: 1)
        maleBt.topGradientColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        maleBt.bottomGradientColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        femaleBtn.setImage(UIImage(named: "female_selected"), for: .normal)
         maleBt.setImage(UIImage(named: "male_unselected"), for: .normal)
        femaleRadioBtn.tintColor = #colorLiteral(red: 1, green: 0.07843137255, blue: 0, alpha: 1)
        maleRadioBtn.tintColor = .lightGray
        femaleRadioBtn.setImage(UIImage(named: "radio-on-button"), for: .normal)
        maleRadioBtn.setImage(UIImage(named: "circle-outline"), for: .normal)
        maleRadioBtn.setTitleColor(.lightGray, for: .normal)
        femaleRadioBtn.setTitleColor(#colorLiteral(red: 1, green: 0.07843137255, blue: 0, alpha: 1), for: .normal)
      }
    @IBAction func NextBtnPressed(_ sender: UIButton) {
        PassData()
        let singleTon = UserParams()
         
       
           print("passsData=---------\(singleTon.sharedInstance.Selctedgender)")
      navigateTo(.Main, .IntrestVc){ (vc) in
      }
       
    }
    @IBAction func backClicked(_ sender: UIButton) {
        sender.pulsate(sender: sender)
        navigationController?.popViewController(animated: true)
    }
    @IBAction func maleRadioClicked(_ sender: UIButton) {
        maleBt.setImage(UIImage(named: "male_unselected"), for: .normal)
               femaleBtn.setImage(UIImage(named: "female_unselected"), for: .normal)
               //femaleBtn.setImage(UIImage(named: ""), for: .normal)
               maleBt.topGradientColor = #colorLiteral(red: 1, green: 0.07843137255, blue: 0, alpha: 1)
               maleBt.bottomGradientColor = #colorLiteral(red: 1, green: 0.7137254902, blue: 0.1098039216, alpha: 1)
               femaleBtn.topGradientColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
               femaleBtn.bottomGradientColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
               femaleRadioBtn.tintColor = .lightGray
               maleRadioBtn.tintColor = #colorLiteral(red: 1, green: 0.07843137255, blue: 0, alpha: 1)
        maleRadioBtn.setTitleColor( #colorLiteral(red: 1, green: 0.07843137255, blue: 0, alpha: 1), for: .normal)
        femaleRadioBtn.setTitleColor( .lightGray, for: .normal)
               femaleRadioBtn.setImage(UIImage(named: "circle-outline"), for: .normal)
               maleRadioBtn.setImage(UIImage(named: "radio-on-button"), for: .normal)
    }
    @IBAction func femaleRadioClicked(_ sender: UIButton) {
        femaleBtn.topGradientColor = #colorLiteral(red: 1, green: 0.07843137255, blue: 0, alpha: 1)
              femaleBtn.bottomGradientColor = #colorLiteral(red: 1, green: 0.7137254902, blue: 0.1098039216, alpha: 1)
              maleBt.topGradientColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
              maleBt.bottomGradientColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
              femaleBtn.setImage(UIImage(named: "female_selected"), for: .normal)
               maleBt.setImage(UIImage(named: "male_unselected"), for: .normal)
              femaleRadioBtn.tintColor = #colorLiteral(red: 1, green: 0.07843137255, blue: 0, alpha: 1)
              maleRadioBtn.tintColor = .lightGray
              femaleRadioBtn.setImage(UIImage(named: "radio-on-button"), for: .normal)
              maleRadioBtn.setImage(UIImage(named: "circle-outline"), for: .normal)
                maleRadioBtn.setTitleColor(.lightGray, for: .normal)
              femaleRadioBtn.setTitleColor(#colorLiteral(red: 1, green: 0.07843137255, blue: 0, alpha: 1), for: .normal)
    }
    func PassData(){
        let singleton = UserParams()
        if   maleBt.topGradientColor == #colorLiteral(red: 1, green: 0.07843137255, blue: 0, alpha: 1)   {
            singleton.sharedInstance.Selctedgender = "male"
        }else{
            singleton.sharedInstance.Selctedgender = "female"
        }
    }
}

    
    
  

