//
//  PrivacyPolicyViewController.swift
//  Myngal
//
//  Created by SONU on 25/12/19.
//  Copyright © 2019 SONU. All rights reserved.
//

import UIKit





class PrivacyPolicyViewController: BaseViewController,UITextViewDelegate {
    
    @IBOutlet weak var policyTextView: UITextView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let attributedString = NSMutableAttributedString(string: "https://www.google.com                                                                                                                                                                                                                                                                           Online dating applications target a young demographic group. Whereas before, people had very little exposure to online dating, today almost 50% of people know of someone who use the services or has met their loved one through the service.[3] After the iPhone launch in 2007, online dating data has only increased as application usage increased. In 2005, only 10% of 18-24 year olds reported to have used online dating services; this number increased to over 27% of this population.[4] Making this target demographic the largest number of users for most applications. When Pew Research Center conducted a study in 2016, they found that 59% of U.S. adults agreed that online dating is a good way to meet people compared to 44% in 2005. This increase in usage by this target group can be justified by their increased use of smartphones which lead them to use these smartphone dating apps. About 1 in 5 18-24-year-old (22%) reported using dating applications in 2016, whereas only 5% did so in 2003.!")
               attributedString.addAttribute(.link, value: "https://www.google.com", range: NSRange(location: 0, length: 30))
        
               policyTextView.attributedText = attributedString
    
        
//     let linkedText = NSMutableAttributedString(attributedString: policyTextView.attributedText)
//        let hyperlinked = linkedText.
//     if hyperlinked {
//           policyTextView.attributedText = NSAttributedString(attributedString: linkedText)
//     }
        
    }
    @IBAction func backTApped(_ sender: UIButton) {
        sender.pulsate(sender: sender)
        dismiss(animated: true, completion: nil)
    }
    
    
}
