//
//  ChatViewController.swift
//  Myngal
//
//  Created by Gulshan Agrawal on 18/02/20.
//  Copyright © 2020 SONU. All rights reserved.
//

import UIKit
import Firebase
import ReverseExtension

protocol UserDataDelegate {
    func getUserData(name:String,userId:Int)
}


class ChatViewController: BaseViewController {

    //MARK: Properties
    var name:String!
    var userId:Int!
    var userImg:String!
    var imagePicker : UIImagePickerController! = UIImagePickerController()
    var arrChats = [chat]()
    var i:Int = 0
    let common = Common()
    let apiMapper = APIMapper()
    
    //MARK: IBOutlets
    
    @IBOutlet weak var bottomViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var userOnlineStatus: UILabel!
    @IBOutlet weak var userNameTitle: UILabel!
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var userImage:UIImageView!
    @IBOutlet weak var tblMessage: UITableView!
    @IBOutlet weak var txtMsg: UITextView!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var bottomVw: UIView!
    @IBOutlet weak var btnsend:UIButton!
    @IBOutlet weak var btnAttach:UIButton!
    
    //MARK: IBAction
    
    @IBAction func btnSendClk(_ sender: Any) {
        ///Date().string(format: "dd-mm-yy - hh:mm a")
//        let messageDate = Date.init(timeIntervalSince1970: TimeInterval(self.items[indexPath.row].timestamp))
//
//        let dateformatter = DateFormatter.init()
//        dateformatter.dateStyle = .short
//        dateformatter.dateFormat = "hh:mm a"
//        let date = dateformatter.string(from: messageDate)
        i+=1
        if i == 1{
            
        }
        
        var chatData = ["image":"","message":"\(txtMsg.text!)","sander":"\(Common().getUserId())","time":Date().string(format: "dd/mm/yy - hh:mm a"),"type":"message"] as [String : Any]
        
        ChatManager.chatInit(fromID: Common().getUserId(), toID: userId, chatData: chatData)
        txtMsg.text = nil
      //  txtMsg.textColor = UIColor.lightGray
        let newMessg = chat.init(chatDic: chatData as NSDictionary)
        arrChats.append(newMessg)
        tblMessage.reloadData()
    }
    @IBAction func backBtnClk(_ sender: Any) {
        //(sender as AnyObject).pulsate(sender: sender)
        self.performSegueToReturnBack()
        //navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAttachClk(_ sender: Any){
        pickImage()
    }
    
    
    
    //MARK: Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
    navigationController?.navigationBar.isHidden = false
        txtMsg.delegate = self
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
       
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow(_:)),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        userImage.layer.cornerRadius = 25
        userImage.layer.borderWidth = 1
        userImage.layer.borderColor = UIColor.white.cgColor
        userImage.setImage(from: AppUrls.imagebaseUrl+"\(userImg as? String ?? "")", placeholderImage: #imageLiteral(resourceName: "male_selected"))
        lblHeader.text = name
        tblMessage.delegate = self
        tblMessage.dataSource = self
        
//        tblMessage.re.delegate = self
//            tblMessage.re.scrollViewDidReachTop = { scrollView in
//                print("scrollViewDidReachTop")
//            }
//           tblMessage.re.scrollViewDidReachBottom = { scrollView in
//                print("scrollViewDidReachBottom")
//            }
        
        txtMsg.text = "Write your message"
        
        txtMsg.textColor = UIColor.lightGray
        
        var chatId:String!
        let myId = Common().getUserId()
        print(myId)
        if myId < userId
         {
            chatId = "\(userId!)_\(myId)"
         }
         else
         {
            chatId = "\(myId)_\(userId!)"
         }
        ChatManager.getChatDetail(chatId: chatId) { (fir_chats) in
            self.arrChats = fir_chats
            self.tblMessage.reloadData()
            print(fir_chats)
        }
    }

}

extension ChatViewController:UITextViewDelegate
{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
               textView.text = nil
               textView.textColor = UIColor.black
           }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        bottomViewConstraint.constant  = 0
    }
}


extension ChatViewController:UIImagePickerControllerDelegate,UINavigationControllerDelegate
{
    func pickImage() {
       
           let alert:UIAlertController=UIAlertController(title: "Select Image", message: nil, preferredStyle: .actionSheet)
           
           let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
               
           {
               
               UIAlertAction in
               
               self.openCamera()
           }
           let galleryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default)
           {
               UIAlertAction in
               self.openGallery()
           }
           let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
           {
               UIAlertAction in
           }
           
           self.imagePicker.delegate = self
           alert.addAction(cameraAction)
           alert.addAction(galleryAction)
           
           alert.addAction(cancelAction)
           //You must add this line then only in iPad it will not crash
           alert.popoverPresentationController?.sourceView = self.view
           self.present(alert, animated: true, completion: nil)
       }

       
       
       func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
           picker.dismiss(animated: true, completion: nil)
       }
     
       func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
           
           guard let image = info[.originalImage] as? UIImage else {
               fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
           }
        
        sendImageOnServer(selectedImg: image)
        picker.dismiss(animated: true, completion: nil)
           //pickImageCallback?(image)
       }
    
    func openCamera()
    {
        
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            
            self.imagePicker.sourceType = .camera
            self.imagePicker.allowsEditing = true
            self.imagePicker.delegate = self
            self.present(self.imagePicker, animated: true, completion: nil)
        }
    }
    
    func openGallery()
    {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            self.imagePicker.allowsEditing = true
            self.imagePicker.sourceType = .photoLibrary
            self.imagePicker.delegate = self
            
            self.present(self.imagePicker, animated:true, completion: nil);
        }
        
    }
    
    func sendImageOnServer(selectedImg:UIImage){
        
        let imgName = AppUrls.chatimagebaseUrl+"\(Date().timeIntervalSince1970).png"
        
        
        APIMapper().chatImage(img: selectedImg, imgName: imgName, success: { (res) in
            print(res)
            let result = res as! NSDictionary
            if result["status"] as! String == "true"
            {
                let data = result["data"] as! NSDictionary
               let imgNameRet = AppUrls.chatimagebaseUrl+"\(data["upload_file"] as! String)"
                self.sendImageOnFir(imgName: imgNameRet)
            }
        }) { (err) in
            print("error"+err)
        }
        
        
        
    }
    
    
    func sendImageOnFir(imgName:String)
    {
        let chatData = ["image":imgName,"message":"","sander":"\(Common().getUserId())","time":Date().string(format: "dd/mm/yy - hh:mm a"),"type":"image"] as [String : Any]
        
        ChatManager.chatInit(fromID: Common().getUserId(), toID: userId, chatData: chatData)
       // let newMessg = chat.init(chatDic: chatData as NSDictionary)
        //arrChats.append(newMessg)
        //tblMessage.reloadData()
    }
    
    
    func callLikeApi(){
       // let userID:String = userInfo!.data!.id
        self.showActivity()
        let param : [String:Any] = [
            "from_id" : self.common.getUserId(),
               "to_id": userId,
        ]
        print(param)
        apiMapper.addTolikes(param, success: { (SucessDict) in
            let message = SucessDict["message"] as! String
            self.showToast(message: message)
            self.hideActivity()
            
            print(param)
           print(SucessDict)
            
        }) { (Failure) in
            print(Failure)
            self.hideActivity()
            self.showAlert(Failure)
        }
    }
    //MARK: Keyboard Notifications
         
         @objc func keyboardWillShow(_ notification: Notification) {
            
             if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
                 
                 let keyboardRectangle = keyboardFrame.cgRectValue
                 let keyboardHeight = keyboardRectangle.height
                
                // constraintYNextButton.constant = keyboardHeight+20
                 
                 view.layoutIfNeeded()
                 UIView.animate(withDuration: 0.3) {
                self.bottomViewConstraint.constant = keyboardHeight-20
                       self.view.layoutIfNeeded()
                    // self.view.layoutIfNeeded()
                 }
                 
             }
             
         }
}


extension ChatViewController:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrChats.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let det = arrChats[indexPath.row] as! chat
        print(det)
        if det.sender == "\(Common().getUserId())"
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Receiver") as! ReceiverCell
    
            if det.type == "message"
            {
            
                cell.message.isHidden = false
                cell.message.text = det.message
            }
            else
            {
                cell.message.isHidden = true
                cell.messageBackground.sd_setImage(with: URL(string: det.image), completed: nil)
            }
            
            cell.times.text = det.time
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Sender") as! SenderCell
           if det.type == "message"
            {
                cell.message.isHidden = false
                cell.message.text = det.message
                
            }
            else
            {
                cell.message.isHidden = true
                cell.messageBackground.sd_setImage(with: URL(string: det.image), completed: nil)
            }
            
            cell.times.text = det.time
                       return cell
        }
    }
    
    
}
extension Date {
    func string(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}


