//
//  UsePointTableViewCell.swift
//  Daily Delivery Driver
//
//  Created by i4 consulate on 27/03/20.
//  Copyright © 2020 SONU. All rights reserved.
//

import UIKit

class UsePointTableViewCell: UITableViewCell {
    var maxValue = 400 {
      didSet {
        pointsInputTxtFld.text = nil
      }
    }
    @IBOutlet weak var pointsInputTxtFld: UITextField!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var checkBoxPoints: UIButton!
    @IBOutlet weak var baseVw: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        pointsInputTxtFld.delegate = self
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
