//
//  OtherUserDetailTableViewCell.swift
//  Myngal
//
//  Created by SONU on 18/01/20.
//  Copyright © 2020 SONU. All rights reserved.
//

import UIKit

class OtherUserDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var blockButton: UIButton!
    @IBOutlet weak var messageBtn: UIButton!
    var Message = "false"
    @IBOutlet weak var nameAgeLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
          
       messageBtn.isHidden = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
  
   
}
class OtherUserProfession: UITableViewCell {
    @IBOutlet weak var educationViewHeight: NSLayoutConstraint!
    @IBOutlet weak var professionHeight: NSLayoutConstraint!
    @IBOutlet weak var commitmentHeight: NSLayoutConstraint!
    @IBOutlet weak var locationHeight: NSLayoutConstraint!
    @IBOutlet weak var profession: UILabel!
    @IBOutlet weak var professioView: UIView!
    @IBOutlet weak var educationView: UIView!
    
    @IBOutlet weak var locationView: UIView!
    @IBOutlet weak var commitmentView: UIView!
    @IBOutlet weak var professionIcon: UIImageView!
    @IBOutlet weak var commitmentType: UILabel!
    @IBOutlet weak var education: UILabel!
    @IBOutlet weak var location: UILabel!
    
    override func awakeFromNib() {
           super.awakeFromNib()
        self.professionIcon.tintColor = .black
//         professionIcon.layer.borderWidth = 1.5
//           let color = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
//          professionIcon.layer.cornerRadius = 10
//        professionIcon.layer.borderColor = color.cgColor
       }
    
    
}
class OtherUserAboutme: UITableViewCell {
    
    @IBOutlet weak var aboutMeText: UITextView!
    
}
class OtherUserIntrest: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    var Intrestarr = [""]
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var intrest: UILabel!
    override func awakeFromNib() {
        print(Intrestarr)
               super.awakeFromNib()
        collectionView.delegate = self
        collectionView.dataSource = self
           }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Intrestarr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OtherUserIntrestCell", for: indexPath) as! OtherUserIntrestCell
        cell.label.textColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
        cell.label.text = "#\(Intrestarr[indexPath.row])"
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let text = "#\(Intrestarr[indexPath.row])"
           let sysFont: UIFont = UIFont.systemFont(ofSize: 16.0)
           let width = UILabel.textWidth(font: sysFont, text: text )
           return CGSize(width: width + 18, height:35)
            
         }
    
}


class OtherUserIntrestCell: UICollectionViewCell{
    @IBOutlet weak var BackView: UIView!
    
    @IBOutlet weak var label: UILabel!
    override func awakeFromNib() {
           super.awakeFromNib()
            BackView.layer.borderWidth = 1.5
              let color = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
      let width =  BackView.frame.size.height
             BackView.layer.cornerRadius = 10
           BackView.layer.borderColor = color.cgColor
       }
    
}


