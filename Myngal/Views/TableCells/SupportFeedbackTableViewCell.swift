    //
    //  SupportFeedbackTableViewCell.swift
    //  Myngal
    //
    //  Created by SONU on 16/01/20.
    //  Copyright © 2020 SONU. All rights reserved.
    //
    
    import UIKit
    import Photos
    
    class SupportFeedbackTableViewCell: UITableViewCell,UITextViewDelegate,UIImagePickerControllerDelegate{
        @IBOutlet weak var FeedbackView: UIView!
        @IBOutlet weak var textView: UITextView!
        @IBOutlet weak var characterCountLbl: UILabel!
        @IBOutlet weak var submitbutton: GradientButton!
        @IBOutlet weak var browseButton: UIButton!
           let common  = Common()
        @IBOutlet weak var PickedImageName: UILabel!
        var imagePicker : UIImagePickerController! = UIImagePickerController()
        var actionSheet : UIActionSheet!           = UIActionSheet()
        let apiMapper = APIMapper()
        @IBOutlet weak var subjectTextfield: UITextField!
        override func awakeFromNib() {
            super.awakeFromNib()
            textView.delegate = self
            FeedbackView.isHidden = true
            textView.layer.borderColor =  UIColor.lightGray.cgColor
            textView.layer.borderWidth = 0.5
            textView.layer.cornerRadius = 1
            subjectTextfield.layer.borderColor =  UIColor.lightGray.cgColor
            subjectTextfield.layer.borderWidth = 0.5
            subjectTextfield.layer.cornerRadius = 1
            
        }
        
        
        override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)
            
            // Configure the view for the selected state
        }
      
        
        internal func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
        {
            let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
            let numberOfChars = newText.count
            return numberOfChars <= 250
        }
        
        func textViewDidChange(_ textView: UITextView)
        {
            let currentCharacterCount = 250 - textView.text.count
            
            if currentCharacterCount > 0 {
                characterCountLbl.text = "\(250 - textView.text.count) Left"
            }
            else
            {
                
                characterCountLbl.text = "\(250 - textView.text.count) Left"
            }
        }
        
        
        
        
        
        @IBAction func showFeedbackView(_ sender: UIButton) {
            print("click here tapped..")
            FeedbackView.isHidden = false
        }
        @IBAction func UploadImage(_ sender: UIButton) {
            
        }
        
        
        
       
        
    }
