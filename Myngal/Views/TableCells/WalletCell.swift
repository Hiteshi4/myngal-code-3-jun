//
//  WalletCell.swift
//  Myngal
//
//  Created by SONU on 12/03/20.
//  Copyright © 2020 SONU. All rights reserved.
//

import UIKit

class WalletCell: UITableViewCell {
    @IBOutlet weak var baseVw: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var valueLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
