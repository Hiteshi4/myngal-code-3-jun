//
//  SettingTableViewCell.swift
//  Myngal
//
//  Created by SONU on 03/01/20.
//  Copyright © 2020 SONU. All rights reserved.
//

import UIKit

class SettingTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var subTitleLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

class UserSetting: UITableViewCell {
    
    @IBOutlet weak var hideAgeSwitch: UISwitch!
    @IBOutlet weak var lastActiveSwitch: UISwitch!
    @IBOutlet weak var distanceSwitch: UISwitch!
    var HideAge = "0"
    var lastActive = "0"
    var locationInvisible = "0"
    
    override func awakeFromNib() {
        super.awakeFromNib()
      //  DataSet()
        // Initialization code
    }
    
    func DataSet(){
        if hideAgeSwitch.isOn == true{
            self.HideAge = "1"
        }else{
                self.HideAge = "0"
                print("Show Age")
                }
        if lastActiveSwitch.isOn == true{
            self.lastActive = "1"
        }else{
                self.lastActive = "0"
                print("Show Age")
                }
//        if distanceSwitch.isOn == true{
//            self.locationInvisible = "1"
//        }else{
//                self.locationInvisible = "0"
//                print("Show Age")
//                }
    }
    
    
    
    
    
    @IBAction func HideAgeTapped(_ sender: UISwitch) {
        sender.viewWithTag(0)
     if  sender.isOn{
       
        self.HideAge = "1"
            print("Hide Age")
     }else{
        self.HideAge = "0"
        print("Show Age")
        }

    }
    @IBAction func lastActiveTapped(_ sender: UISwitch) {
        if  sender.isOn{
               self.lastActive = "1"
                   print("Hide lastActive")
            }else{
               self.lastActive = "0"
               print("Show lastActive")
               }
    }
    @IBAction func locationInvisibleTapped(_ sender: UISwitch) {
        if  sender.isOn{
                      self.locationInvisible = "1"
                          print("Hide Location")
                   }else{
                      self.locationInvisible = "0"
                      print("Show Location")
                      }
    }
}
class NotificationSetting: UITableViewCell {
    var chatMsg = "0"
    var newMatch = "0"
    var beingShortlist = "0"
    var beingLiked = "0"
    var adminMsg = "0"
    @IBOutlet weak var chatSwitch: UISwitch!
    @IBOutlet weak var matchesSwitch: UISwitch!
    @IBOutlet weak var beingLikedSwitc: UISwitch!
    @IBOutlet weak var beingShortlistedSwitch: UISwitch!
    @IBOutlet weak var adminMsgSwitch: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBAction func messageRecieveTapped(_ sender: UISwitch) {
        if sender.tag == 101{
            if sender.isOn{
                self.chatMsg = "1"
                print("\(sender.tag) is ON")
            }else{
                self.chatMsg = "0"
                 print("\(sender.tag) is OFF")
            }
        }
        if sender.tag == 102{
            self.newMatch = "1"
            if sender.isOn{
                 print("\(sender.tag) is ON")
            }else{
                self.newMatch = "0"
                 print("\(sender.tag) is OFF")
            }
        }
        if sender.tag == 103{
            if sender.isOn{
                self.beingShortlist = "1"
                 print("\(sender.tag) is ON")
            }else{
                self.beingShortlist = "0"
                 print("\(sender.tag) is OFF")
            }
        }
        if sender.tag == 104{
            
            if sender.isOn{
                self.beingLiked = "1"
                 print("\(sender.tag) is ON")
            }else{
                self.beingLiked = "0"
                 print("\(sender.tag) is OFF")
            }
        }
        if sender.tag == 105{
                   
                   if sender.isOn{
                       self.adminMsg = "1"
                        print("\(sender.tag) is ON")
                   }else{
                       self.adminMsg = "0"
                        print("\(sender.tag) is OFF")
                   }
               }
    }
    
}
