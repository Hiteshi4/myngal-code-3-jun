//
//  MessageTableViewCell.swift
//  Myngal
//
//  Created by SONU on 01/01/20.
//  Copyright © 2020 SONU. All rights reserved.
//

import UIKit

class MessageTableViewCell: UITableViewCell {

    @IBOutlet weak var professionlbl: UILabel!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var BackView: UIView!
    @IBOutlet weak var namelbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
   
       BackView!.dropShadow(color: .lightGray, opacity: 1, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
   
        accessoryType = .none
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
