//
//  HelpSupportTableViewCell.swift
//  Myngal
//
//  Created by SONU on 16/01/20.
//  Copyright © 2020 SONU. All rights reserved.
//

import UIKit

class HelpSupportTableViewCell: UITableViewCell {

    @IBOutlet weak var questionLbl: UILabel!
    @IBOutlet weak var ansTextView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        ansTextView.isUserInteractionEnabled = false
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
