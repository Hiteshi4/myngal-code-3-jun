//
//  UploadPhotoCell.swift
//  Myngal
//
//  Created by Gulshan Agrawal on 06/02/20.
//  Copyright © 2020 SONU. All rights reserved.
//

import UIKit

class UploadPhotoCell: UITableViewCell {

    
    @IBOutlet weak var baseVw1:UIView!
    @IBOutlet weak var baseVw2:UIView!
    @IBOutlet weak var baseVw3:UIView!
    @IBOutlet weak var baseVw4:UIView!
    
    @IBOutlet weak var Img1:UIImageView!
    @IBOutlet weak var Img2:UIImageView!
    @IBOutlet weak var Img3:UIImageView!
    @IBOutlet weak var Img4:UIImageView!
    
    @IBOutlet weak var btn1:UIButton!
     @IBOutlet weak var btn2:UIButton!
     @IBOutlet weak var btn3:UIButton!
     @IBOutlet weak var btn4:UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
