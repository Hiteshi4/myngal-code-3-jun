//
//  ProfilePhotosTableViewCell.swift
//  Myngal
//
//  Created by SONU on 25/01/20.
//  Copyright © 2020 SONU. All rights reserved.
//

import UIKit

class ProfilePhotosTableViewCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
   
    var imageData : UserDetail?
    @IBOutlet weak var collectioView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectioView.delegate = self
        collectioView.dataSource = self
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
           return 4
       }
       
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ProfilePhotosCollectionViewCell
        if indexPath.row == 0{
            let image = self.imageData?.image1
            cell.cellImg.setImage(from: AppUrls.imagebaseUrl+"\(image ?? "")")
            cell.cellImg.contentMode = .scaleToFill
            cell.cellImg.layer.cornerRadius = 10
            
        }
       else if indexPath.row == 1{
            let image = self.imageData?.image2
            cell.cellImg.setImage(from: AppUrls.imagebaseUrl+"\(image ?? "")")
            cell.cellImg.contentMode = .scaleToFill
        }else if indexPath.row == 2{
            let image = self.imageData?.image2
            cell.cellImg.setImage(from: AppUrls.imagebaseUrl+"\(image ?? "")")
            cell.cellImg.contentMode = .scaleToFill
        }else if indexPath.row == 3{
            let image = self.imageData?.image3
            cell.cellImg.setImage(from: AppUrls.imagebaseUrl+"\(image ?? "")")
            cell.cellImg.contentMode = .scaleToFill
        }
                 return cell
}
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
                
                 return CGSize(width: 130, height: 130)
             }
}




class ProfilePhotosCollectionViewCell: UICollectionViewCell {
   
    @IBOutlet weak var AddImgBtn: UIButton!
    @IBOutlet weak var cellImg: UIImageView!
    
    override func awakeFromNib() {
    super.awakeFromNib()
        
    }
        
    }
    

class SubscriptioCollectionViewCell: UICollectionViewCell{
    
    @IBOutlet weak var baseVw: UIView!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var pricelbl: UILabel!
    @IBOutlet weak var lblMonth: UILabel!
    
    var layerArray = NSMutableArray()
    
    override func awakeFromNib() {
       super.awakeFromNib()
        
           baseVw.backgroundColor = #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 0.5950034981)
       }
    func toggleSelected ()
    {
     if (isSelected){
       baseVw.backgroundColor = .red
      let gradient = CAGradientLayer()
let topColor:UIColor = #colorLiteral(red: 0.9960784314, green: 0.07450980392, blue: 0.003921568627, alpha: 1)
let bottomColor:UIColor = #colorLiteral(red: 0.9607843161, green: 0.7058823705, blue: 0.200000003, alpha: 1)
gradient.frame = baseVw.bounds
gradient.colors = [bottomColor.cgColor, topColor.cgColor]
baseVw.layer.insertSublayer(gradient, at: 0)
        layerArray.add(gradient)
       baseVw.transform = baseVw.transform.scaledBy(x: 0.001, y: 0.001)

       UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.3, options: .curveEaseInOut, animations: {
        self.baseVw.transform = CGAffineTransform.identity.scaledBy(x: 1.0, y: 1.0)
         }, completion: nil)
baseVw.layer.shadowColor = UIColor.gray.cgColor
baseVw.layer.shadowOpacity = 10
baseVw.layer.shadowRadius = 10
lblDuration.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
pricelbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
lblMonth.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)

        }else {
baseVw.layer.shadowColor = #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 0.5950034981)
baseVw.layer.shadowOpacity = 0
baseVw.layer.shadowRadius = 0
baseVw.backgroundColor = #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 0.5950034981)
lblDuration.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
pricelbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
lblMonth.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)

        
        for layer in self.baseVw.layer.sublayers! {
            if(layerArray.contains(layer)){
                layer.removeFromSuperlayer()
                layerArray.remove(layer)
            }
        }
        

        }
    }

}


