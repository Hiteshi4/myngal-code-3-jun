//
//  CommitmentTableViewCell.swift
//  Myngal
//
//  Created by SONU on 31/12/19.
//  Copyright © 2019 SONU. All rights reserved.
//

import UIKit

class CommitmentTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
       selectionStyle = .none
    if selected {
            accessoryType = .checkmark
        textLabel?.textColor = #colorLiteral(red: 1, green: 0.07843137255, blue: 0, alpha: 1)
        print(IndexPath())

          } else {
        textLabel?.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            accessoryType = .none
          }
    }

}
