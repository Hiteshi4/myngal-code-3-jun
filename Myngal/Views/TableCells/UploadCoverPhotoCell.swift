//
//  UploadCoverPhotoCell.swift
//  Myngal
//
//  Created by Gulshan Agrawal on 06/02/20.
//  Copyright © 2020 SONU. All rights reserved.
//

import UIKit

class UploadCoverPhotoCell: UITableViewCell {

    
    @IBOutlet weak var baseVw:UIView!
    @IBOutlet weak var img:UIImageView!
    @IBOutlet weak var btn:UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
