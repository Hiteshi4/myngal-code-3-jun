//
//  SideMenuTableViewCell.swift
//  Myngal
//
//  Created by SONU on 26/12/19.
//  Copyright © 2019 SONU. All rights reserved.
//

import UIKit

class SideMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var MenuIcon: UIImageView!
    @IBOutlet weak var Menutitle: UILabel!
    
    var menuTitle = ["Profile","Setting"]
    
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

 struct SideMenuItem1{
        
        let image:UIImage!
        let title:String!
        
        init(imageName:String,title:String) {
            self.image = UIImage(named: imageName)
            self.title = title
        }
        
    }
    struct SideMenuTableViewList1 {
    
    var items = [SideMenuItem1]()
    var selectedIndex:IndexPath?
    
    
    init() {
        reset()
    }
    
    mutating func reset(){
        
           var arrImageName = [ "profile","message", "heart", "block", "bookmark-2","message", "filter_icon","invite", "subscription", "message", "helpSupport", "setting","logout"]
           
         
           
           var arrTitles =  ["Profile","Messages","Likes","Blocked Users","Saved Users","Matches","Filter","Invite Friends","Subscription","Privacy Policy","Help & Support","Setting","Logout"]
           
          for i in 0..<arrTitles.count{
                     let item1 = SideMenuItem1(imageName: arrImageName[i], title: arrTitles[i])
                     items.append(item1)
           }
           
         
           
         
           }
 
    

}
