//
//  BlockTableViewCell.swift
//  Myngal
//
//  Created by SONU on 01/01/20.
//  Copyright © 2020 SONU. All rights reserved.
//

import UIKit

class BlockTableViewCell: UITableViewCell {
    @IBOutlet weak var mainView: GradientButton!
    @IBOutlet weak var nameAgeLb: UILabel!
    @IBOutlet weak var profession: UILabel!
    @IBOutlet weak var profileImg: UIImageView!
    
    @IBOutlet weak var trashButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        mainView!.dropShadow(color: .lightGray, opacity: 3, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
