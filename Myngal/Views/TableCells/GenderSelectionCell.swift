//
//  GenderSelectionCell.swift
//  Myngal
//
//  Created by SONU on 27/01/20.
//  Copyright © 2020 SONU. All rights reserved.
//

import UIKit

protocol GenderSelectionProtocol {
    func getGender(gender:String)
}


class GenderSelectionCell: UITableViewCell {
    @IBOutlet weak var maleBtn: UIButton!
    @IBOutlet weak var femaleBtn: UIButton!
    
    @IBOutlet weak var titleCell: UILabel!
    var delegate:GenderSelectionProtocol!
    var seletedGender:String!
    override func awakeFromNib() {
        super.awakeFromNib()
        seletedGender = "male"
       // self.delegate = self as! GenderSelectionProtocol
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
 
        
    }
    
 
    @IBAction func MaleTapped(_ sender: UIButton) {
        
        
        
                if #available(iOS 13.0, *) {
                    seletedGender = "male"
                              maleBtn.setImage(UIImage(systemName: "checkmark.square.fill"), for: .normal)
                               femaleBtn.setImage(UIImage(systemName: "square"), for: .normal)
                          }

                
                maleBtn.tintColor = #colorLiteral(red: 1, green: 0.07843137255, blue: 0, alpha: 1)
                femaleBtn.tintColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        delegate.getGender(gender: seletedGender)

       }
    @IBAction func femaleTapped(_ sender: UIButton) {
        if #available(iOS 13.0, *) {
            seletedGender = "female"
                      femaleBtn.setImage(UIImage(systemName: "checkmark.square.fill"), for: .normal)
                       maleBtn.setImage(UIImage(systemName: "square"), for: .normal)
                  }

        
        maleBtn.tintColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        femaleBtn.tintColor = #colorLiteral(red: 1, green: 0.07843137255, blue: 0, alpha: 1)
      delegate.getGender(gender: seletedGender)
   }
}
