//
//  HomeExtension.swift
//  Myngal
//
//  Created by SONU on 15/11/19.
//  Copyright © 2019 SONU. All rights reserved.
//

import Foundation
import Koloda
import Kingfisher

extension HomeViewController:KolodaViewDelegate,KolodaViewDataSource{
    
    
    
    
    func kolodaDidRunOutOfCards(_ koloda: KolodaView) {
        self.showAlert("No More users nearby")
    }
    
    func koloda(_ koloda: KolodaView, didSelectCardAt index: Int) {
        let userProfile = propertyArray[index]
        PresentTo(.Main, .OtherUserProfileVC){ (vc) in
            guard let viewProfileVC = vc as? OtherUserProfileViewController else{return}
            viewProfileVC.propertyArray = userProfile
        }
        print("\(index) card Tapped")
        
    }
    
    func kolodaNumberOfCards(_ koloda:KolodaView) -> Int {
        
        return propertyArray.count
    }
    
    func kolodaSpeedThatCardShouldDrag(_ koloda: KolodaView) -> DragSpeed {
        return .fast
    }
    func kolodaShouldMoveBackgroundCard(_ koloda: KolodaView) -> Bool {
        return true
    }
    
    func koloda(_ koloda: KolodaView, didSwipeCardAt index: Int, in direction: SwipeResultDirection) {
        guard let profileImageURL1 = URL(string:(propertyArray[index].profile_image) ?? "") else{return }
        backgroundImage.setImage(from: AppUrls.imagebaseUrl+"\(profileImageURL1)")
        
        let userID = propertyArray[index].id
        if direction == SwipeResultDirection.left{
            self.callLikeApi(islike: "unlike",otherUserId: "\(userID)", isBlock: "")
            
            print("Left Swiped")
            
        }else{
            self.callLikeApi(islike: "like",otherUserId: "\(userID)" ,isBlock: "")
            print("Right Swiped")
            
        }
        
        
    }
    func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        
        
        let view = Bundle.main.loadNibNamed("SwipeView", owner: self, options: nil)?[0] as! Swipe
        
        if index < propertyArray.count{
            
            
            
            let data = propertyArray[index]
            let userAge = Age.toAge(dob: data.user_info!.dob) as? String ?? ""
            
            view.nameAge.text = "\(data.name), \(userAge)"
            guard let profileImageURL = URL(string:(propertyArray[index].profile_image) ?? "") else{return view}
            
            view.cardImage.setImage(from: AppUrls.imagebaseUrl+"\(profileImageURL)")
            view.cityLbl.text = data.user_info?.city ?? ""
            view.proffession.text = data.user_info?.profession ?? ""
            
            
            guard let profileImageURL1 = URL(string:(propertyArray[index].profile_image) ?? "") else{return backgroundImage}
            backgroundImage.setImage(from: AppUrls.imagebaseUrl+"\(profileImageURL1)")
        }
        
        return view
        
    }
    
}


