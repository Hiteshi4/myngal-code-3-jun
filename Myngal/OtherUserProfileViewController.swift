//
//  OtherUserProfileViewController.swift
//  Myngal
//
//  Created by SONU on 18/01/20.
//  Copyright © 2020 SONU. All rights reserved.
//

import UIKit
import Kingfisher
import Alamofire

class OtherUserProfileViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource{
   
    @IBOutlet weak var tbleView: UITableView!
    var imageArr = [String]()
    @IBOutlet weak var userProfileImg: UIImageView!
    var propertyArray:PreferredProduct1!
    var Height:CGFloat = 10
    var arrImageName = [ "profile","message", "heart", "block", "block","message", "profile","invite", "subscription", "message", "logout", "logout","logout"]
    let apiMapper = APIMapper()
    let common = Common()
    var isFromeMatches = "false"
    override func viewDidLoad() {
        super.viewDidLoad()
    print("Is from matchessssss",isFromeMatches)
    setUpdata()
    tbleView.tableFooterView = UIView()
        
    }
 @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
  setupView()
        print(imageArr)
       
    }
    
    
  func setupView(){
    
    
        PresentTo(.Main, .ImageSliderVc) { (vc) in
           guard let viewProfileVC = vc as? ImageSliderVc else{return}
            viewProfileVC.imageArr = self.imageArr
        }

     }
     
    func setUpdata(){
                
        print("DAtaPased==---\(propertyArray)")
                guard  let profileImageURL = URL(string:(self.propertyArray.profile_image ?? "")) else{return userProfileImg.setImage(from: "cover")}
      
                    userProfileImg.setImage(from: AppUrls.imagebaseUrl+"\(profileImageURL)", placeholderImage: #imageLiteral(resourceName: "man1"))
       
                tbleView.reloadData()
                tbleView.estimatedRowHeight = 300
                tbleView.rowHeight = UITableView.automaticDimension
                navigationController?.navigationBar.isHidden = false
                
                let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
                userProfileImg.isUserInteractionEnabled = true
                userProfileImg.addGestureRecognizer(tapGestureRecognizer)
         if propertyArray.profile_image! != ""{
             imageArr.append(propertyArray.profile_image! as? String ?? "")
         }
        if propertyArray.user_info!.image1 != ""{
            imageArr.append(propertyArray.user_info!.image1 as? String ?? "")
        }
        if propertyArray.user_info!.image2 != ""{
            imageArr.append(propertyArray.user_info!.image2 as? String ?? "")
        }
       if propertyArray.user_info!.image3 != ""{
                  imageArr.append(propertyArray.user_info!.image3 as? String ?? "")
              }
        if propertyArray.user_info!.image4 != ""{
                   imageArr.append(propertyArray.user_info!.image4 as? String ?? "")
               }
    }
    func callLikeApi(islike:String,otherUserId:String,isBlock:String){
       // let userID:String = userInfo!.data!.id
        self.showActivity()
        let param : [String:Any] = [
            "user_id" : self.common.getUserId(),
               "other_user_id": otherUserId,
               "is_like" : islike,
               "is_block":isBlock,
               
               
               
            
        ]
        print(param)
        apiMapper.addTolikes(param, success: { (SucessDict) in
            let message = SucessDict["message"] as! String
            self.showToast(message: message)
            self.hideActivity()
            
            print(param)
           print(SucessDict)
            
        }) { (Failure) in
            print(Failure)
            self.hideActivity()
            self.showAlert(Failure)
        }
    }
    
    func CallsaveForlaterApi(otherUserId:String = ""){
        self.showActivity()
           
           let param : [String:Any] = [
            "user_id" : common.getUserId(),
                  "other_user_id": otherUserId,
                  "is_save": "save"
               
           ]
           
           apiMapper.saveForlater(param, success: { (SucessDict) in
            
               self.hideActivity()
            let message = SucessDict["message"] as! String
            self.showToast(message: message)
               print("Parametere:=----------\(param)")
               
           }) { (Failure) in
               self.hideActivity()
               self.showAlert(Failure)
           }
       }
   
//MARK:- TableView Delegate
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return 4
      }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
   
        if indexPath.row == 0 {
            return 50
        }
        if indexPath.row == 1 {
            return Height
           
        }
        if indexPath.row == 2 {
            let text = self.propertyArray.user_info?.aboutme ?? ""
            if text == ""{
                return 50
            }else{
                let sysFont: UIFont = UIFont.systemFont(ofSize: 16.0)
        
                let height =  text.height(withConstrainedWidth: tbleView.frame.size.width, font: sysFont)
                    return CGFloat((height)+60)
            }
                                
          
            
           
        }else{
            var interest = self.propertyArray.user_info?.interest ?? ""
             if interest != ""
             {return UITableView.automaticDimension}
             else{ return 50}
        }
    }
      
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         
        if indexPath.row == 0 {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OtherUserDetailTableViewCell") as! OtherUserDetailTableViewCell
            if self.isFromeMatches == "true"{
                cell.messageBtn.isHidden = false
            }
            let myFormatte = DateFormatter()
                   myFormatte.dateFormat = "yyyy-MM-DD"
            let finalDate : Date = myFormatte.date(from: propertyArray.user_info!.dob)!
                   let now = Date()
                   let calendar = Calendar.current
                   let ageComponents = calendar.dateComponents([.year], from: finalDate, to: now)
                           let age = ageComponents.year
            cell.nameAgeLbl.text = "\(self.propertyArray.name), \(age!)"
            cell.likeButton.tag = 201
            cell.likeButton.addTarget(self, action: #selector(liked(_:)), for: .touchUpInside)
            cell.blockButton.addTarget(self, action: #selector(liked(_:)), for: .touchUpInside)
            cell.selectionStyle = .none
        return cell
        }
        if indexPath.row == 1 {
               let cell = tableView.dequeueReusableCell(withIdentifier: "OtherUserProfession") as! OtherUserProfession
            let profession = self.propertyArray.user_info?.profession ?? ""
             let commitment = self.propertyArray.user_info?.commitment_type ?? ""
            let location = self.propertyArray.user_info?.city ?? ""
            let education = self.propertyArray.user_info?.education ?? ""
            
            if profession == "" {
                
                cell.professionHeight.constant = 0
               
            }else{
                Height+=30
                cell.professionHeight.constant = 30
                cell.profession.text = profession
            }
            
            
            if education == "" {
                
                cell.educationViewHeight.constant = 0
                
               
                
            }else{
                Height+=30
                cell.educationViewHeight.constant = 30
                cell.education.text = education
            }
           if commitment == "" {
                          
                          cell.commitmentHeight.constant = 0
                           
                         
                      }else{
            Height+=30
                          cell.commitmentHeight.constant = 30
                          cell.commitmentType.text = commitment
                      }
            if location == "" {
                
                cell.locationHeight.constant = 0
              
               
            }else{
                Height+=30
                cell.locationHeight.constant = 30
                cell.location.text = location
            }
          
          
            
            cell.selectionStyle = .none
               return cell
               }
        if indexPath.row == 2 {
                      let cell = tableView.dequeueReusableCell(withIdentifier: "OtherUserAboutme") as! OtherUserAboutme
            cell.aboutMeText.text = self.propertyArray.user_info?.aboutme ?? ""
            cell.selectionStyle = .none
                      return cell
                      }
        
    
    else {
           let cell = tableView.dequeueReusableCell(withIdentifier: "OtherUserIntrest") as! OtherUserIntrest
            var intrest = self.propertyArray.user_info?.interest ?? ""
            cell.Intrestarr =  intrest.components(separatedBy: ",")
           
            cell.selectionStyle = .none
           return cell
           }

}
    
    @objc func liked(_ sender:UIButton){
         sender.isSelected = !sender.isSelected
     
        if sender.tag == 201{
            if sender.isSelected{
                sender.tintColor = .red
                
                self.callLikeApi(islike: "like",otherUserId: "\(self.propertyArray.id)",isBlock: "")
            }
            else{
                sender.tintColor = .lightGray
                self.callLikeApi(islike: "unlike",otherUserId: "\(self.propertyArray.id)",isBlock: "")
            }
        }
        else{
            if sender.isSelected{
                sender.tintColor = .black
                self.callLikeApi(islike: "",otherUserId: "\(self.propertyArray.id)",isBlock: "block")
            }
            else{
                sender.tintColor = .lightGray
               self.callLikeApi(islike: "",otherUserId: "\(self.propertyArray.id)",isBlock: "unblock")
            }
        }
        
    }
    
    @objc func messageTapped(_ sender: UIButton){
          self.performSegue(withIdentifier: "chat_matches", sender: self)
    }
    
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        sender.pulsate(sender: sender)
        dismiss(animated: true, completion: nil)
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if(segue.identifier == "chat_matches"){
                let displayVC = segue.destination as! ChatViewController
                displayVC.name = "\(propertyArray.name)"
         displayVC.userId = propertyArray.id as! Int
         displayVC.userImg = propertyArray.profile_image
         displayVC.modalPresentationStyle = .overFullScreen
         displayVC.modalTransitionStyle = .crossDissolve
        }
    }
}

extension UIImage {
    convenience init?(url: URL?) {
        guard let url = url else { return nil }

        do {
            let data = try Data(contentsOf: url)
            self.init(data: data)
        } catch {
            print("Cannot load image from url: \(url) with error: \(error)")
            return nil
        }
    }
}



