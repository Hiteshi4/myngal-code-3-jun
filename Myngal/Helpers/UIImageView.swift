//
//  KeyboardManager.swift
//  Myngal
//
//  Created by SONU on 25/12/19.
//  Copyright © 2019 SONU. All rights reserved.
//

import UIKit
import Kingfisher

extension UIImageView {
    func withTintColor(_ tintColor : UIColor?) {
        if let tintColor = tintColor {
            let image = self.image?.withRenderingMode(.alwaysTemplate)
            self.image = image
            self.tintColor = tintColor
        }
    }
    
    func setImage(from urlOfImage: String, placeholderImage: UIImage? = nil) {
        guard let url = URL(string: urlOfImage) else { return }
        self.kf.setImage(
            with: url,
            placeholder: placeholderImage,
            options: [
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
            ]
        )
    }
}

extension UIImage {
    func toString() -> String? {
        let data: Data? = self.pngData()
        return data?.base64EncodedString(options: .endLineWithLineFeed)
    }
}

extension UILabel {
    func textWidth() -> CGFloat {
        return UILabel.textWidth(label: self)
    }
    
    class func textWidth(label: UILabel) -> CGFloat {
        return textWidth(label: label, text: label.text!)
    }
    
    class func textWidth(label: UILabel, text: String) -> CGFloat {
        return textWidth(font: label.font, text: text)
    }
    
    class func textWidth(font: UIFont, text: String) -> CGFloat {
        let myText = text as NSString
    
        let rect = CGSize(width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude)
        let labelSize = myText.boundingRect(with: rect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(labelSize.width)
    }
    
}

       
       
extension String{
    func toAge(dob:String) -> String {
        let myFormatte = DateFormatter()
               myFormatte.dateFormat = "yyyy-MM-DD"
        let finalDate : Date = myFormatte.date(from:dob) ?? Date()
               let now = Date()
               let calendar = Calendar.current
               let ageComponents = calendar.dateComponents([.year], from: finalDate, to: now)
                    let age = ageComponents.year
return "\(age!)"
    }
}

extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)

        return ceil(boundingBox.height)
    }

    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)

        return ceil(boundingBox.width)
    }
}
extension Notification.Name{
    static let noInternet = Notification.Name("NO_INTERNET")
    static let internetConnected = Notification.Name("INTERNET_CONNECTED")
    static let getRefinedProducts = Notification.Name("callRefineProductsApi")
}
