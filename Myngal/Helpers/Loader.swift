//
//  Loader.swift
//  Myngal
//
//  Created by SONU on 04/01/20.
//  Copyright © 2020 SONU. All rights reserved.
//

import Foundation
import NVActivityIndicatorView
import MapKit
import CoreLocation

class BaseViewController: UIViewController{
    // for user current location
    let locationManager = CLLocationManager()
    var backgroundView = UIView()

       var activity :NVActivityIndicatorView?
       
       override func viewDidLoad() {
           super.viewDidLoad()
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = UIColor.red
            view.addSubview(statusbarView)
          
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
          
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = UIColor.red
        }
           // Do any additional setup after loading the view.
       }
    
    func showActivity() {
           
           let window = UIApplication.shared.keyWindow!
           
        backgroundView = UIView(frame: window.frame)
        backgroundView.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        
         window.addSubview(backgroundView)
      let backgroundView1 = GradientButton(frame: CGRect(x: (UIScreen.main.bounds.size.width-75)/2, y: (UIScreen.main.bounds.size.height-75)/2, width: 80, height: 80))
        backgroundView1.layer.cornerRadius = 40
        backgroundView1.topGradientColor = #colorLiteral(red: 1, green: 0.07843137255, blue: 0, alpha: 1)
        backgroundView1.bottomGradientColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
           backgroundView.addSubview(backgroundView1)
           self.view.isUserInteractionEnabled = false
           
           activity = NVActivityIndicatorView(frame: CGRect(x:0, y: 0, width: 75, height: 75), type: NVActivityIndicatorType.ballTrianglePath, color: UIColor.white, padding: 10)
           backgroundView1.addSubview(activity!)
           activity?.startAnimating()
       }
       func hideActivity()  {
           backgroundView.removeFromSuperview()
           self.view.isUserInteractionEnabled = true
           activity?.stopAnimating()
       }
    
    
    //MARK: - Email Validation
//    func isValidEmail(emailRegx : String) -> Bool {
//        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
//        let range = emailRegx.range(of: emailRegEx, options: .regularExpression)
//        let result = range != nil ? true : false
//        return result
//    }

    //---------------------------------------------------------------------
    
    //MARK: - Alert Method
    
    func alert(title : String , message : String , cancelButton : String) {
        let alert : UIAlertView = UIAlertView(title: title as String, message: message as String, delegate: nil, cancelButtonTitle: cancelButton as String)
        alert.show()
    }
    func showAlert(_ text:String){
     
        let alertController = UIAlertController(title: "Myngal", message: text, preferredStyle: .alert)
        let actionOK = UIAlertAction(title: "OK", style: .cancel) { (action) in
            alertController.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(actionOK)
        present(alertController, animated: true, completion: nil)
        
    }
    func showAlert(_ title:String,_ message:String,_ style:UIAlertController.Style, _ buttons:[String],_ callback:@escaping (_ tappedIndex:Int)->Void){
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let actionCancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            alertController.dismiss(animated: true, completion: nil)
            callback(11)
        }
        alertController.addAction(actionCancel)
        
        for button in buttons{
            let actionButton = UIAlertAction(title: button, style: .default) { (action) in
                alertController.dismiss(animated: true, completion: nil)
                callback(buttons.firstIndex(of: button)!)
            }
            alertController.addAction(actionButton)
        }
        
        present(alertController, animated: true, completion: nil)
        
    }
    
    
    //MARK: - User Defaults (Set/Get)
    func saveDataIntoUserDefault (object : AnyObject, key : String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(object, forKey:key)
        UserDefaults.standard.synchronize()
    }

    func updateUserDetail(value : String , valueForKey : String) {
        
        var dict : NSMutableDictionary = NSMutableDictionary()
        dict = (UserDefaults.standard.value(forKey: "kUserData") as! NSDictionary).mutableCopy() as! NSMutableDictionary
        dict.setValue(value, forKey: valueForKey)
        self.saveDataIntoUserDefault(object: dict, key: "kUserData")
    }

    func removeUserDefaults (key : String) {
        let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    //MARK: - User Detail From Defauls
    func getUserDetail (key : String) -> String {
        
        var dict : NSDictionary = NSDictionary()
        
        if (UserDefaults.standard.value(forKey: "kUserData") != nil) {
            
            dict = UserDefaults.standard.value(forKey: "kUserData") as! NSDictionary
            
            let value : String? = dict.value(forKey: key) as? String
            
            guard let
                letValue = value, !letValue.isEmpty else {
                    print(":::::::::-Value Not Found-:::::::::::")
                    return ""
            }
            
            return value!
        }
        
        return ""
        
    }
    
    //------------------------------------------------------
    
    //MARK: - Get Device Token
    func getDeviceToken () -> String {
        
        
        if (UserDefaults.standard.value(forKey: "kDeviceToken") != nil) {
            
            let deviceToken : String? = UserDefaults.standard.value(forKey: "kDeviceToken") as? String
            
            
            guard let
                letValue = deviceToken, !letValue.isEmpty else {
                    print(":::::::::-Device Token Not Found-:::::::::::")
                    return "0"
            }
            return deviceToken!
        }
        return "0"
    }
    
}


