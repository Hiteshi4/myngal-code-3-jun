//
//  KeyboardManager.swift
//  Myngal
//
//  Created by SONU on 25/12/19.
//  Copyright © 2019 SONU. All rights reserved.
//

import Foundation
import IQKeyboardManagerSwift
//import DropDown

class Keyboard {
    
    private init() {}
    
    static let shared = Keyboard()
    
    func setUp() {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "Done"
        //DropDown.startListeningToKeyboard()
    }
    
}
