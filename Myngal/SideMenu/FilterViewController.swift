//
//  FilterViewController.swift
//  Myngal
//
//  Created by SONU on 01/01/20.
//  Copyright © 2020 SONU. All rights reserved.
//

import UIKit
import RangeSeekSlider
import CoreData

class FilterViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,RangeSeekSliderDelegate {
    
    let apiMapper = APIMapper()
    let common = Common()
    var ResponseData: FilterData?
    var filterData:Filter_data?
    
    var professionUpdated:String = ""
    var commitmentUpdated:String = ""
    
    
    @IBOutlet weak var tbleView: UITableView!
    
    override func viewDidLoad(){
        let fetchdata = CoreDataManager.shared.fetchDataFromFilterData()
        print("fetchdata/////\(fetchdata)")
        for item in fetchdata!
        {
            
            print("itemitemitemitemitemitemitemitem\(item)")
            self.ResponseData = item
             professionUpdated = item.profession!
            commitmentUpdated = item.commitment!
            
            
        }
        
//        let  userData = CoreDataManager.shared.fetchDataFromUserDetail()
//        for item in userData!
//               {
//
//                   print("itemitemitemitemitemitemitemitem\(item)")
//                 //var det = item
//                professionUpdated = item.profession!
//                commitmentUpdated = item.commitment!
//
//               }
        //let detailData = userData?[0] as! UserDetail
       
        print("ResponseData== \(ResponseData)")
        super.viewDidLoad()
        tbleView.estimatedRowHeight = 60
        tbleView.rowHeight = 120
        
        tbleView.reloadData()
    }
    
    func CallfilterApi(){
        self.showActivity()
        
        let index = IndexPath(row: 0, section: 0)
        let cell: Showme = self.tbleView.cellForRow(at: index) as! Showme
        var gender:String = ""
        if cell.value == 1{
            gender = "male"
        }else{
            gender = "female"
        }
        
        
        let index1 = IndexPath(row: 1, section: 0)
        let cell1: DistanceCell = self.tbleView.cellForRow(at: index1) as! DistanceCell
        let distance = cell1.kmvalue
        
        let index2 = IndexPath(row: 2, section: 0)
        let cell2: ageRange = self.tbleView.cellForRow(at: index2) as! ageRange
        let age_range_start = cell2.ageStart
        let age_range_end = cell2.ageEnd
        
        let index3 = IndexPath(row: 3, section: 0)
        let cell3: HeightRange = self.tbleView.cellForRow(at: index3) as! HeightRange
        let height_range_start =   cell3.heightStrt
        let height_range_end =   cell3.heightEnd
        
        let param : [String:Any] = [
            
            "gender" : gender,
            "max_distance": "\(distance)",
            "age_range_start" : age_range_start,
            "age_range_end" : age_range_end,
            "height_range_start" : height_range_start,
            "height_range_end" : height_range_end,
            "profession": professionUpdated,
            "commitment_type": commitmentUpdated
            
            
        ]
        apiMapper.filterData(param, success: { (SucessDict) in
            CoreDataManager.shared.deleteDataFromFilterData()
            self.hideActivity()
            print("Parametere:=----------\(param)")
            print(SucessDict)
            let message = SucessDict["message"] as! String
            var view: MenuViewController = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
            let data = SucessDict["data"] as! [String:AnyObject]
            // self.updateUserData()
            //       let filterdData = Filter_data(dictionary: data)
            //        self.filterData = filterdData
            //        self.ResponseData?.ageRangeStart = self.filterData?.age_range_start
            //        self.ResponseData?.ageRangeEnd = self.filterData?.age_range_end
            //        self.ResponseData?.gender = self.filterData?.gender
            //        self.ResponseData?.maxDistance = self.filterData?.max_distance
            //        self.ResponseData?.heightRangeStart = self.filterData?.height_range_start
            //        self.ResponseData?.heightRangeEnd = self.filterData?.height_range_end
            CoreDataManager.shared.saveDatainFilterData(data: SucessDict["data"] as! NSDictionary)
            self.showToast(message: message)
        }) { (Failure) in
            self.hideActivity()
            self.showAlert(Failure)
        }
    }
    
//    func updateUserData(){
//         let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        let context = appDelegate.persistentContainer.viewContext
//        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "UserDetail")
//        //request.predicate = NSPredicate(format: "age = %@", "12")
//        request.returnsObjectsAsFaults = false
//        do {
//            let result = try context.fetch(request)
//            let objData = result as! [UserDetail]
//            let item = objData[0] as! UserDetail
//            item.profession = professionUpdated
//            item.commitment = commitmentUpdated
//           // return result as! [UserDetail]
//            //                   for data in result as! [LocalNotification] {
//            //
//            //                 }
//
//        } catch {
//
//            print("Failed")
//        }
//    }
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Showme") as! Showme
            let gender = self.ResponseData?.gender as? String ?? ""
            
            if gender == "female"{
                print("female.......")
                cell.femaleTapped(cell.femaleBtn)
            }else{
                print("male.......")
                cell.MaleTapped(cell.maleBtn)
            }
            
            return cell
        }
        if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DistanceCell") as! DistanceCell
            
            cell.kmLbl.text! = "\(self.ResponseData?.maxDistance as? String ?? "0") km"
            cell.slider.value = Float(self.ResponseData?.maxDistance ?? "0") ?? 0
            
            return cell
            
        }
        if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ageRange") as! ageRange
            cell.AgeRangeLbl.text = "\(ResponseData?.ageRangeStart ?? "18") - \(ResponseData?.ageRangeEnd ?? "40")year "
            cell.slider.minValue = 18
            cell.slider.maxValue = 40
            cell.slider.selectedMinValue = CGFloat(Float(self.ResponseData?.ageRangeStart ?? "18") ?? 0)
            cell.slider.selectedMaxValue = CGFloat(Float(self.ResponseData?.ageRangeEnd ?? "40") ?? 40)
            
            return cell
        }
        if indexPath.row == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeightRange") as! HeightRange
            //  cell.heightLbl.text! = "\(ResponseData?.heightRangeStart as? String ?? "0'0")-  \(ResponseData?.heightRangeEnd as? String ?? "8'0") feet"
            //cell.sliderHeight.minValue = 0
            // cell.sliderHeight.maxValue = 8
            let heightStrt = CGFloat(Float(ResponseData?.heightRangeStart ?? "") ?? 0.0)
            let heightEnd = CGFloat(Float(ResponseData?.heightRangeEnd ?? "") ?? 0.0)
            let inches  = "\((Int32(heightStrt) ?? 0)%12)"
            let feet =   "\((Int32(heightStrt) ?? 0)/12)"
            let height = "\(feet)"+"'"+"\(inches)"+"''"
            let inches1  = "\((Int32(heightEnd) ?? 0)%12)"
            let feet1 =   "\((Int32(heightEnd) ?? 0)/12)"
            let height1 = "\(feet1)"+"'"+"\(inches1)"+"''"
            cell.sliderHeight.selectedMinValue = heightStrt
            cell.sliderHeight.selectedMaxValue = heightEnd
            cell.heightLbl.text! = "\(height) Feet - \(height1) Feet "
            return cell
        }
        if indexPath.row == 4{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Profession") as! Profession
            cell.arrowBtn.tag = 4
            cell.arrowBtn.addTarget(self, action:  #selector(arrowBtnClk(_:)) ,for: .touchUpInside)
            cell.titleLbl.text = professionUpdated == "" ? "Profession" : professionUpdated
            return cell
            
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Commitment") as! Commitment
            cell.arrowBtn.tag = 5
            cell.arrowBtn.addTarget(self, action:  #selector(arrowBtnClk(_:)) ,for: .touchUpInside)
            cell.titleLbl.text =  commitmentUpdated == "" ? "Commitment" : commitmentUpdated
            return cell
            
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 120
        case 4:
            return 100
        case 5:
            return 100
        default:
            return CGFloat(120)
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 4:
            print("index Tapped")
        case 5:
            print("index Tapped")
        default:
            break
        }
    }
    
    @objc func arrowBtnClk(_ sender:UIButton)
    {
        if sender.tag == 4
        {
            let storybrd = UIStoryboard(name: "Main", bundle: nil)
            let obj = storybrd.instantiateViewController(withIdentifier: "ProfessionVc") as! MyProfessionViewController
            obj.delegate = self
            obj.callType = "Edit"
            obj.modalPresentationStyle = .overFullScreen
            obj.modalTransitionStyle = .crossDissolve
            self.present(obj, animated: true) {
                
            }
        }
        else if sender.tag == 5
        {
            let storybrd = UIStoryboard(name: "Main", bundle: nil)
            let obj = storybrd.instantiateViewController(withIdentifier: "CommitmentVc") as! MyCommitmentViewController
            obj.delegate = self
            obj.callType = "Edit"
            obj.modalPresentationStyle = .overFullScreen
            obj.modalTransitionStyle = .crossDissolve
            self.present(obj, animated: true) {
                
            }
        }
    }
    
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
        sender.pulsate(sender: sender)
        print("tapped")
        dismiss(animated: true, completion: nil)
    }
    @IBAction func SaveAndUpdateClicked(_ sender: UIButton) {
        CallfilterApi()
    }
}
class Showme: UITableViewCell{
    var value :Int = 0
    @IBOutlet weak var femaleBtn: UIButton!
    @IBOutlet weak var maleBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    @IBAction func MaleTapped(_ sender: UIButton){
        
        maleBtn.isSelected = !maleBtn.isSelected
        if sender.isSelected{
            self.value = 1
            if #available(iOS 13.0, *) {
                maleBtn.setImage(UIImage(systemName: "checkmark.square.fill"), for: .selected)
                femaleBtn.setImage(UIImage(systemName: "square"), for: .selected)
            } else {
                maleBtn.setImage(UIImage(named: "checkmark.square.fill"), for: .selected)
                femaleBtn.setImage(UIImage(named: "square"), for: .normal)
            }
            
            maleBtn.tintColor = #colorLiteral(red: 1, green: 0.07843137255, blue: 0, alpha: 1)
            femaleBtn.tintColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            
        }else{
            if #available(iOS 13.0, *) {
                femaleBtn.setImage(UIImage(systemName: "square"), for: .normal)
            } else {
                femaleBtn.setImage(UIImage(named: "square"), for: .normal)
                // Fallback on earlier versions
            }
            
            maleBtn.tintColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            
        }
        
        //
    }
    
    @IBAction func femaleTapped(_ sender: UIButton) {
        
        femaleBtn.isSelected = !femaleBtn.isSelected
        if sender.isSelected{
            self.value = 2
            if #available(iOS 13.0, *) {
                femaleBtn.setImage(UIImage(systemName: "checkmark.square.fill"), for: .selected)
                maleBtn.setImage(UIImage(systemName: "square"), for: .selected)
            } else {
                femaleBtn.setImage(UIImage(named: "checkmark.square.fill"), for: .selected)
                maleBtn.setImage(UIImage(named: "square"), for: .normal)
            }
            
            femaleBtn.tintColor = #colorLiteral(red: 1, green: 0.07843137255, blue: 0, alpha: 1)
            maleBtn.tintColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            
        }else{
            if #available(iOS 13.0, *) {
                femaleBtn.setImage(UIImage(systemName: "square"), for: .selected)
            } else {
                femaleBtn.setImage(UIImage(named: "square"), for: .normal)
                // Fallback on earlier versions
            }
            
            femaleBtn.tintColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            
        }
        
        //
    }
    
}

class DistanceCell: UITableViewCell{
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var kmLbl: UILabel!
    var kmvalue:Int = 0
    
    
    @IBAction func valueChanged(_ sender: UISlider) {
        let currentValue = Int(sender.value)
        
        kmLbl.text = "\(currentValue) km"
        kmvalue = currentValue
    }
}


class ageRange: UITableViewCell{
    
    @IBOutlet weak var slider: RangeSeekSlider!
    
    @IBOutlet weak var AgeRangeLbl: UILabel!
    var ageStart:String = "18"
    var ageEnd:String = "40"
    override func awakeFromNib() {
          super.awakeFromNib()
        slider.delegate = self
        
      }
  
   
}


extension ageRange: RangeSeekSliderDelegate{
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        
        
        let minAge  = "\(Int32(minValue))"
        let maxAge =   "\(Int32(maxValue))"
        
        ageStart = "\(minAge)"
        ageEnd = "\(maxAge)"
        
        AgeRangeLbl!.text! = "\(minAge)-\(maxAge)year"
        print("Standard slider updated. Min Value: \(minValue) Max Value: \(maxValue)")
    }
    
    func didStartTouches(in slider: RangeSeekSlider) {
        print("did start touches")
    }
    
    func didEndTouches(in slider: RangeSeekSlider) {
        print("did end touches")
    }
}

//class HeightFormatter : String{
//    override func string(from number: NSNumber) -> String? {
//        let feet = number.int32Value/12
//        let inches = number.int32Value % 12
//
//        let height = "\(feet)"+"'"+"\(inches)"+"\""
//        return height
//    }
//}

extension String{
    func heightChange (value:String) -> String {
        let inches  = "\((Int32(value) ?? 0)%12)"
        let feet =   "\((Int32(value) ?? 0)/12)"
        let height = "\(feet)"+"'"+"\(inches)"+"''"
    return height
    }
}


class HeightRange: UITableViewCell{
    
    var heightStrt = "0"
    var heightEnd = "0"
    
    @IBOutlet weak var sliderHeight: RangeSeekSlider!
    @IBOutlet weak var heightLbl: UILabel!
    override func awakeFromNib() {
             super.awakeFromNib()
           sliderHeight.delegate = self
           
         }
}

extension HeightRange: RangeSeekSliderDelegate {

    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
      
        let inches  = "\(Int32(minValue)%12)"
        let feet =   "\(Int32(minValue)/12)"
        let height = "\(feet)"+"'"+"\(inches)"+"''"
        
        let inches1  = "\(Int32(maxValue)%12)"
            let feet1 =   "\(Int32(maxValue)/12)"
            let height1 = "\(feet1)"+"'"+"\(inches1)"+"''"
     
        heightStrt = "\(minValue)"
        heightEnd = "\(maxValue)"
        
      
        
        
        
          heightLbl!.text! = "(\(height)) - (\(height1))feet"
        print("Standard slider updated. Min Value: \(minValue) Max Value: \(maxValue)")
    }

    func didStartTouches(in slider: RangeSeekSlider) {
        print("did start touches")
    }

    func didEndTouches(in slider: RangeSeekSlider) {
        print("did end touches")
    }
}



class Profession : UITableViewCell{
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var arrowBtn: UIButton!
}
class Commitment : UITableViewCell{
    @IBOutlet weak var titleLbl: UILabel!
       
       @IBOutlet weak var arrowBtn: UIButton!
}
extension FilterViewController:MyProfessionDelegate,MyCommitmentProtocol
{
    func getCommitment(commitment: String) {
        commitmentUpdated = commitment
        tbleView.reloadData()
    }
    
    func getProfession(profession: String) {
        print(profession)
        professionUpdated = profession
        tbleView.reloadData()
    }

}
