//
//  EditProfileViewController.swift
//  Myngal
//
//  Created by SONU on 27/01/20.
//  Copyright © 2020 SONU. All rights reserved.
//




import UIKit
import ActionSheetPicker_3_0
import SDWebImage
import Alamofire

class EditProfileViewController: BaseViewController ,UIImagePickerControllerDelegate , UINavigationControllerDelegate  ,UIActionSheetDelegate {
    var picker = UIImagePickerController();
    var imagePicked = 0
    
    var arrHead = ["Name","Date of Birth","Gender","Company","Job Title","Education","School","About Me","Interest","Location","Profession","Commitment Type"]
    var updatedData:NSMutableArray = ["Name","Date of Birth","Gender","Company","Job Title","Education","School","About Me","Interest","Location","Profession","Commitment Type"]
    var arrIcon:[UIImage] = [#imageLiteral(resourceName: "profile"),#imageLiteral(resourceName: "calendra_icon"),#imageLiteral(resourceName: "loation_icon"),#imageLiteral(resourceName: "comapny_icon"),#imageLiteral(resourceName: "job_icon"),#imageLiteral(resourceName: "education_icon"),#imageLiteral(resourceName: "school_bag"),#imageLiteral(resourceName: "invite"),#imageLiteral(resourceName: "job_icon"),#imageLiteral(resourceName: "job_icon"),#imageLiteral(resourceName: "job_icon"),#imageLiteral(resourceName: "job_icon"),#imageLiteral(resourceName: "job_icon")]
    var arrData = Dictionary<String,Any>()
    var coverImag = UIImage()
    var ProfileImg = UIImage()
    var image1 = UIImage()
    var image2 = UIImage()
    var image3 = UIImage()
    var image4 = UIImage()
    var imagesArr:[UIImage] = []
    var arrimg:NSMutableArray = [UIImage(),UIImage(),UIImage(),UIImage()]
    
    var pickImageCallback : ((UIImage) -> ())?;
    var index : IndexPath = IndexPath()
    let common = Common()
    
    @IBOutlet weak var addProfileImage: UIButton!
    var propertyArray: ProfileUpdateModel?
    var ResponseData: Data2?
    var userDetails:UserDetail?
    let apiMapper = APIMapper()
    
    
    @IBOutlet weak var profileImag: UIImageView!
    var imagePicker : UIImagePickerController! = UIImagePickerController()
    
    var actionSheet : UIActionSheet! = UIActionSheet()
    
    @IBOutlet weak var tblView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(userDetails)
        print(ResponseData)
        profileImag.layer.borderWidth = 1.5
        let color = #colorLiteral(red: 1, green: 0.07843137255, blue: 0, alpha: 1)
        profileImag.layer.borderColor = color.cgColor
        
        tblView.register(UINib(nibName: "UserInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "UserInfoCell")
        tblView.register(UINib(nibName: "TextFieldTableViewCell", bundle: nil), forCellReuseIdentifier: "TextFieldTableViewCell")
        tblView.register(UINib(nibName: "UploadPhotoCell", bundle: nil), forCellReuseIdentifier: "UploadPhotoCell")
        //tblView.register(TextFieldTableViewCell.self, forCellReuseIdentifier: "TextFieldCell")
        tblView.register(UINib(nibName: "UploadCoverPhotoCell", bundle: nil), forCellReuseIdentifier: "UploadCoverPhotoCell")
        
        
        tblView.register(UINib(nibName: "GenderSelectionCell", bundle: nil), forCellReuseIdentifier: "GenderSelectionCell")
        let profileImageURL = self.ResponseData?.profileImage ?? ""
        profileImag.setImage(from: AppUrls.imagebaseUrl+"\(profileImageURL)", placeholderImage: #imageLiteral(resourceName: "man1"))
        tblView.delegate = self
        tblView.dataSource = self
        // self.tblView.reloadData()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tblView.reloadData()
    }
    
    func ProfileUpdateApi(profileImage:UIImage,images:[UIImage],success:@escaping (NSDictionary) -> Void, failure:@escaping (Error) -> Void)
    {
        
    
        var parameters =
            [
                "name": (updatedData[0] as! String) == arrHead[0] ? ResponseData?.name! : updatedData[0] as! String,
                "dob": (updatedData[1] as! String) == arrHead[1] ? userDetails?.dob : updatedData[1] as! String,
                // "gender": (updatedData[2] as! String) == arrHead[2] ? userDetails?.gender : updatedData[2] as! String,
                  "company": (updatedData[3] as! String) == arrHead[3] ? userDetails?.company : updatedData[3] as! String,
                   "job_title": (updatedData[4] as! String) == arrHead[4] ? userDetails?.jobTitle : updatedData[4] as! String,
                    "education":"BTech" /*(updatedData[5] as! String) == arrHead[5] ? userDetails?.education : updatedData[5] as! String*/,
                     "school": (updatedData[6] as! String) == arrHead[6] ? userDetails?.education : updatedData[6] as! String,
                     "aboutme": (updatedData[7] as! String) == arrHead[7] ? userDetails?.aboutMe : updatedData[7] as! String,
                    "intrest": (updatedData[8] as! String) == arrHead[8] ? userDetails?.interest : updatedData[8] as! String,
                
                "profession": (updatedData[10] as! String) == arrHead[10] ? userDetails?.profession : updatedData[10] as! String,
                "commitment_type": updatedData[11] as! String   == arrHead[11] ? userDetails?.commitment : updatedData[11] as! String
               
  
                ] as [String : Any]

  
        var _:NSError?
        do
        {

            let headers: HTTPHeaders = [
                "Content-type": "multipart/form-data"
          //  "Accept": "application/json"
            ]
            let url = (AppUrls.baseURL + AppUrls.editprofile+"\(self.common.getUserId())")
            print("Params:----\(parameters)")
            print("Url:-----------\(url)")
            AF.upload(multipartFormData: { multipartFormData in
                
                
                
                if self.imagePicked == 1{
                    let coverImg = self.coverImag.jpegData(compressionQuality: 0.2)!

                    multipartFormData.append(coverImg, withName: "cover_image",fileName: "\(Date().timeIntervalSince1970).png", mimeType: "image/png")

                }
                else if images.count > 0 {
                    for i in 0..<images.count
                    {
                        let imgData = images[i].jpegData(compressionQuality: 0.2)!
                        let x=i+1
                        print("profile_img\(x)")
                        multipartFormData.append(imgData, withName: "image\(x)",fileName: "\(Date().timeIntervalSince1970).png", mimeType: "image/png")
                    }
                }
                else if profileImage.size.height != 0 {
                    let profileImg = profileImage.jpegData(compressionQuality: 0.2)!

                    multipartFormData.append(profileImg, withName: "profile_image",fileName: "\(Date().timeIntervalSince1970).png", mimeType: "image/png")
               }
                
                for (key, value) in parameters {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
                
            },
                      to: url,method: .post,headers: headers).responseJSON { (resp) in
                        print(resp)
                        guard  let dictionary = resp.value as? NSDictionary else{return}
                        success(dictionary)
            }
            
        }
        catch
        {
            print("error")
        }
  
    }
    func ageLimit(age:String) -> Bool{
          var isEligible = true
        let index1 = IndexPath(row: 1, section: 0)
        let Dobcell: TextFieldTableViewCell = self.tblView.cellForRow(at: index1) as! TextFieldTableViewCell

        let myFormatte = DateFormatter()
                      myFormatte.dateFormat = "yyyy-MM-DD"
               let finalDate : Date = myFormatte.date(from:age) ?? Date()
                      let now = Date()
                      let calendar = Calendar.current
                      let ageComponents = calendar.dateComponents([.year], from: finalDate, to: now)
        
        if (ageComponents.year! < 18){

                   self.showAlert("You must be 18 years of age or older.")
            isEligible = false
            Dobcell.txtField.text = userDetails?.dob
        }
        return true

    }
    func getDateStringForDate(date : Date, andDateFormat dateformatter: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = dateformatter
        //print(formatter.string(from: date))
        return formatter.string(from: date)
        
    }
    @objc func selectDob(textField: UITextField) {
        let index1 = IndexPath(row: 1, section: 0)
        let currentDate = Date()
        let Dobcell: TextFieldTableViewCell = self.tblView.cellForRow(at: index1) as! TextFieldTableViewCell
       // self.view.endEditing(true)
      ActionSheetDatePicker.show(withTitle: "Select DOB ", datePickerMode: .date, selectedDate: Date(), minimumDate: nil, maximumDate: nil, doneBlock: { (picker, date, selectedValue) in
            
            Dobcell.txtField.text = self.getDateStringForDate(date: date as! Date, andDateFormat: DateConstants.dateFormat)
            //self.view.endEditing(false)
           // self.updatedData.replaceObject(at: 1, with: Dobcell.txtField.text)
            print("Date Select")
             self.ageLimit(age: textField.text!)
             self.view.endEditing(true)
       
        }, cancel: { (picker) in
            
        }, origin: Dobcell.txtField)
      //  picker?.Calendar.date(byAdding: .year, value: -1, to: Date())
    }
    
    @objc func arrowBtnClk(_ sender:UIButton)
    {
        if sender.tag == 8
        {
            let storybrd = UIStoryboard(name: "Main", bundle: nil)
            let obj = storybrd.instantiateViewController(withIdentifier: "IntrestVc") as! MyIntrestViewController
            obj.delegate = self
            obj.callType = "Edit"
            obj.modalPresentationStyle = .overFullScreen
            obj.modalTransitionStyle = .crossDissolve
            self.present(obj, animated: true) {
                
            }
            
//            PresentTo(.Main,  .IntrestVc){ (vc) in
//                       print(vc)
//                   }
        }
        else if sender.tag == 10
        {
            let storybrd = UIStoryboard(name: "Main", bundle: nil)
            let obj = storybrd.instantiateViewController(withIdentifier: "ProfessionVc") as! MyProfessionViewController
            obj.delegate = self
            obj.callType = "Edit"
            obj.modalPresentationStyle = .overFullScreen
            obj.modalTransitionStyle = .crossDissolve
            self.present(obj, animated: true) {
                
            }
        }
        else if sender.tag == 11
        {
              let storybrd = UIStoryboard(name: "Main", bundle: nil)
                      let obj = storybrd.instantiateViewController(withIdentifier: "CommitmentVc") as! MyCommitmentViewController
                      obj.delegate = self
                      obj.callType = "Edit"
                      obj.modalPresentationStyle = .overFullScreen
                      obj.modalTransitionStyle = .crossDissolve
                      self.present(obj, animated: true) {
                          
                      }
        }
    }
    
//
//    @objc func navigateIntrest(_ sender: UIButton){
//
//        let index2 = IndexPath(row: 8, section: 0)
//
//        let intrstCell: UserInfoTableViewCell = self.tblView.cellForRow(at: index2) as! UserInfoTableViewCell
//        intrstCell.descrptionLbl.text = "<oviews"
//        PresentTo(.Main,  .IntrestVc){ (vc) in
//            print(vc)
//        }
//    }
//    @objc func navigateProfession(_ sender: UIButton){
//
//
//        PresentTo(.Main,  .ProfessionVc){ (vc) in
//
//        }
//    }
//    @objc func navigateCommitment(_ sender: UIButton){
//
//        PresentTo(.Main,  .CommitmentVc){ (vc) in
//        }
//    }
    
    @IBAction func AddProfileClicked(_ sender: UIButton) {
        imagePicked = 1
        ShowAlert(sender)
        print("Add profile Clicked")
    }
    @IBAction func UpdateBtnTapped(_ sender: UIButton) {
        self.showActivity()
        self.ProfileUpdateApi(profileImage: profileImag.image!, images: imagesArr, success: {
            (sucessDict) in
            print(sucessDict)
           
          
         
            let message = sucessDict["message"] as! String
            self.showToast(message: message)
            if let dictionary = sucessDict as? [String : Any]{
               
              print(dictionary)
            
                   
             self.hideActivity()
                    
            
                CoreDataManager.shared.saveDatainUserDetail(data:  dictionary["user_data"] as! NSDictionary)
                                   
                    
                   
                   // if CoreDataManager.shared.entityIsEmpty(entity: "Data2")
                   // {
                        CoreDataManager.shared.saveDatainData2(data:  dictionary["data"] as! NSDictionary)
                  self.dismiss(animated: true, completion: nil)
                       
                 // }
   
                }
            
            self.hideActivity()
            //self.showToast(message: message)
            print("propertyArraypropertyArraypropertyArray    --=\(self.propertyArray)")
        }) { (Error) in
            print(Error)
             self.hideActivity()
            self.showAlert(Error as? String ?? "")
        }
        
    }
    
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
         sender.pulsate(sender: sender)
        dismiss(animated: true, completion: nil)
    }
    
    @objc func ShowAlert(_ button:UIButton){
        imagePicked = button.tag
        
        pickImage(button) { (img) in
            
            if button.tag == 131
            {
                self.image1 = img
                self.imagesArr.append(self.image1)
                self.arrimg.replaceObject(at: 0, with: img)
                
            }
            else if button.tag == 132
            {
                self.image2 = img
                self.imagesArr.append(self.image2)
                self.arrimg.replaceObject(at: 1, with: img)
                
            }
            else if button.tag == 133
            {
                self.image3 = img
                self.imagesArr.append(self.image3)
                self.arrimg.replaceObject(at: 2, with: img)
                
            }
            else if button.tag == 134
            {
                self.image4 = img
                self.imagesArr.append(self.image4)
                self.arrimg.replaceObject(at: 3, with: img)
                
            }
            else if button.tag == 141
            {
                self.ProfileImg = img
                self.profileImag.image = self.ProfileImg
                
            }
            else
            {
                self.coverImag = img
                self.imagePicked = 1
                
                
            }
            print(self.arrimg)
            if button.tag == 12
            {
                self.tblView.reloadRows(at: [IndexPath(row: 12, section: 0)], with: .none)
            }
            else
            {
                self.tblView.reloadRows(at: [IndexPath(row: 13, section: 0)], with: .none)
            }
            
        }
        
        
        // if  button.tag == 0 {
        let alert:UIAlertController=UIAlertController(title: "Select Image", message: nil, preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
            
        {
            
            UIAlertAction in
            
            self.openCamera()
        }
        let galleryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openGallery()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in
        }
        
        self.imagePicker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(galleryAction)
        
        alert.addAction(cancelAction)
        //You must add this line then only in iPad it will not crash
        alert.popoverPresentationController?.sourceView = self.view
        self.present(alert, animated: true, completion: nil)
        
        //      }else{
        //        print("\(button.tag)  tapeeddd...")
        //        }
    }
    func openCamera()
    {
        
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            
            self.imagePicker.sourceType = .camera
            self.imagePicker.allowsEditing = true
            self.imagePicker.delegate = self
            self.present(self.imagePicker, animated: true, completion: nil)
        }
    }
    
    func openGallery()
    {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            self.imagePicker.allowsEditing = true
            self.imagePicker.sourceType = .photoLibrary
            self.imagePicker.delegate = self
            
            self.present(self.imagePicker, animated:true, completion: nil);
        }
        
    }
    
}


extension EditProfileViewController: UITableViewDelegate,UITableViewDataSource
{
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.row {
        case 2:
            return 100
        case 8:
            return 120
        case 9:
            return 120
        case 10:
            return 120
        case 11:
            return 120
        case 12:
            return 150
        case 13:
            return 150
            
        default:
            return 80
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("tapped... .. ")
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 14
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row < 8 && indexPath.row != 2
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextFieldTableViewCell", for: indexPath) as! TextFieldTableViewCell
            cell.txtField.placeholder = arrHead[indexPath.row]
            cell.icon.image = arrIcon[indexPath.row]
            //cell.selectionstyle = .none
            cell.txtField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingDidEnd)
            cell.baseVw.layer.borderColor = UIColor.gray.cgColor
            cell.baseVw.layer.borderWidth = 1
            cell.baseVw.layer.cornerRadius = 8
            print("userdata ",self.userDetails?.city)
            print("data", self.ResponseData?.name)
            cell.txtField.tag = indexPath.row
            
            if indexPath.row == 0
            {
                if arrHead[indexPath.row] == updatedData[indexPath.row] as! String
                {
                    cell.txtField.text = ResponseData?.name

                  //  updatedData.replaceObject(at: indexPath.row, with: ResponseData?.name)
                }
                else
                {
                    cell.txtField.text = updatedData[indexPath.row] as! String
                }
                
            }
            else if indexPath.row == 1
            {
               cell.txtField.addTarget(self, action: #selector(selectDob(textField:)), for: .editingDidBegin)
                if arrHead[indexPath.row] == updatedData[indexPath.row] as! String
                {
                    cell.txtField.text = userDetails?.dob
                   //updatedData.replaceObject(at: indexPath.row, with: cell.txtField.text)
                }
                else
                {
                    cell.txtField.text = updatedData[indexPath.row] as! String
                }
                
            }
            else if indexPath.row == 3
            {
                if arrHead[indexPath.row] == updatedData[indexPath.row] as! String
                {
                    cell.txtField.text = userDetails?.company
                    //updatedData.replaceObject(at: indexPath.row, with: userDetails?.company)
                }
                else
                {
                    cell.txtField.text = updatedData[indexPath.row] as! String
                }
                
            }
            else if indexPath.row == 4
            {
                if arrHead[indexPath.row] == updatedData[indexPath.row] as! String
                {
                    cell.txtField.text = userDetails?.jobTitle
                   // updatedData.replaceObject(at: indexPath.row, with: userDetails?.jobTitle)
                }
                else
                {
                    cell.txtField.text = updatedData[indexPath.row] as! String
                }
                
            }
            else if indexPath.row == 5
            {
                if arrHead[indexPath.row] == updatedData[indexPath.row] as! String
                {
                    cell.txtField.text = userDetails?.education
                   // updatedData.replaceObject(at: indexPath.row, with: userDetails?.education)
                }
                else
                {
                    cell.txtField.text = updatedData[indexPath.row] as! String
                }
                
            }
            else if indexPath.row == 6
            {
                if arrHead[indexPath.row] == updatedData[indexPath.row] as! String
                {
                    cell.txtField.text = userDetails?.school
                   // updatedData.replaceObject(at: indexPath.row, with: userDetails?.school)
                }
                else
                {
                    cell.txtField.text = updatedData[indexPath.row] as! String
                }
                
            }
            else if indexPath.row == 7
            {
                if arrHead[indexPath.row] == updatedData[indexPath.row] as! String
                {
                    cell.txtField.text = userDetails?.aboutMe
                  //  updatedData.replaceObject(at: indexPath.row, with: userDetails?.aboutMe)
                    
                }
                else
                {
                    cell.txtField.text = updatedData[indexPath.row] as! String
                }
                
            }
            return cell
        } else if indexPath.row > 7 && indexPath.row < 12
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserInfoCell", for: indexPath) as! UserInfoTableViewCell
            cell.titleLbl.text = arrHead[indexPath.row]
            cell.arrowBtn.tag = indexPath.row
            if indexPath.row == 8
            {
                cell.arrowBtn.tag = indexPath.row
                cell.arrowBtn.addTarget(self, action:  #selector(arrowBtnClk(_:)) ,for: .touchUpInside)
                if arrHead[indexPath.row] == updatedData[indexPath.row] as! String
                {
                    cell.descrptionLbl.text = userDetails?.interest
                }
                else
                {
                    cell.descrptionLbl.text = updatedData[indexPath.row] as! String
                }
                
               
            }
            else if indexPath.row == 9
            {
                cell.descrptionLbl.text = userDetails?.city
            }
            else if indexPath.row == 10
            {
                cell.arrowBtn.addTarget(self, action:  #selector(arrowBtnClk(_:)) ,for: .touchUpInside)
                if arrHead[indexPath.row] == updatedData[indexPath.row] as! String
                {
                    cell.descrptionLbl.text = userDetails?.profession
                }
                else
                {
                    cell.descrptionLbl.text = updatedData[indexPath.row] as! String
                }
                
            }
            else if indexPath.row == 11
            {
                cell.arrowBtn.addTarget(self, action:  #selector(arrowBtnClk(_:)) ,for: .touchUpInside)
                if arrHead[indexPath.row] == updatedData[indexPath.row] as! String
                {
                    cell.descrptionLbl.text = userDetails?.commitment
                }
                else
                {
                    cell.descrptionLbl.text = updatedData[indexPath.row] as! String
                }
                
            }
            
            
            return cell
        }
        else if indexPath.row == 12
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "UploadCoverPhotoCell", for: indexPath) as! UploadCoverPhotoCell
            cell.baseVw.layer.borderColor = UIColor.gray.cgColor
            cell.baseVw.layer.borderWidth = 1
            cell.baseVw.layer.cornerRadius = 8
            cell.img.image = coverImag
            cell.img.contentMode = .scaleToFill
            
            cell.btn.addTarget(self, action: #selector(ShowAlert(_:)), for: .touchUpInside)
            cell.btn.tag = indexPath.row
            return cell
        }
        else if indexPath.row == 13
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "UploadPhotoCell", for: indexPath) as! UploadPhotoCell
            
            
            cell.baseVw1.layer.borderColor = UIColor.gray.cgColor
            cell.baseVw1.layer.borderWidth = 1
            cell.baseVw1.layer.cornerRadius = 8
            cell.baseVw2.layer.borderColor = UIColor.gray.cgColor
            cell.baseVw2.layer.borderWidth = 1
            cell.baseVw2.layer.cornerRadius = 8
            cell.baseVw3.layer.borderColor = UIColor.gray.cgColor
            cell.baseVw3.layer.borderWidth = 1
            cell.baseVw3.layer.cornerRadius = 8
            cell.baseVw4.layer.borderColor = UIColor.gray.cgColor
            cell.baseVw4.layer.borderWidth = 1
            cell.baseVw4.layer.cornerRadius = 8
            cell.Img1.image = arrimg[0] as! UIImage
            cell.Img1.contentMode = .scaleToFill
            cell.Img2.image = arrimg[1] as! UIImage
            cell.Img2.contentMode = .scaleToFill
            cell.Img3.image = arrimg[2] as! UIImage
            cell.Img3.contentMode = .scaleToFill
            cell.Img4.image = arrimg[3] as! UIImage
            cell.Img4.contentMode = .scaleToFill
            
            
            cell.btn1.tag = 131
            cell.btn1.addTarget(self, action: #selector(ShowAlert(_:)), for: .touchUpInside)
            cell.btn2.tag = 132
            cell.btn2.addTarget(self, action: #selector(ShowAlert(_:)), for: .touchUpInside)
            cell.btn3.tag = 133
            cell.btn3.addTarget(self, action: #selector(ShowAlert(_:)), for: .touchUpInside)
            cell.btn4.tag = 134
            cell.btn4.addTarget(self, action: #selector(ShowAlert(_:)), for: .touchUpInside)
            
            
            return cell
        }
            
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            let baseVw = cell.viewWithTag(20) as! UIView
            let labelTitle = cell.viewWithTag(21) as! UILabel
            let labelGender = cell.viewWithTag(22) as! UILabel
            baseVw.applyViewCornerRadius(cornerRadius: 10, borderColor: .clear, borderWidth: 0)
            baseVw.applyViewShadow(shadowOffset: CGSize(width: 1, height: 1), shadowColor: .lightGray, shadowOpacity: 10)
            let capitalizedString = userDetails?.gender?.capitalizingFirstLetter()
            labelGender.text = capitalizedString
            
         //   cell.maleBtn.isHidden = true
            //cell.femaleBtn.isHidden = true
//            cell.maleBtn.isEnabled = false
//            cell.femaleBtn.isEnabled = false
//            cell.maleBtn.setImage(UIImage(named: ""), for: .normal)
//            cell.maleBtn.setTitle("Gender", for: .normal)
//            cell.femaleBtn.setTitle(userDetails?.gender, for: .normal)
//           cell.femaleBtn.setImage(UIImage(named: ""), for: .normal)
//            cell.femaleBtn.contentMode = .right
//          cell.titleCell.text = " "
//            cell.delegate = self
//            if arrHead[indexPath.row] == updatedData[indexPath.row] as! String {
                
               // cell.seletedGender = userDetails?.gender
                
//                if userDetails?.gender == "male"
//                {
//                    // cell.maleBtn.isSelected = true
//                    cell.MaleTapped(cell.maleBtn)
//                }
//                else
//                {
//                    // cell.femaleBtn.isSelected = true
//                    cell.femaleTapped(cell.femaleBtn)
//                }
//            }
//            else
//            {
//                cell.seletedGender = updatedData[indexPath.row] as! String
//            }
            
            
            return cell
        }
        
    }
    
    
    func pickImage(_ btn: UIButton, _ callback: @escaping ((UIImage) -> ())) {
        self.pickImageCallback = callback;
        //self.viewController = viewController;
        
        let alert:UIAlertController=UIAlertController(title: "Select Image", message: nil, preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
            
        {
            
            UIAlertAction in
            
            self.openCamera()
        }
        let galleryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openGallery()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in
        }
        
        self.imagePicker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(galleryAction)
        
        alert.addAction(cancelAction)
        //You must add this line then only in iPad it will not crash
        alert.popoverPresentationController?.sourceView = self.view
        self.present(alert, animated: true, completion: nil)
    }
    //      func openCamera(){
    //          alert.dismiss(animated: true, completion: nil)
    //          if(UIImagePickerController .isSourceTypeAvailable(.camera)){
    //              picker.sourceType = .camera
    //              self.viewController!.present(picker, animated: true, completion: nil)
    //          } else {
    //              let alertWarning = UIAlertView(title:"Warning", message: "You don't have camera", delegate:nil, cancelButtonTitle:"OK", otherButtonTitles:"")
    //              alertWarning.show()
    //          }
    //      }
    //      func openGallery(){
    //          alert.dismiss(animated: true, completion: nil)
    //          picker.sourceType = .photoLibrary
    //          self.viewController!.present(picker, animated: true, completion: nil)
    //      }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    //for swift below 4.2
    //func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    //    picker.dismiss(animated: true, completion: nil)
    //    let image = info[UIImagePickerControllerOriginalImage] as! UIImage
    //    pickImageCallback?(image)
    //}
    
    // For Swift 4.2+
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        pickImageCallback?(image)
    }
    
    
    
    @objc func imagePickerController(_ picker: UIImagePickerController, pickedImage: UIImage?) {
    }
    
    
}

extension EditProfileViewController:UITextFieldDelegate
{
    @objc func textFieldDidChange(_ textField: UITextField) {
        updatedData.replaceObject(at: textField.tag, with: "\(textField.text!)")
        print(updatedData)
        tblView.reloadData()
        
    }
}

extension UIImage {
    func resizeImage(targetSize: CGSize) -> UIImage {
        let size = self.size
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        let newSize = widthRatio > heightRatio ?  CGSize(width: size.width * heightRatio, height: size.height * heightRatio) : CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}

extension EditProfileViewController:MyInterestDelegate,MyProfessionDelegate,MyCommitmentProtocol,GenderSelectionProtocol
{
    func getInterest(interests: String) {
        print(interests)
        updatedData.replaceObject(at: 8, with: interests)
        tblView.reloadData()
    }
    
    func getProfession(profession: String) {
        print(profession)
        updatedData.replaceObject(at: 10, with: profession)
        tblView.reloadData()
    }
    
    func getCommitment(commitment: String) {
        print(commitment)
        updatedData.replaceObject(at: 11, with: commitment)
        tblView.reloadData()
    }
    func  getGender(gender:String) {
        print(gender)
        updatedData.replaceObject(at: 2, with: gender)
        tblView.reloadData()
    }
}


extension String {
    func capitalizingFirstLetter() -> String {
      return prefix(1).uppercased() + self.lowercased().dropFirst()
    }

    mutating func capitalizeFirstLetter() {
      self = self.capitalizingFirstLetter()
    }
}
