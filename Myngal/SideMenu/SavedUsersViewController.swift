//
//  SavedUsersViewController.swift
//  Myngal
//
//  Created by SONU on 01/01/20.
//  Copyright © 2020 SONU. All rights reserved.
//

import UIKit

class SavedUsersViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var tbleView: UITableView!
    
    
   var propertyArray =  [PreferredProduct1]()
    let common = Common()
    let apiMapper = APIMapper()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.isUserInteractionEnabled = true
       
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 0.07843137255, blue: 0, alpha: 1)]

        tbleView.register(UINib(nibName: "BlockTableViewCell", bundle: nil), forCellReuseIdentifier: "BlockCell")
              tbleView.estimatedRowHeight = 60
              tbleView.rowHeight = 100
              tbleView.isScrollEnabled = false
        
         getSavedUserApi()
    }
    func CallsaveForlaterApi(otherUserId:String = ""){
        self.showActivity()
           
           let param : [String:Any] = [
            "user_id" : common.getUserId(),
                  "other_user_id": otherUserId,
                  "is_save": "unsave"
               
           ]
           
           apiMapper.saveForlater(param, success: { (SucessDict) in
            
               self.hideActivity()
            let message = SucessDict["message"] as! String
            self.showToast(message: message)
               print("Parametere:=----------\(param)")
               
           }) { (Failure) in
               self.hideActivity()
               self.showAlert(Failure)
           }
       }
    
    func getSavedUserApi(){
       // self.showActivity()
        
        //apiMapper.let common = Common()({ (successDict) in
            let userid = common.getUserId()
            let param : [String:Any] = [
                "user_id" : userid as Any
                
            ]
            self.showActivity()
            apiMapper.savedList(param, success: { (SucessDict) in
                
                print(SucessDict)
                let status = SucessDict["status"] as? String ?? ""
                let Message = SucessDict["message"] as? String ?? ""
                self.showToast(message: Message)
                if status == "true"{
                    if let dictionary = SucessDict as? [String : AnyObject]{
                        let data = dictionary["data"] as? [[String: Any]] ?? []
                        for dict in data{
                            let propertyList = PreferredProduct1(dictionary: dict)
                            self.propertyArray.append(propertyList)

                        }
                    }

                }else{

                    self.hideActivity()
                    self.showAlert(Message)
                }
 
                self.tbleView.reloadData()
                self.hideActivity()
                
            }) { (Failure) in
                self.showAlert(Failure)
                self.hideActivity()
                      
            }

    }
  
    @IBAction func backTapped(_ sender: UIButton) {
        sender.pulsate(sender: sender)
        self.dismiss(animated: true, completion: nil)
    }
    
       
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return propertyArray.count
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           let cell = tableView.dequeueReusableCell(withIdentifier: "BlockCell") as! BlockTableViewCell
      cell.trashButton.isHidden = false
      cell.trashButton.addTarget(self, action: #selector(removeRow), for: .touchUpInside)
        cell.nameAgeLb.text = propertyArray[indexPath.row].name
               cell.profession.text = propertyArray[indexPath.row].user_info?.profession ?? ""
               let profile = propertyArray[indexPath.row].profile_image
               cell.profileImg.setImage(from: AppUrls.imagebaseUrl+"\(profile as? String ?? "")", placeholderImage: #imageLiteral(resourceName: "male_selected"))
               cell.selectionStyle = .none
        
         
           return cell
       }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            self.showAlert("Deleted")
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let userProfile = propertyArray[indexPath.row]
           PresentTo(.Main, .OtherUserProfileVC){ (vc) in
        guard let viewProfileVC = vc as? OtherUserProfileViewController else{return}
         viewProfileVC.propertyArray = userProfile
                    }
    }
    
    @objc func removeRow(){
        propertyArray.remove(at: 0)
        let indexPath = IndexPath(item: 0, section: 0)
        tbleView.deleteRows(at: [indexPath], with: .fade)
        for item in propertyArray{
            let user_id = item.id
            self.CallsaveForlaterApi(otherUserId: "\(user_id)")
        }

    }
}
