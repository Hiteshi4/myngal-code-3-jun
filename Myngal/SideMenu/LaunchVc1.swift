//
//  LaunchVc1.swift
//  Myngal
//
//  Created by GEETAM SHARMA on 02/03/20.
//  Copyright © 2020 SONU. All rights reserved.
//

import UIKit

class LaunchVc1: BaseViewController {
    @IBOutlet weak var logoImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        Up()
       //  LoginCheck()
    }
    
    func LoginCheck(){
        
       if CoreDataManager.shared.entityIsEmpty(entity: "UserDetail")
                     {
                       
                        
            Router.loadOnboardingInterface()

              }else{
         
             Router.loadMainInterface()
              }
    }

    
    func Up()  {
        let center = UIScreen.main.bounds.origin.y
        let width = self.logoImage.frame.width - 50
        UIView.animate(withDuration: 3.0, delay: 0.2, options: [.curveEaseOut], animations: {
            self.logoImage.frame = CGRect(x: 0, y: center, width: width, height: 200)
        }) { (finished) in
            if finished {
                // Repeat animation to bottom to top
               // self.bottom()
                self.LoginCheck()
            }
        }

    }
}
