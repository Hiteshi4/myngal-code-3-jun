//
//  InviteFriendsViewController.swift
//  Myngal
//
//  Created by SONU on 25/01/20.
//  Copyright © 2020 SONU. All rights reserved.
//

import UIKit

class InviteFriendsViewController: BaseViewController {
    @IBOutlet weak var logoImg: UIImageView!
    @IBOutlet weak var reffralText: UILabel!
    @IBOutlet weak var inviteFriendBtn: GradientButton!
    
    @IBOutlet weak var referralWidth: NSLayoutConstraint!
    @IBOutlet weak var refraalCode: UILabel!
    var userDetails:UserDetail?
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        let userDetails = CoreDataManager.shared.fetchDataFromUserDetail()
        
        for item in userDetails!
        {
            self.userDetails = item
            self.refraalCode.text =  item.referralCode
        }
    }
    
    func setupUI(){
        logoImg.layer.borderColor = #colorLiteral(red: 0.9960784314, green: 0.07450980392, blue: 0.003921568627, alpha: 1)
        logoImg.layer.borderWidth = 1
        logoImg.layer.shadowColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        logoImg.layer.shadowRadius = 5
       // reffralText.text = "Invite your friends to join Myngal App  and get 50 points for each friend that joins using your referral code \n https:myngal.com"
        refraalCode.layer.borderColor = #colorLiteral(red: 0.9960784314, green: 0.07450980392, blue: 0.003921568627, alpha: 1)
        refraalCode.layer.borderWidth = 1
        refraalCode.layer.shadowColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        refraalCode.layer.shadowRadius = 5
        refraalCode.layer.cornerRadius = 5
        let width = refraalCode.text
        let text = "#\(width)"
                 let sysFont: UIFont = UIFont.systemFont(ofSize: 16.0)
                 let width1 = UILabel.textWidth(font: sysFont, text: text )
       referralWidth.constant = width1+15
        
          let attributedString = NSMutableAttributedString(string: "Invite your friends to join Myngal App  and get 50 points for each friend that joins using your referral code \n https://www.Myngal.com")
        attributedString.addAttribute(.link, value: "https://www.Myngal.com", range: NSRange(location: 110, length: 24))
        reffralText.attributedText = attributedString
    }
    
    func share() {
        let textToShare = "Download myngal app  my referral code: \(userDetails?.referralCode!)"
        
        if let image :UIImage = UIImage(named: "logo")!{
            
            let objectsToShare: [Any] = [image,textToShare]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            //   activityVC.popoverPresentationController?.sourceView = sender
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    func share(shareText:String?,shareImage:UIImage?){
        var objectsToShare = [AnyObject]()
        
        
        if let shareTextObj = shareText{
            objectsToShare.append((shareTextObj as? AnyObject)!)
        }
        
        if let shareImageObj = shareImage{
            objectsToShare.append(shareImageObj)
        }
        
        if shareText != nil || shareImage != nil{
            let activityViewController = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            
            present(activityViewController, animated: true, completion: nil)
        }else{
            print("There is nothing to share")
        }
    }
    // MARK:- Action
    @IBAction func backBtnTapped(_ sender: UIButton) {
        sender.pulsate(sender: sender)
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func invitebtnTapped(_ sender: UIButton) {
        //share()
        let imageToShare = UIImage(named: "logo")
        share(shareText: "Sharing this text", shareImage: imageToShare)
    }
    
}
