//
//  SettingViewController.swift
//  Myngal
//
//  Created by SONU on 01/01/20.
//  Copyright © 2020 SONU. All rights reserved.
//

import UIKit


class SettingViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {
     var ResponseData: AppSetting?
    var AppSettingData:App_setting_data?
    let apiMapper = APIMapper()
    @IBOutlet weak var tblView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 0.07843137255, blue: 0, alpha: 1) ]
        tblView.estimatedRowHeight = 60
        tblView.rowHeight = 100
        tblView.isScrollEnabled = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
        dataSet()
        print(ResponseData)
    }
    
    //MARK:- Table View Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingTableViewCell") as! SettingTableViewCell
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingTableViewCell") as! SettingTableViewCell
            cell.titleLbl.text = "Update Location"
            cell.subTitleLbl.text = "Jaipur"
            return cell
            
        }
        if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingTableViewCell") as! SettingTableViewCell
            cell.titleLbl.text = "Verify Mobile Number"
            cell.subTitleLbl.text = "1111111111"
            return cell
            
        }
        if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingTableViewCell") as! SettingTableViewCell
            cell.titleLbl.text = "Become a premium member"
            cell.subTitleLbl.isHidden = true
            return cell
            
        }
        if indexPath.row == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserSetting") as! UserSetting
            if ResponseData?.hide_my_age == "1"{
                cell.HideAge = "1"
                cell.hideAgeSwitch.isOn = true
            }else{
                cell.HideAge = "0"
                cell.hideAgeSwitch.isOn = false
            }
            if ResponseData?.activeDate == "1"{
                cell.lastActive = "1"
                cell.lastActiveSwitch.isOn = true
            }else{
                cell.lastActive = "0"
                cell.lastActiveSwitch.isOn = false
            }
            if ResponseData?.makeDistanceInvisible == "1"{
                cell.locationInvisible  = "1"
                           cell.distanceSwitch.isOn = true
                       }else{
                 cell.locationInvisible  = "0"
                           cell.distanceSwitch.isOn = false
                       }
            return cell
            
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell") as! NotificationSetting
            if ResponseData?.chatMessageReceived == "1"{
                cell.chatMsg = "1"
                cell.chatSwitch.isOn = true
            }else{
               cell.chatMsg = "0"
                cell.chatSwitch.isOn = false
            }
            if ResponseData?.newMatches == "1"{
                cell.newMatch = "1"
                cell.matchesSwitch.isOn = true
            }else{
                cell.newMatch = "0"
                cell.matchesSwitch.isOn = false
            }
            if ResponseData?.onBeingShortlisted == "1"{
                cell.beingShortlist  = "1"
                           cell.beingShortlistedSwitch.isOn = true
                       }else{
                 cell.beingShortlist  = "0"
                           cell.beingShortlistedSwitch.isOn = false
                       }
            if ResponseData?.onBeingLiked == "1"{
                cell.beingLiked = "1"
                cell.beingLikedSwitc.isOn = true
            }else{
                cell.beingLiked = "0"
                cell.beingLikedSwitc.isOn = false
            }
            if ResponseData?.adminMessage == "1"{
                cell.adminMsg  = "1"
                           cell.adminMsgSwitch.isOn = true
                       }else{
                 cell.adminMsg  = "0"
                           cell.adminMsgSwitch.isOn = false
                       }
            
            return cell
            
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        
        //        let sectionName: String
        //           switch section {
        //               case 0:
        //                   sectionName = NSLocalizedString("mySectionName", comment: "mySectionName")
        //               case 1:
        //                   sectionName = NSLocalizedString("myOtherSectionName", comment: "myOtherSectionName")
        //               // ...
        //               default:
        //                   sectionName = ""
        //           }
        
        headerView.backgroundColor = UIColor.white
        
        let headerLabel = UILabel(frame: CGRect(x: 20, y: 0, width:
            tableView.bounds.size.width, height: tableView.bounds.size.height))
        headerLabel.font = UIFont(name: "ProximaNova-Bold", size: 30)
        
        headerLabel.textColor =  #colorLiteral(red: 1, green: 0.07843137255, blue: 0, alpha: 1)
        headerLabel.text = "App Setting"
        headerLabel.sizeToFit()
        headerView.addSubview(headerLabel)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0{
            return 100
        }
        if indexPath.row == 1{
            return 100
        }
        if indexPath.row == 2{
            return 100
        }
        if indexPath.row == 3{
            return 200
            
        }else{
            return 250
        }
    }
    
    func CallUpdateSettingApi(){
        self.showActivity()
      
      let index1 = IndexPath(row: 3, section: 0)
         let cell1: UserSetting = self.tblView.cellForRow(at: index1) as! UserSetting
        let HideAge = cell1.HideAge
        let LastActive = cell1.lastActive
        let LocationHide = cell1.locationInvisible
        
      
      let index2 = IndexPath(row: 4, section: 0)
            let cell2: NotificationSetting = self.tblView.cellForRow(at: index2) as! NotificationSetting
    let chatMsg = cell2.chatMsg
    let newMatch = cell2.newMatch
    let beingShortlist = cell2.beingShortlist
    let beingLiked = cell2.beingLiked
    let adminMsg = cell2.adminMsg
        
      
     
      
      let param : [String:Any] = [
             "hide_my_age" : HideAge,
             "make_distance_invisible": "\(LocationHide)",
             "chat_message_received" : chatMsg,
            "new_matches" : newMatch,
            "on_being_shortlisted" : beingShortlist,
           "on_being_liked" : beingLiked,
            "admin_message" : adminMsg,
           "active_date": LastActive
        
      ]
      apiMapper.appsSettingData(param, success: { (SucessDict) in
          self.hideActivity()
        print(SucessDict)
        let message = SucessDict["message"] as! String
          let data = SucessDict["data"] as! [String:AnyObject]
     

         CoreDataManager.shared.saveDatainAppsetting(data: SucessDict["data"] as! NSDictionary)
        self.showToast(message: message)
      }) { (Failure) in
          self.hideActivity()
        self.showAlert(Failure)
      }
      }
    
    
    
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
        sender.pulsate(sender: sender)
        dismiss(animated: true, completion: nil)
    }
    @IBAction func saveAndUpdateClicked(_ sender: UIButton) {
        CallUpdateSettingApi()
    }
}

extension SettingViewController{
    func dataSet(){
        let fetchdata = CoreDataManager.shared.fetchDataFromAppSetting()
              
                      for item in fetchdata!
                      {
                       self.ResponseData = item
                         
                         
                      }
               
    }
}
