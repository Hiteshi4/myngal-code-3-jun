//
//  MyProfileViewController.swift
//  Myngal
//
//  Created by SONU on 20/11/19.
//  Copyright © 2019 SONU. All rights reserved.
//

import UIKit
import MGCollapsingHeader
import Alamofire

class MyProfileViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource ,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate{
    var imageString:String = ""
    var imageStr = String()
    let common = Common()
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var uploadImageBtn: UIButton!
    @IBOutlet weak var UserNameBottomSpace: NSLayoutConstraint!
    var imagePicker : UIImagePickerController! = UIImagePickerController()
    @IBOutlet weak var UserNameTopSpace: NSLayoutConstraint!
    @IBOutlet weak var editProfileBtn: UIButton!
    var singelton = UserParams()
    var ResponseData: Data2?
    var userDetails:UserDetail?
    var arrTitle = ["Wallet","About","Intrest","Location","Logout"]
    var lastContentOffset: CGFloat = 60.0
    let maxHeaderHeight: CGFloat = 250
    
    @IBOutlet weak var HeaderGeight: NSLayoutConstraint!
    @IBOutlet weak var HeaderView: MGCollapsingHeaderView!
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var heightHrader: NSLayoutConstraint!
    @IBOutlet weak var DetailsView: UIView!
    @IBOutlet weak var tableview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
    }
    
    override func viewWillAppear(_ animated: Bool){
        dataSet()
    }
    
    
    func dataSet(){
        let fetchdata = CoreDataManager.shared.fetchDataFromData2()
               
                      for item in fetchdata!
                      {
                       self.ResponseData = item
                         
                         
                      }
               let userDetails = CoreDataManager.shared.fetchDataFromUserDetail()
              
                          for item in userDetails!
                          {
                           self.userDetails = item
                          }
           
               
               
               let profile = ResponseData?.profileImage
               profileImage.setImage(from: AppUrls.imagebaseUrl+"\(profile ?? "")", placeholderImage: #imageLiteral(resourceName: "male_selected"))
               let username =  ResponseData?.name
               userNameLbl.text = username ?? ""
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
            
            UIView.animate(withDuration: 0.3) {
                self.HeaderGeight.constant = 60.0
                self.UserNameTopSpace.constant = -40
                self.userNameLbl.textColor = .white
                self.userNameLbl.font = UIFont.boldSystemFont(ofSize: 18.0)
                self.UserNameBottomSpace.constant = 25
                self.profileImage.image = nil
                self.uploadImageBtn.isHidden = true
                self.editProfileBtn.isHidden = true
                self.view.layoutIfNeeded()
            }
        }
        else if (scrollView.contentOffset.y < self.lastContentOffset || scrollView.contentOffset.y <= 0) && (self.HeaderGeight.constant != self.maxHeaderHeight)  {
            //Scrolling up, scrolled to top
            UIView.animate(withDuration: 0.3) {
                self.HeaderGeight.constant = self.maxHeaderHeight
                let profile = self.ResponseData?.profileImage ?? ""
                self.profileImage.setImage(from: AppUrls.imagebaseUrl+"\(profile)", placeholderImage: #imageLiteral(resourceName: "male_selected"))
              
                self.UserNameTopSpace.constant = 10
                self.userNameLbl.textColor = .black
                self.uploadImageBtn.isHidden = false
                self.editProfileBtn.isHidden = false
                self.view.layoutIfNeeded()
            }
        }
        else if (scrollView.contentOffset.y > self.lastContentOffset) && self.HeaderGeight.constant != 0.0 {
            //Scrolling down
            UIView.animate(withDuration: 0.3) {
                self.HeaderGeight.constant = 0.0
                self.profileImage.image = nil
                self.uploadImageBtn.isHidden = true
                self.editProfileBtn.isHidden = true
                self.view.layoutIfNeeded()
            }
        }
        // self.lastContentOffset = scrollView.contentOffset.y
    }
    //MARK:- TablEVIEW Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PhotosCell") as! ProfilePhotosTableViewCell
            cell.imageData = self.userDetails
            
            
            return cell
        }
        else{
           
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell") as! ProfileTableViewCell
            switch indexPath.row {
            case 0:
                cell.TitleLbl.text = "Wallet"
                let logoutButton = UIButton(frame: CGRect(x: 0, y: 0, width: cell.frame.size.width, height: 60))
                logoutButton.addTarget(self, action: #selector(walletTapped(_:)), for: UIControl.Event.touchUpInside)
                cell.contentView.addSubview(logoutButton)
               
              
            case 1:
                cell.TitleLbl.text = "About"
                cell.decsriptionLbl.text = userDetails?.aboutMe ?? ""
            case 2:
                cell.TitleLbl.text = "Intrest"
                cell.decsriptionLbl.text = userDetails?.interest ?? ""
            case 3:
                cell.TitleLbl.text = "Location"
                cell.decsriptionLbl.text = userDetails?.city ?? ""
            case 5:
                let logoutButton = UIButton(frame: CGRect(x: 0, y: 0, width: cell.frame.size.width, height: 60))
                logoutButton.addTarget(self, action: #selector(logoutTapped(_:)), for: UIControl.Event.touchUpInside)
                cell.contentView.addSubview(logoutButton)
                cell.TitleLbl.text = "Logout"
                cell.TitleLbl.textAlignment = .center
                cell.decsriptionLbl.isHidden = true
            default:
                break
            }
            
            //  cell.TitleLbl.text = arrTitle[indexPath.row]
            return cell
        }
    }
    @objc func logoutTapped(_ sender:UIButton){
         Router.loadOnboardingInterface()
    }
    @objc func walletTapped(_ sender:UIButton){
       PresentTo(.Main, .WalletVc) { (vc) in
            
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.row {
        case 4 :
            return 210
        case 5 :
            return 60
        default:
            return 80
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 5{
            Router.loadOnboardingInterface()
        }
    }
    func ShowAlert(){
        //imagePicked = button.tag
        // if  button.tag == 0 {
        let alert:UIAlertController=UIAlertController(title: "Select Image", message: nil, preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
            
        {
            
            UIAlertAction in
            
            self.openCamera()
        }
        let galleryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openGallery()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in
        }
        
        self.imagePicker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(galleryAction)
        
        alert.addAction(cancelAction)
        //You must add this line then only in iPad it will not crash
        alert.popoverPresentationController?.sourceView = self.view
        self.present(alert, animated: true, completion: nil)
        
        //      }else{
        //        print("\(button.tag)  tapeeddd...")
        //        }
    }
    func openCamera()
    {
        
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            
            self.imagePicker.sourceType = .camera
            self.imagePicker.allowsEditing = true
            self.imagePicker.delegate = self
            self.present(self.imagePicker, animated: true, completion: nil)
        }
    }
    
    func openGallery()
    {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            self.imagePicker.allowsEditing = true
            self.imagePicker.sourceType = .photoLibrary
            self.imagePicker.delegate = self
            
            self.present(self.imagePicker, animated:true, completion: nil);
        }
        
    }
    //MARK: - ImagePicker Delegate
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
        self.profileImage.image = pickedImage
        
        self.ProfileUpdateApi(profileImage: pickedImage!, success: { (suceesDict) in
            print(suceesDict)
            self.showActivity()
            let message = suceesDict["message"] as! String
            let data = suceesDict["data"] as! [String:AnyObject]
            let profileImage = data["profile_image"] as! String
            self.ResponseData?.profileImage = profileImage
            self.hideActivity()
            self.showToast(message: message)
        }) { (Error) in
            self.hideActivity()
            print(Error)
        }
    dismiss(animated: true, completion: nil)
    }
        func ProfileUpdateApi(profileImage:UIImage,success:@escaping (NSDictionary) -> Void, failure:@escaping (Error) -> Void)
        {
            
            
            
            
            var parameters =
                [
                    
                    
                    "name": "Ravii",
                    "intrest": "",
                    "profession": "",
                    
                    
                    ] as [String : Any]
            var req_parameters = [String:Any]()
            var jsonString:String!
            
            
            
            
            var _:NSError?
            do
            {
    //            let jsonData = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
    //
    //            let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
    //            print(jsonString)
                
                
                
                let headers: HTTPHeaders = [
                    "Content-type": "multipart/form-data"
              //  "Accept": "application/json"
                ]
                let url = (AppUrls.baseURL + AppUrls.editprofile+"\(self.common.getUserId())")
                print("Params:----\(parameters)")
                print("Url:-----------\(url)")
                AF.upload(multipartFormData: { multipartFormData in
                    
                    
                     if profileImage.size.height != 0 {
                        let profileImg = profileImage.jpegData(compressionQuality: 0.2)!

                        multipartFormData.append(profileImg, withName: "profile_image",fileName: "\(Date().timeIntervalSince1970).png", mimeType: "image/png")
                   }
                    
                    for (key, value) in parameters {
                        multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                    }
                    
                },
                          to: url,method: .post,headers: headers).responseJSON { (resp) in
                            print(resp)
                            guard  let dictionary = resp.value as? NSDictionary else{return}
                            success(dictionary)
                }
                
            }
            catch
            {
                print("error")
            }
            
            
            
            
            
            
        }
    
    @IBAction func AddProfileImgTapped(_ sender: UIButton) {
        
        ShowAlert()
    }
    @IBAction func editProfileClicked(_ sender: UIButton) {
        
        var Editprofile: EditProfileViewController = self.storyboard?.instantiateViewController(withIdentifier: "editProfileVc") as! EditProfileViewController
        Editprofile.ResponseData = self.ResponseData
        Editprofile.userDetails = self.userDetails
          
        
        
        Editprofile.modalPresentationStyle = .overFullScreen
        Editprofile.modalTransitionStyle = .crossDissolve
        present(Editprofile, animated: true, completion: nil)
        
        
        
    }
    @IBAction func backTapped(_ sender: UIButton) {
        sender.pulsate(sender: sender)
        dismiss(animated: true, completion: nil)
    }
}




extension MyProfileViewController: MGCollapsingHeaderDelegate {
    
    func headerDidCollapse(toOffset offset: Double) {
        print("222")
    }
    
    func headerDidFinishCollapsing() {
        print("222")
    }
    
    func headerDidExpand(toOffset offset: Double) {
        print("222")
    }
    
    func headerDidFinishExpanding() {
        print("222")
    }
}
