//
//  MatchesViewController.swift
//  Myngal
//
//  Created by SONU on 04/02/20.
//  Copyright © 2020 SONU. All rights reserved.
//

import UIKit

class MatchesViewController: BaseViewController ,UITableViewDataSource,UITableViewDelegate{
    @IBOutlet weak var tblView: UITableView!
    let apiMapper = APIMapper()
    let common = Common()
    var propertyArray =  [PreferredProduct1]()
    var isFromeMatches = "true"
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.isUserInteractionEnabled = true
        tblView.register(UINib(nibName: "BlockTableViewCell", bundle: nil), forCellReuseIdentifier: "BlockCell")
        tblView.estimatedRowHeight = 60
        tblView.rowHeight = 100
        tblView.isScrollEnabled = false
    getMatchListApi()
    }
    
    func getMatchListApi(){
        
        self.showActivity()
        let userid = common.getUserId()
        let param : [String:Any] = [
            "user_id" : userid as Any
            
        ]
        apiMapper.matchList(param, success:{ (successDict) in
            let status = successDict["status"] as? String ?? ""
                       let Message = successDict["message"] as? String ?? ""
            self.showToast(message: Message)
                       if status == "true"{
            if let dictionary = successDict as? [String : AnyObject]{
                            let data = dictionary["data"] as? [[String: Any]] ?? []
              
                               for dict in data{
                                   let propertyList = PreferredProduct1(dictionary: dict)
                                   self.propertyArray.append(propertyList)
                                self.hideActivity()

                               }
                           }
                       }else{
                         self.showAlert(Message)
                        
            }
                           
                             self.tblView.reloadData()
                             self.hideActivity()
                           
                       }) { (Failure) in
                           
                           self.hideActivity()
                           self.showAlert(Failure)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return propertyArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BlockCell") as! BlockTableViewCell
                           cell.nameAgeLb.text = propertyArray[indexPath.row].name ?? ""
                           cell.profession.text = propertyArray[indexPath.row].user_info?.profession ?? ""
        cell.profileImg.setImage(from: AppUrls.imagebaseUrl+"\(propertyArray[indexPath.row].profile_image ?? "")", placeholderImage: #imageLiteral(resourceName: "male_selected"))
        cell.selectionStyle = .none
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let userProfile = propertyArray[indexPath.row]
        
           PresentTo(.Main, .OtherUserProfileVC){ (vc) in
        guard let viewProfileVC = vc as? OtherUserProfileViewController else{return}
         viewProfileVC.propertyArray = userProfile
            viewProfileVC.isFromeMatches  = self.isFromeMatches
                    }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if(segue.identifier == "chat_segue"){
                let displayVC = segue.destination as! ChatViewController
               // displayVC.name = "\(propertyArray[selectedIndex].name)"
        // displayVC.userId = propertyArray[selectedIndex].id as! Int
        // displayVC.userImg = propertyArray[selectedIndex].profile_image
         displayVC.modalPresentationStyle = .overFullScreen
         displayVC.modalTransitionStyle = .crossDissolve
        }
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
         sender.pulsate(sender: sender)
        dismiss(animated: true, completion: nil)
    }
    
}
