//
//  HelpSupportViewController.swift
//  Myngal
//
//  Created by SONU on 01/01/20.
//  Copyright © 2020 SONU. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import Photos
import Alamofire

class HelpSupportViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    @IBOutlet weak var heightTblView: NSLayoutConstraint!
    let common = Common()
    let apiMapper = APIMapper()
    
    var imagePicker : UIImagePickerController! = UIImagePickerController()
    var imageString:String = ""
    var imageStr: UIImage?
    var imageName:String = ""
    
    
    var selectedIndex = -1
        var isCollapse = false
    var questions = ["Q1.How can i use myngal ?",
    "Q.2 How do i can edit my profile ?",
    "Q.3 How do i can pay for Subscription?",
    "Q4.How do i can use my bonus points ?"]
    
    
    
    @IBOutlet weak var tblView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.estimatedRowHeight = 300
        tblView.rowHeight = UITableView.automaticDimension
        
        
        
        tblView.register(UINib(nibName: "SupportFeedbackTableViewCell", bundle: nil), forCellReuseIdentifier: "SupportFeedbackTableViewCell")
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 0.07843137255, blue: 0, alpha: 1)]
    }
    
    //MARK:- TableView Delegaes
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questions.count+1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row != 4 {
            if selectedIndex == indexPath.row
            {
                return 300
            }
            else
            {
                return 40
            }
            
            //return 480
        }else{
            return 500
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row != 4 {
                   let cell = tableView.dequeueReusableCell(withIdentifier: "help") as! HelpSupportTableViewCell
                 cell.questionLbl.text = questions[indexPath.row]
                            cell.selectionStyle = .none
            
                    return cell
//            let cell = tableView.dequeueReusableCell(withIdentifier: "MainHelpTableViewCell") as! MainHelpTableViewCell
//            cell.selectionStyle = .none
//            cell.tblView.sizeToFit()
//            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SupportFeedbackTableViewCell") as! SupportFeedbackTableViewCell
            cell.selectionStyle = .none
            cell.browseButton.addTarget(self, action: #selector(ShowAlert(_:)), for:.touchUpInside)
            cell.submitbutton.addTarget(self, action: #selector(submitBtnPressed(_:)), for: .touchUpInside)
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tblView.deselectRow(at: indexPath, animated: true)
     
        if self.selectedIndex == indexPath.row{
            if self.isCollapse == false{
               
                self.isCollapse = true
               // vc.tblView.rowHeight = 500
            }else{
                self.isCollapse = false
               // vc.tblView.rowHeight = 300
            }
        }else{
            self.isCollapse = true
           // vc.tblView.rowHeight = 500

        }
        self.selectedIndex = indexPath.row
        tableView.reloadRows(at: [indexPath], with: .automatic)
        print("Select")
    }
    
    
    
    @objc func ShowAlert(_ button:UIButton){
        
        
        let alert:UIAlertController=UIAlertController(title: "Select Image", message: nil, preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            
            self.openCamera()
        }
        let galleryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openGallery()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in
        }
        imagePicker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(galleryAction)
        
        alert.addAction(cancelAction)
        //You must add this line then only in iPad it will not crash
        alert.popoverPresentationController?.sourceView = self.view
        self.present(alert, animated: true, completion: nil)
        
    }
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            self.imagePicker.sourceType = .camera
            self.imagePicker.allowsEditing = true
            self.imagePicker.delegate = self
            self.present(self.imagePicker, animated: true, completion: nil)
        }
    }
    
    func openGallery()
    {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            self.imagePicker.allowsEditing = true
            self.imagePicker.sourceType = .photoLibrary
            self.imagePicker.delegate = self
            
            self.present(self.imagePicker, animated:true, completion: nil);
        }
        
    }
    //MARK: - ImagePicker Delegate
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let imageURL = info[UIImagePickerController.InfoKey.referenceURL] as? URL {
            let result = PHAsset.fetchAssets(withALAssetURLs: [imageURL], options: nil)
            let asset = result.firstObject
            let name = asset?.value(forKey: "filename") as? String ?? ""
            imageName = name as? String ?? ""
            print(asset?.value(forKey: "filename"))
            let index = IndexPath(row: 4, section: 0)
                       var cell:SupportFeedbackTableViewCell = tblView.cellForRow(at: index) as! SupportFeedbackTableViewCell
               cell.PickedImageName.text = name
        }

        
        guard let image = info[UIImagePickerController.InfoKey.originalImage]
            as? UIImage else {return}
        
        
        let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
        self.imageStr = pickedImage!
        
        dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        // sender.flash()
       
       sender.pulsate(sender: sender)
        
        dismiss(animated: true, completion: nil)
    }
    
    @objc func submitBtnPressed (_ button:UIButton){
        let isValid = Validate()
        if isValid{
            let index = IndexPath(row: 4, section: 0)
            var cell:SupportFeedbackTableViewCell = tblView.cellForRow(at: index) as! SupportFeedbackTableViewCell
            
            
            let param : [String:Any] = [
                
                "user_id": "\(common.getUserId())",
                "subject":   cell.subjectTextfield.text ?? "",
                "message": cell.textView.text ?? "",
            ]
            self.showActivity()
            apiMapper.upload(params: param, imageData: imageStr ?? UIImage() , success: { (succesDict) in
                print(succesDict)
                cell.subjectTextfield.text = ""
                cell.textView.text = ""
                self.imageStr = nil
                let message = succesDict["message"] as? String
                
                self.hideActivity()
                self.showToast(message: message!)
               
                
            }) { (failure) in
                print(failure)
                self.hideActivity()
            }
        }
        
        
        
        
        
        
        
    }
    func Validate()-> (Bool){
        var isValid = true
        let index = IndexPath(row: 4, section: 0)
        var cell:SupportFeedbackTableViewCell = tblView.cellForRow(at: index) as! SupportFeedbackTableViewCell
        if cell.subjectTextfield.text == ""{
            self.showAlert("Please Enter Subject")
            isValid = false
        }else if cell.textView.text == ""{
            self.showAlert("Please Enter Message")
            isValid = false
        }
        return isValid
    }
}
