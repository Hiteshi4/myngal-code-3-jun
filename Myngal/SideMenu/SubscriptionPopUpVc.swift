//
//  SubscriptionPopUpVc.swift
//  Myngal
//
//  Created by SONU on 04/01/20.
//  Copyright © 2020 SONU. All rights reserved.
//

import UIKit



class SubscriptionPopUpVc: BaseViewController, UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    var userPoints = 20
    var totalAmount = 0.0
    var currentSubscriptionValue = 0.0
    var userDetails:UserDetail?
    @IBOutlet weak var usePointsButton: UIButton!
    @IBOutlet weak var nextButton: GradientButton!
    @IBOutlet weak var collectionView: UICollectionView!
    let apiMapper = APIMapper()
    var subscriptionData = [SubcriptionResponse]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        getsubscriptionPackage()
        let userDetails = CoreDataManager.shared.fetchDataFromUserDetail()
               
                           for item in userDetails!
                           {
                           
                               self.userPoints = (Int(item.referralPoints ?? "") ?? 0)
                               
                           }
               self.usePointsButton.setTitle("Use Points = \(self.userPoints)", for: .normal)
        print("yurituuusytyusrt",self.userPoints)
      
    }
    
 


    @IBAction func ContinueTapped(_ sender: UIButton) {
       PresentTo(.Main, .PaymentVc) { (vc) in
            guard let PaymentVc = vc as? PaymentViewController else{return}
            PaymentVc.SelectedPlanPrice = self.currentSubscriptionValue
        PaymentVc.userPoints = self.userPoints

        }
       // dismiss(animated: true, completion: nil)
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
       self.dismiss(animated: true, completion: nil)
    }
    
    //MARK :- API CALL
    
   func getsubscriptionPackage(){
       self.showActivity()
       
       apiMapper.subscriptionPackage({ (successDict) in
        print(successDict)
        let Message = successDict["message"] as? String ?? ""
        self.showToast(message: Message)
      if let dictionary = successDict as? [String : AnyObject]{
                  let data = dictionary["data"] as! [[String: Any]]
                     for dict in data{
                         let propertyList = SubcriptionResponse(dictionary: dict)
                        
                        self.subscriptionData.append(propertyList)
                     }
        print("DAta...\(self.subscriptionData)")
                 }
        self.collectionView.reloadData()
           self.hideActivity()
       }) { (failure) in
           print(failure)
           self.hideActivity()
       }
   }
    @IBAction func usePointsClicked(_ sender: UIButton){
      usePointsButton.isSelected = !usePointsButton.isSelected
        if usePointsButton.isSelected {
            if currentSubscriptionValue != 0.0
                   {
                      var totalAmount1 = currentSubscriptionValue - Double(userPoints)
                       self.nextButton.setTitle("\(totalAmount1) TO PAY", for: .normal)
                       self.totalAmount = totalAmount1
                   }
        }
        else
        {
            if currentSubscriptionValue != 0.0
                   {
                      var totalAmount1 = currentSubscriptionValue
                       self.nextButton.setTitle("\(totalAmount1) TO PAY", for: .normal)
                       self.totalAmount = totalAmount1
                   }
        }
       
        
        
    }
    
    //MARK :- Collectioview delegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return subscriptionData.count
           }
           
           func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SubscriptioCollectionViewCell
            cell.lblDuration.text  = subscriptionData[indexPath.row].duration
            cell.lblMonth.text  =  subscriptionData[indexPath.row].name
            cell.pricelbl.text  = subscriptionData[indexPath.row].price

           
                     return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! SubscriptioCollectionViewCell
        
        currentSubscriptionValue = Double(self.subscriptionData[indexPath.row].price)!
        
//        if usePointsButton.isSelected
//        {
//            totalAmount = (Double(self.subscriptionData[indexPath.row].price) ?? 0.0) - Double(userPoints)
//        }
//        else
//        {
//            totalAmount = Double(self.subscriptionData[indexPath.row].price) ?? 0.0
//        }
        
        
        
        
//        DispatchQueue.main.async {
//            self.nextButton.setTitle("PROCEED TO PAY", for: .normal)
//        }
         
        cell.toggleSelected()
    }

    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! SubscriptioCollectionViewCell
      // print("deselect")
    cell.toggleSelected()
    }
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            let width = collectionView.frame.size.width
            
                     return CGSize(width:(width/4)-3 , height: 130)
                 }
    }



struct SubcriptionResponse {
    let  id: String
    let name : String
    let price: String
    let duration: String
    let status: String
    
    init(dictionary: [String:Any]) {
    id = dictionary["id"] as? String ?? ""
    name = dictionary["name"] as? String ?? ""
    price = dictionary["price"] as? String ?? ""
    duration = dictionary["duration"] as? String ?? ""
    status = dictionary["status"] as? String ?? ""
        
        
        
    }
}

