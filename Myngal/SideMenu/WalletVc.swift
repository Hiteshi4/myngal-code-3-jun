//
//  WalletVc.swift
//  Myngal
//
//  Created by SONU on 12/03/20.
//  Copyright © 2020 SONU. All rights reserved.
//

import UIKit

class WalletVc: UIViewController,UITableViewDelegate,UITableViewDataSource{
    @IBOutlet weak var tblView: UITableView!
     var arrTitle = ["My Points: ","Total Number of Points Accumulated: ","Points Earned Through Referral Sign Ups: ","Points Earned Through Other Medium: ","Points Redeemed: "]
    override func viewDidLoad() {
        super.viewDidLoad()
        
    tblView.register(UINib(nibName: "WalletCell", bundle: nil), forCellReuseIdentifier: "WalletCell")
        tblView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    

    //MARK:- Tableview Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WalletCell") as! WalletCell
         cell.titleLbl.text = arrTitle[indexPath.row]
        if indexPath.row == 0{
            cell.baseVw.layer.cornerRadius = 10
            cell.titleLbl.textColor = #colorLiteral(red: 0.9882352941, green: 0.168627451, blue: 0.4862745098, alpha: 1)
            cell.baseVw.layer.cornerRadius = 10
            cell.baseVw.layer.opacity = 2
            cell.baseVw.layer.shadowRadius = 2
            cell.baseVw.layer.shadowOffset.height = 2
            
            
            
        }
       
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 150
        }else{
            return 60
        }
    }
    
    
//MARK:- Button Action
    @IBAction func backBtnTapped(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }
}
