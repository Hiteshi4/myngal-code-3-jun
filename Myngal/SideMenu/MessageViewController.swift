//
//  MessageViewController.swift
//  Myngal
//
//  Created by SONU on 01/01/20.
//  Copyright © 2020 SONU. All rights reserved.
//

import UIKit

class MessageViewController: BaseViewController ,UITableViewDelegate,UITableViewDataSource{
    
    
    @IBOutlet weak var msgTableView: UITableView!
    var propertyArray =  [PreferredProduct1]()
    let apiMapper = APIMapper()
  let common = Common()
    var selectedIndex = 0
    
    
    override func viewDidLoad() {
       super.viewDidLoad()
       msgTableView.register(UINib(nibName: "MessageTableViewCell", bundle: nil), forCellReuseIdentifier: "MessageTableViewCell")
       msgTableView.estimatedRowHeight = 60
       msgTableView.rowHeight = 100
        
        callMessageListApi()
    }
    
    func callMessageListApi(){
        self.showActivity()
       
        let userid = common.getUserId()
               let param : [String:Any] = [
                   "user_id" : userid
                   
               ]
       
        apiMapper.messageList(param, success: { (SucessDict) in
            
            print(SucessDict)
            let status = SucessDict["status"] as? String ?? ""
            let message = SucessDict["message"] as? String ?? ""
            
            self.showToast(message: message)
            if status == "true"{
                if let dictionary = SucessDict as? [String : AnyObject]{
                  
                    let data = dictionary["data"] as? [[String: Any]] ?? []
                    for dict in data{
                        let propertyList = PreferredProduct1(dictionary: dict)
                        self.propertyArray.append(propertyList)

                    }
                }
               
            }
            
            else{
               self.showAlert(message)
//                if self.propertyArray.count == 0{
//                    self.showAlert("Currently You have no Messages")
//                }
                self.hideActivity()
            }
            
           self.msgTableView.reloadData()
           self.hideActivity()
            
        }) { (Failure) in
            
            
            
        }

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  propertyArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageTableViewCell") as! MessageTableViewCell
        let dob : String = propertyArray[indexPath.row].user_info!.dob


        cell.namelbl.text! = "\(propertyArray[indexPath.row].name)"
        cell.professionlbl.text! = propertyArray[indexPath.row].user_info?.profession as? String ?? ""
      
          let profile = propertyArray[indexPath.row].profile_image
        cell.profileImg.setImage(from: AppUrls.imagebaseUrl+"\(profile as? String ?? "")", placeholderImage: #imageLiteral(resourceName: "male_selected"))
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
//        let obj = storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
//        obj.name = "\(propertyArray[indexPath.row].name)"
//        obj.userId = propertyArray[indexPath.row].id as! Int
        self.performSegue(withIdentifier: "chat_segue", sender: self)
        
    }
   override func prepare(for segue: UIStoryboardSegue, sender: Any?){
       if(segue.identifier == "chat_segue"){
               let displayVC = segue.destination as! ChatViewController
               displayVC.name = "\(propertyArray[selectedIndex].name)"
        displayVC.userId = propertyArray[selectedIndex].id as! Int
        displayVC.userImg = propertyArray[selectedIndex].profile_image
        displayVC.modalPresentationStyle = .overFullScreen
        displayVC.modalTransitionStyle = .crossDissolve
       }
   }
    
    @IBAction func backbuttonTapped(_ sender: UIButton) {
        sender.pulsate(sender: sender)
        self.dismiss(animated: true, completion: nil)
    }
}





extension UIView {

  // OUTPUT 1
  func dropShadow(scale: Bool = true) {
    layer.masksToBounds = false
    layer.shadowColor = UIColor.black.cgColor
    layer.shadowOpacity = 0.5
    layer.shadowOffset = CGSize(width: -1, height: 1)
    layer.shadowRadius = 1

    layer.shadowPath = UIBezierPath(rect: bounds).cgPath
    layer.shouldRasterize = true
    layer.rasterizationScale = scale ? UIScreen.main.scale : 1
  }

  // OUTPUT 2
  func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
    layer.masksToBounds = false
    layer.shadowColor = color.cgColor
    layer.shadowOpacity = opacity
    layer.shadowOffset = offSet
    layer.shadowRadius = radius

    layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
    layer.shouldRasterize = true
    layer.rasterizationScale = scale ? UIScreen.main.scale : 1
  }
}
