//
//  LikesViewController.swift
//  Myngal
//
//  Created by SONU on 01/01/20.
//  Copyright © 2020 SONU. All rights reserved.
//

import UIKit

class LikesViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var tblView: UITableView!
    let apiMapper = APIMapper()
    let common = Common()
    var propertyArray =  [PreferredProduct1]()
    var userInfo: PreferredProduct1?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let status = self.common.getSubscription()
        print("CheckSubscription \(status)")
        CalllikeListApi()
        
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 0.07843137255, blue: 0, alpha: 1)]
        
        tblView.register(UINib(nibName: "BlockTableViewCell", bundle: nil), forCellReuseIdentifier: "BlockCell")
        tblView.estimatedRowHeight = 60
        tblView.rowHeight = 100
        tblView.isScrollEnabled = false
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return propertyArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BlockCell") as! BlockTableViewCell
        
        cell.nameAgeLb.text = propertyArray[indexPath.row].name
        cell.profession.text = propertyArray[indexPath.row].user_info?.profession ?? ""
        let profile = propertyArray[indexPath.row].profile_image
        cell.profileImg.setImage(from: AppUrls.imagebaseUrl+"\(profile as? String ?? "")", placeholderImage: #imageLiteral(resourceName: "male_selected"))
        cell.selectionStyle = .none
        //
        //        if common.getSubscription() == "false"{
        //            if indexPath.row > 5 {
        //                cell.mainView.makeBlurView(targetView:  cell.mainView)
        //
        //            }
        //
        //        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("tapped")
        
        let userProfile = propertyArray[indexPath.row]
           PresentTo(.Main, .OtherUserProfileVC){ (vc) in
        guard let viewProfileVC = vc as? OtherUserProfileViewController else{return}
         viewProfileVC.propertyArray = userProfile
                    }
        
        //        if common.getSubscription() == "false"{
        //                           if indexPath.row > 5 {
        //                            self.showAlert("Please Subscribe First")
        //
        //                           }else{
        //                             self.showAlert("Subscribed USer")
        //            }
        //
        //                       }
    }
//    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
//        if editingStyle == .delete {
//            objects.remove(at: indexPath.row)
//            tableView.deleteRows(at: [indexPath], with: .fade)
//        } else if editingStyle == .insert {
//            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
//        }
//    }
    
    func CalllikeListApi() {
        print("called function...")
        
        let userid = common.getUserId()
        let param : [String:Any] = [
            "user_id" : userid as Any
            
        ]
        self.showActivity()
        apiMapper.getLikelist(param, success: { (SucessDict) in
            
            print(SucessDict)
            let status = SucessDict["status"] as? String ?? ""
            let Message = SucessDict["message"] as? String ?? ""
            self.showToast(message: Message)
            if status == "true"{
                if let dictionary = SucessDict as? [String : AnyObject]{
                    let data = dictionary["data"] as? [[String: Any]] ?? []
                    for dict in data{
                        let propertyList = PreferredProduct1(dictionary: dict)
                        self.propertyArray.append(propertyList)
                        
                    }
                }
            }else{
                self.showAlert(Message)
            }
            
            
            
            self.tblView.reloadData()
            self.hideActivity()
            
        }) { (Failure) in
            self.showAlert(Failure)
            self.hideActivity()
            
            
        }
        
    }
    
    
    
    @IBAction func backTapped(_ sender: UIButton) {
        sender.pulsate(sender: sender)
        print("tapped")
        dismiss(animated: true, completion: nil)
    }
}
