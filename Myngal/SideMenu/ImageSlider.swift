//
//  ImageSliderVc.swift
//  Myngal
//
//  Created by SONU on 06/02/20.
//  Copyright © 2020 SONU. All rights reserved.
//

import UIKit

class ImageSliderVc: BaseViewController , UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionView: UICollectionView!
    
      var imageArr = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
      print(imageArr)
        
    }
    

   func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return imageArr.count
            }
            
            func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageSliderCell", for: indexPath) as! ImageSliderCell
            //    let profileImageURL = self.ResponseData?.data?.profile_image
               // profileImag.setImage(from: AppUrls.imagebaseUrl+"\(profileImageURL)
                cell.image.setImage(from: AppUrls.imagebaseUrl+"\(imageArr[indexPath.row])", placeholderImage: #imageLiteral(resourceName: "man1"))
                      return cell
     }
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        
     }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.size.width
        let height = collectionView.frame.size.height
        
        
        
             return CGSize(width:width , height: height)
         }
    @IBAction func backButtonTaped(_ sender: UIButton) {
         sender.pulsate(sender: sender)
        dismiss(animated: true, completion: nil)
    }
}


class ImageSliderCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
}
