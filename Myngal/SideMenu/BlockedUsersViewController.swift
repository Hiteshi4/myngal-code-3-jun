//
//  BlockedUsersViewController.swift
//  Myngal
//
//  Created by SONU on 01/01/20.
//  Copyright © 2020 SONU. All rights reserved.
//

import UIKit

class BlockedUsersViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var tblView: UITableView!
    let apiMapper = APIMapper()
    let common = Common()
    var propertyArray =  [PreferredProduct1]()
    override func viewDidLoad() {
        super.viewDidLoad()

self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 0.07843137255, blue: 0, alpha: 1)]
        self.view.isUserInteractionEnabled = true
        tblView.register(UINib(nibName: "BlockTableViewCell", bundle: nil), forCellReuseIdentifier: "BlockCell")
            tblView.estimatedRowHeight = 60
            tblView.rowHeight = 100
            tblView.isScrollEnabled = false
           CallBlockListApi()
        }
    
    func CallBlockListApi() {
        print("called function...")
        
        let userid = common.getUserId()
        let param : [String:Any] = [
            "user_id" : userid as Any
            
        ]
        self.showActivity()
        apiMapper.blockList(param, success: { (SucessDict) in
            
            print(SucessDict)
            let status = SucessDict["status"] as? String ?? ""
            let Message = SucessDict["message"] as? String ?? ""
            self.showToast(message: Message)
            if status == "true"{
                if let dictionary = SucessDict as? [String : AnyObject]{
                   let data = dictionary["data"] as? [[String: Any]] ?? []
                    for dict in data{
                        let propertyList = PreferredProduct1(dictionary: dict)
                        self.propertyArray.append(propertyList)
                        
                    }
                }
            }else{
                self.showAlert(Message)
            }
            
            
            
            self.tblView.reloadData()
            self.hideActivity()
            
        }) { (Failure) in
            self.showAlert(Failure)
            self.hideActivity()
            
            
        }
        
    }
      
    //MARK:- TableView Delegates
    
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return propertyArray.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BlockCell") as! BlockTableViewCell
            cell.selectionStyle = .none
           
           
            
            cell.nameAgeLb.text = "\(self.propertyArray[indexPath.row].name)"
         let profileImageURL = self.propertyArray[indexPath.row].profile_image
                 
cell.profileImg.setImage(from: AppUrls.imagebaseUrl+"\(profileImageURL as? String ?? "")", placeholderImage: #imageLiteral(resourceName: "man1"))
            
            return cell
        }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let userProfile = propertyArray[indexPath.row]
           PresentTo(.Main, .OtherUserProfileVC){ (vc) in
        guard let viewProfileVC = vc as? OtherUserProfileViewController else{return}
         viewProfileVC.propertyArray = userProfile
                    }
    }
        
    @IBAction func backTapped(_ sender: UIButton) {
        sender.pulsate(sender: sender)
        dismiss(animated: true, completion: nil)
    }
    
   
}
    

    
