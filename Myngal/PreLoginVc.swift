//
//  ViewController.swift
//  Myngal
//
//  Created by SONU on 11/11/19.
//  Copyright © 2019 SONU. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn

import Alamofire


class PreLoginVc: BaseViewController, GIDSignInDelegate{
    let apiMapper = APIMapper()
    var UserData           : Dictionary<String, Any>?
    //  var userData1 =  LoginRespnse()
    lazy var userData1: [LoginRespnse] = [LoginRespnse]()
    var propertyArray:LoginRespnse?
    
    
    @IBOutlet weak var loginBtn: GradientButton!
    
    @IBOutlet weak var CreteNewBtn: UIButton!
    @IBOutlet weak var facebookLoginBtn: GradientButton!
    
    
    var dicFacebook                                     = Dictionary<String,Any>()
    var dicGoogle                                       = Dictionary<String,Any>()
    
    
    @IBOutlet weak var googleLoginBtn: GIDSignInButton!
    let spacing:CGFloat = 10
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance().presentingViewController = self
        GIDSignIn.sharedInstance().delegate = self
        navigationController?.navigationBar.isHidden = true
        
        //  loginBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            
           
            
            print(user.profile)
            
            let singleTon  = UserParams()
            singleTon.sharedInstance.username = user.profile.name!
            singleTon.sharedInstance.userEmail = user.profile.email!
            singleTon.sharedInstance.ProfileImage = "\(user.profile.imageURL(withDimension: 100))"
            singleTon.sharedInstance.dob = ""
            self.loginApiCall()
          
            
          
            
        } else {
            print(error)
        }
    }
    @IBAction func logInWithPhoneClicked(_ sender: UIButton) {
        navigateTo(.Main, .LoginVc){ (vc) in
            
            
        }
    }
    
    @IBAction func fbLoginClicked(_ sender: UIButton) {
        
        
        let loginManager : LoginManager = LoginManager()
        //        loginManager.logOut()
        //
        loginManager.logIn(permissions: ["email","public_profile","user_hometown"], from: self) {
            (result, error) -> Void in
            
            //         //   GFunction.shared.removeLoader()
            //
            if (error == nil){
                let fbloginresult : LoginManagerLoginResult = result!
                
                if (fbloginresult.grantedPermissions != nil) {
                    if(fbloginresult.grantedPermissions.contains("email")) {
                        
                        if((AccessToken.current) != nil){
                            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email,gender,birthday"]).start(completionHandler: { (connection, result, error) -> Void in
                                if (error == nil){
                                    //everything works print the user data
                                    print("Facebook Result :", result as Any)
                                    
                                    self.dicFacebook = result as! Dictionary<String, Any>
                                    let vc : SignUpViewController = self.storyboard?.instantiateViewController(withIdentifier: "SignUp") as! SignUpViewController
                                    var view : OtpViewController = self.storyboard?.instantiateViewController(withIdentifier: "OtpVc") as! OtpViewController
                                    
                                    var parameter  = Dictionary<String, String>()
                                    parameter["fb_id"] = self.dicFacebook["id"] as? String
                                    vc.dicFacebook = self.dicFacebook
                                    let singleTon  = UserParams()
                                    
                                     singleTon.sharedInstance.userEmail =  self.dicFacebook["email"] as? String ?? ""
                                    
                                    singleTon.sharedInstance.username = self.dicFacebook["name"] as? String ?? ""
                                    singleTon.sharedInstance.dob = self.dicFacebook["birthday"] as? String ?? ""
                                    singleTon.sharedInstance.Selctedgender = self.dicFacebook["gender"] as? String ?? ""
                                   
                                    
                                    if let imageURL = ((self.dicFacebook["picture"] as? [String: Any])?["data"] as? [String: Any])?["url"] as? String {
                                        singleTon.sharedInstance.ProfileImage =  imageURL
                                    }
                                    self.loginApiCall()

                                    
                                }
                            }
                            )}
                    }
                    
                }
            }
        }
    }
   //MARK:- Web Service
    func loginApiCall(){
        self.showActivity()
        let singleTon = UserParams()
            
               let Email = singleTon.sharedInstance.userEmail
               let number = singleTon.sharedInstance.contact_No
    
        let param : [String:Any] = [
            "contact_number"  : number  as Any,
            "email"   : Email  as Any

        ]
        
        apiMapper.userLogin(param, success: { (successDict) in
           var view: HomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeVc") as! HomeViewController
            
        
           
            if let dictionary = successDict as? [String : Any]{
               
              print(dictionary)
               
                var propertyList = LoginRespnse(dictionary: dictionary as NSDictionary)
                self.propertyArray = propertyList
                   
                if  propertyList.status == "true"{
                          self.hideActivity()
                    view.userInfo = self.propertyArray
              if CoreDataManager.shared.entityIsEmpty(entity: "UserDetail")
              {
                CoreDataManager.shared.saveDatainUserDetail(data:  dictionary["user_data"] as! NSDictionary)
                                   }
                    
                    if CoreDataManager.shared.entityIsEmpty(entity: "FilterData")
                    {
                        CoreDataManager.shared.saveDatainFilterData(data: dictionary["filter_data"] as! NSDictionary)
                    }
                    if CoreDataManager.shared.entityIsEmpty(entity: "Data2")
                    {
                        CoreDataManager.shared.saveDatainData2(data:  dictionary["data"] as! NSDictionary)
                       
                    }
                    
                    
                    
                
                    Router.loadMainInterface()
                    
                    }else{
                        self.hideActivity()
                         self.navigateTo(.Main, .SignUp){ (vc) in
                
                            
                                                          }

                    }
                }
                
         
          
            
            
            }) { (errorDict) in
                self.hideActivity()
                self.showAlert(errorDict)
               // print(errorDict)
        }
        
    }
    @IBAction func googleBtnClicked(_ sender: UIButton) {
//        navigateTo(.Main, .LocationPermissionVc) { (vc) in
//
//        }
        GIDSignIn.sharedInstance().signIn()
        
    }
}


class CustomButton: UIButton {
    
    @IBInspectable open var cornerRadius: Float = 0 {
        didSet{
            layer.cornerRadius = CGFloat(cornerRadius)
            layer.masksToBounds = true
        }
    }
    
    @IBInspectable open var borderWidth: Float = 0 {
        didSet{
            layer.borderWidth = CGFloat(borderWidth)
        }
    }
    
    @IBInspectable open var borderColor: UIColor = .clear {
        didSet{
            layer.borderColor = borderColor.cgColor
        }
    }
    
}


class GradientButton: UIButton {
    
    open class override var layerClass: Swift.AnyClass {
        return CAGradientLayer.self
    }
    
    private var gradientLayer:CAGradientLayer{
        return layer as! CAGradientLayer
    }
    
    @IBInspectable var topGradientColor: UIColor = #colorLiteral(red: 0.2071596682, green: 0.7757281661, blue: 0.8106579185, alpha: 1) {
        didSet {
            setGradient(topGradientColor: topGradientColor, bottomGradientColor: bottomGradientColor)
        }
    }
    
    
    @IBInspectable var bottomGradientColor: UIColor = #colorLiteral(red: 0.6771834493, green: 0.9201820493, blue: 0.8484352827, alpha: 1) {
        didSet {
            setGradient(topGradientColor: topGradientColor, bottomGradientColor: bottomGradientColor)
        }
    }
    
    private func setGradient(topGradientColor: UIColor, bottomGradientColor: UIColor) {
        gradientLayer.colors = [topGradientColor.cgColor, bottomGradientColor.cgColor]
        gradientLayer.startPoint = CGPoint(x:0.9, y:0)
        gradientLayer.endPoint = CGPoint(x:0, y:0)
        //            gradientLayer.startPoint = CGPoint(x: 0, y:0)
        //            gradientLayer.endPoint = CGPoint(x:1, y:1)
        gradientLayer.borderColor = layer.borderColor
        gradientLayer.borderWidth = layer.borderWidth
        gradientLayer.cornerRadius = layer.cornerRadius
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
    
}


