//
//  WebService.swift
//  Myngal
//
//  Created by SONU on 04/01/20.
//  Copyright © 2020 SONU. All rights reserved.
//

import Foundation
import Alamofire
import SystemConfiguration

final class WebServiceManager {
    let common = Common()
    private init() {}
    
    static let shared = WebServiceManager()
    
    typealias WebServiceCompletionBlock<ResponseType: Decodable> = (_ response: ResponseType?, _ error: String?) -> Void
    
    private func headers() -> HTTPHeaders {
        var headers: HTTPHeaders
        headers = [:]
      
        return headers
    }
    

    
//    private func showResponseInDebugger<ParameterType: Any>(url: String, parameter: ParameterType?, data: Data?) {
//      //   Print.text(url)
//        if let param = parameter {
//            // Print.text(param)
//        }
//        do {
//            if let data = data {
//                //  Print.text(try JSONSerialization.jsonObject(with:data, options: []))
//            }
//        } catch let err {
//            // Print.text(err)
//        }
//    }
    
//    private func completeUrlString(url: String, extraApiPath: String?) -> String {
//        //        if let baseUrl = Default.shared.user?.baseUrl {
//        //            return baseUrl + url + (extraApiPath ?? "")
//        //        }
//        return API.baseURL + url + (extraApiPath ?? "")
//    }
    
}

//class APIModel {
//    var url: String = ""
//    var httpMethod: HTTPMethod = .post
//    var isRawData: Bool = true
//
//    init(_ url: String, _ httpMethod: HTTPMethod, _ isRawData: Bool) {
//        self.url = url
//        self.httpMethod = httpMethod
//        self.isRawData = isRawData
//    }
//}


//struct API {
//    
//    // Base Url
//    //    static let baseURL                                = "http://myngal.i4acmmosmedia.com/api/user/login" // Live
//    static let baseURL                                  = "http://myngal.i4acmmosmedia.com/api/" // Test
//    
//    //Post Methods
//    static let login                                    = APIModel.init("user/login", .post, false)
//    static let signUp                                    = APIModel.init("user/register", .post, false)
//    
//}

public class Reachability {
    class func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
}




extension Encodable {
    func serialzed() throws -> Parameters {
        let data = try JSONEncoder().encode(self)
        guard let serialzedDictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? Parameters else {
            throw NSError()
        }
        return serialzedDictionary
    }
}


class APIMapper: NSObject {
    let common = Common()
    let headers: HTTPHeaders = [
                          "Content-type": "multipart/form-data",
                      "Accept": "application/json"
                      ]
    func userLogin(_ params: Parameters,success: @escaping (_ result:NSDictionary) -> Void, failure: @escaping (_ message: String) -> Void) {
        
        AF.request(AppUrls.baseURL + AppUrls.login , method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            print(response)
            guard
                let result = response.value as? NSDictionary
                //  let products = result["data"] as? NSDictionary
                
                else {
                    
                    
                    return
            }
            print("response result ===---\(result)")
            success(result)
        }
    }
    func userHomeCards(_ success: @escaping (_ result:NSDictionary) -> Void, failure: @escaping (_ message: String) -> Void) {
     
       guard Reachability.isConnectedToNetwork() == true else {
           NotificationCenter.default.post(name: .noInternet, object: nil)
           failure("No internet connection")
           return
       }
        
        AF.request("http://myngal.i4acmmosmedia.com/api/user/homeuserlist/"+"\(self.common.getUserId())" , method: .post, parameters: [:], encoding: URLEncoding.default, headers: nil).responseJSON{(response) in
            
            guard let result = response.value as? NSDictionary
                
                
                else {
                    
                    
                    return
            }
            
            let products = result["data"] as? NSArray
            let dats = products?.value(forKeyPath: "contact_number")
            
            print("homeuserlist response result ===---\(result)")
            print("homeuserlist response Data ===---\(dats)")
            
            success(result)
        }
    }
    
    func editPRofile(_ params: Parameters,success: @escaping (_ result:NSDictionary) -> Void, failure: @escaping (_ message: String) -> Void) {
        
        AF.request(AppUrls.baseURL + AppUrls.editprofile+"368" , method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            
            guard
                let result = response.value as? NSDictionary
                //  let products = result["data"] as? NSDictionary
                
                else {
                    
                    
                    return
            }
            print("response result ===---\(result)")
            success(result)
        }
    }
    
    func filterData(_ params: Parameters ,success: @escaping (_ result:NSDictionary) -> Void, failure: @escaping (_ message: String) -> Void) {
        let singleTon = UserParams()
        
        AF.request(AppUrls.baseURL + AppUrls.filterApi+"\(common.getUserId())" , method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON{(response) in
            
            guard let result = response.value as? NSDictionary
                
                
                else {
                    
                    failure("failure")
                    return
            }
            
            let products = result["data"] as? NSArray
            
    
            success(result)
        }
    }
    
    func appsSettingData(_ params: Parameters , success: @escaping (_ result:NSDictionary) -> Void, failure: @escaping (_ message: String) -> Void) {
        let singleTon = UserParams()
        print("singleTon User IDDDD\(common.getUserId())")
        AF.request(AppUrls.baseURL + AppUrls.Appsetting+"\(common.getUserId())"   , method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON{(response) in
            
            guard let result = response.value as? NSDictionary
                
                
                else {
                    
                    
                    return
            }
            
            success(result)
        }
    }
    func getLikelist(_ params: Parameters, success: @escaping (_ result:NSDictionary) -> Void, failure: @escaping (_ message: String) -> Void) {
        
        AF.request(AppUrls.baseURL + AppUrls.likeList  , method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON{(response) in
            
            guard let result = response.value as? NSDictionary
                else {return}
            success(result)
        }
        
    }
    func addTolikes(_ params: Parameters, success: @escaping (_ result:NSDictionary) -> Void, failure: @escaping (_ message: String) -> Void) {
        let singleTon = UserParams()
        
        AF.request(AppUrls.baseURL + AppUrls.Addlike  , method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON{(response) in
            
            guard let result = response.value as? NSDictionary
                else {
                    
                    return
            }
            
            
            
            success(result)
        }
        
    }
    func saveForlater(_ params: Parameters, success: @escaping (_ result:NSDictionary) -> Void, failure: @escaping (_ message: String) -> Void) {
        
        
        AF.request(AppUrls.baseURL + AppUrls.saveforlater  , method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON{(response) in
            
            guard let result = response.value as? NSDictionary
                else {
                    
                    
                    return
            }
            
            
            
            success(result)
        }
        
    }
    
    func RegisterNewUser(_ params: Parameters , success: @escaping (_ result:NSDictionary) -> Void, failure: @escaping (_ message: String) -> Void) {
        let singleTon = UserParams()
        print("singleTon User IDDDD\(singleTon.sharedInstance.userID)")
        AF.request(AppUrls.baseURL + AppUrls.NewuserRegister  , method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON{(response) in
            
            guard let result = response.value as? NSDictionary
                
                
                else {
                    
                    
                    return
            }
            
            
            success(result)
        }
    }
    
    func messageList(_ params: Parameters ,success: @escaping (_ result:NSDictionary) -> Void, failure: @escaping (_ message: String) -> Void) {
       
        
        AF.request(AppUrls.baseURL + AppUrls.MessageList , method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON{(response) in
            
            guard let result = response.value as? NSDictionary
                
                
                else {
                    
                    
                    return
            }
          
            success(result)
        }
    }
    func savedList(_ params: Parameters , success: @escaping (_ result:NSDictionary) -> Void, failure: @escaping (_ message: String) -> Void) {
        let singleTon = UserParams()
        
        AF.request(AppUrls.baseURL + AppUrls.savedList , method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON{(response) in
            
            guard let result = response.value as? NSDictionary
                
                
                else {
                
                    return
            }
      
            success(result)
        }
    }
    func matchList(_ params: Parameters ,success: @escaping (_ result:NSDictionary) -> Void, failure: @escaping (_ message: String) -> Void) {
        
        
        AF.request(AppUrls.baseURL + AppUrls.matchesList , method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON{(response) in
            
            guard let result = response.value as? NSDictionary
                else {return}
            print(result)
            success(result)
        }
    }
    
   
    
    func upload( params : [String:Any],imageData:UIImage,success: @escaping (_ result:NSDictionary) -> Void, failure: @escaping (_ message: String) -> Void) {
                let urlString = AppUrls.baseURL + AppUrls.helpAndsupport
                
                let headers: HTTPHeaders = [
                    "Content-type": "multipart/form-data",
                "Accept": "application/json"
                ]
                    
                    
        let image = (imageData as! UIImage).pngData() as NSData?
                    
                   
                   
                AF.upload(
                multipartFormData: { multipartFormData in
                    
                for (key, value) in params {
                if let temp = value as? String {
                multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                }
   
                if let temp = value as? NSArray {
                temp.forEach({ element in
                let keyObj = key + "[]"
                if let string = element as? String {
                multipartFormData.append(string.data(using: .utf8)!, withName: keyObj)
                } else
                if let num = element as? Int {
                let value = "(num)"
                multipartFormData.append(value.data(using: .utf8)!, withName: keyObj)
                }
                })
                }
                }
                   
                    
                    if imageData.size.height != 0{

                       multipartFormData.append(image as! Data, withName: "image", fileName: "\(Date().timeIntervalSince1970).png", mimeType: "image/png")
                    }
                            
                    },
                        to: urlString, //URL Here
                        method: .post,
                        headers: headers)
                        .responseJSON { (resp) in
                            print(resp)
                            
                            guard  let result = resp.value as? NSDictionary else{return}
                            print("parameterrs----\(params)-----/n response \(result)")
                            success(result)
                           
                    }
                }
    
    
    
    func RegistraionApi( params : [String:Any],imageData:UIImage,success: @escaping (_ result:NSDictionary) -> Void, failure: @escaping (_ message: String) -> Void) {
                   let urlString = AppUrls.baseURL+AppUrls.NewuserRegister
                   
                  
                       
                       
           let image = (imageData as! UIImage).pngData() as NSData?
                       
                      
                      
                   AF.upload(
                   multipartFormData: { multipartFormData in
                       
                   for (key, value) in params {
                   if let temp = value as? String {
                   multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                   }
      
                   if let temp = value as? NSArray {
                   temp.forEach({ element in
                   let keyObj = key + "[]"
                   if let string = element as? String {
                   multipartFormData.append(string.data(using: .utf8)!, withName: keyObj)
                   } else
                   if let num = element as? Int {
                   let value = "(num)"
                   multipartFormData.append(value.data(using: .utf8)!, withName: keyObj)
                   }
                   })
                   }
                   }
                      
                       
                       if imageData.size.height != 0{

                          multipartFormData.append(image as! Data, withName: "image", fileName: "\(Date().timeIntervalSince1970).png", mimeType: "image/png")
                       }
                               
                       },
                           to: urlString, //URL Here
                           method: .post,
                           headers: headers)
                           .responseJSON { (resp) in
                               print(resp)
                               
                               guard  let result = resp.value as? NSDictionary else{return}
                               print("parameterrs----\(params)-----/n response \(result)")
                               success(result)
                              
                       }
                   }
       
    
    
    
    func subscriptionPackage(_ success: @escaping (_ result:NSDictionary) -> Void, failure: @escaping (_ message: String) -> Void) {
  
        AF.request(AppUrls.baseURL + AppUrls.subscriptionPackage , method: .get, parameters: [:], encoding: URLEncoding.default, headers: nil).responseJSON{(response) in
            
            guard let result = response.value as? NSDictionary
                
                
                else {
                    
                    
                    return
            }
            
            
            
            success(result)
        }
    }
    func CheckSubscription(_ params: Parameters, success: @escaping (_ result:NSDictionary) -> Void, failure: @escaping (_ message: String) -> Void) {
    
       
        AF.request(AppUrls.baseURL + AppUrls.CheckSubscription  , method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON{(response) in
            
            guard let result = response.value as? NSDictionary
                else {
                    
                    return
            }
            
            success(result)
        }
        
    }
    func blockList(_ params: Parameters, success: @escaping (_ result:NSDictionary) -> Void, failure: @escaping (_ message: String) -> Void) {
    
       
        AF.request(AppUrls.baseURL + AppUrls.blockList  , method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON{(response) in
            
            guard let result = response.value as? NSDictionary
                else {
                    
                    return
            }
            
            success(result)
        }
        
    }
    func startChat(_ params: Parameters, success: @escaping (_ result:NSDictionary) -> Void, failure: @escaping (_ message: String) -> Void) {
    
       
        AF.request(AppUrls.baseURL + AppUrls.chatStart  , method: .post, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON{(response) in
            
            guard let result = response.value as? NSDictionary
                else {
                    
                    return
            }
            
            success(result)
        }
        
    }
    func chatImage(img:UIImage,imgName:String, success: @escaping (_ result:NSDictionary) -> Void, failure: @escaping (_ message: String) -> Void)
    {
            let url = AppUrls.baseURL + AppUrls.chatImageApi
            
            AF.upload(multipartFormData: { multipartFormData in
     
                    let imag = img.jpegData(compressionQuality: 0.2)!
                    
                    multipartFormData.append(imag, withName: "upload_file",fileName: "\(Date().timeIntervalSince1970).png", mimeType: "image/png")
             
            },
                      to: url,method: .post,headers: headers).responseJSON { (result) in
                        print(result)
        }
//            { (result) in
//                switch result {
//                case .success(let upload,_,_ ):
//
//                    upload.uploadProgress(closure: { (progress) in
//                        print("Upload Progress: \(progress.fractionCompleted)")
//                    })
//
//                    upload.responseJSON { response in
//                        if response.result.isSuccess
//                        {
//
//                            //                                let sucessDict = response as! [String:AnyObject]
//                            //
//
//
//                            let val=(response.result.value!) as! NSDictionary
//                            let sucessDict = val as? [String : AnyObject]
//                            print(val)
//                            success(val)
//
//
//
//                        }
//
//                        if (response.result.error != nil)
//                        {
//
//                            let err = response.result.error!
//                            print(err)
//
//                            failure(err as! String)
//
//
//                        }
//
//                        print("response")
//                    }
//
//                case .failure(let encodingError):
//                    print(encodingError)
//                }
//            }
        
    }
    
    
}


struct AppUrls {
   
    static let baseURL = "http://myngal.i4acmmosmedia.com/api/"
    
    static let login = "user/login"
    static let UserHomelist = "user/homeuserlist/"
    static let imagebaseUrl =   "http://myngal.i4acmmosmedia.com/public/uploads/profile-images/"
    static let chatimagebaseUrl =   "http://myngal.i4acmmosmedia.com/public/uploads/chat-images/"
    
    static let NewuserRegister =  "user/register"
    static let editprofile =  "user/edit/"
    static let filterApi =   "user/filter/"
    static let Appsetting =  "user/app_setting/"
    static let likeList =  "like/list"
    static let blockList = "block/list"
    static let Addlike = "add/like"
    static let saveforlater = "add/save"
    static let MessageList = "message/list"
    static let savedList = "save/list"
    static let matchesList = "friend/list"
    static let helpAndsupport = "tickets/add"
    static let subscriptionPackage = "subsciption"
    static let CheckSubscription = "subsciption/user"
    static let chatImageApi = "user/chat_file"
    static let chatStart = "chat"
    
 
}

