//
//  HomeViewController.swift
//  Myngal
//
//  Created by SONU on 15/11/19.
//  Copyright © 2019 SONU. All rights reserved.
//



import UIKit
import Koloda
import GuillotineMenu

class HomeViewController: BaseViewController {
    let common = Common()
     let apiMapper = APIMapper()
    var propertyArray =  [PreferredProduct1]()
    
     var userInfo: LoginRespnse?
     var numberOfCards: UInt = 5
    var Age:String = ""
    

    
    @IBOutlet weak var kolodaView: KolodaView!
    
    @IBOutlet weak var backgroundImage: UIImageView!
 
       fileprivate lazy var presentationAnimator = GuillotineTransitionAnimation()
    var cardsData:NSArray = [""]
   
   
    
       var dicResponse       = Dictionary<String,Any>()
 
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
  CallcardDataApi()
        var det = CoreDataManager.shared.fetchDataFromUserDetail()
         var det1 = CoreDataManager.shared.fetchDataFromFilterData()
         var det2 = CoreDataManager.shared.fetchDataFromData2()
        var det3 = CoreDataManager.shared.fetchDataFromAppSetting()
       
        kolodaView.dataSource = self
        kolodaView.delegate = self
        print(det)
        print(det1)
        print(det2)
        
       
      
  
    }
    
    func CallcardDataApi(){
        self.showActivity()
        apiMapper.userHomeCards({ (successDict: NSDictionary) in
             let singleTon = UserParams()
            
             let message = successDict["message"] as! String
            let status = successDict["message"] as! String
            if let dictionary = successDict as? [String : AnyObject]{
             let data = dictionary["data"] as! [[String: Any]]
                for dict in data{
                    let propertyList = PreferredProduct1(dictionary: dict)
                    self.propertyArray.append(propertyList)
//                    if data.count == 0{
//                        self.showAlert("No More Users Nearby")
//                    }
                    
                }
            }
            self.showToast(message: message)
            self.kolodaView.reloadData()
            self.hideActivity()

           
  
          
            
           }) { (errorDict) in
            self.hideActivity()
        }
        
    }
    func callLikeApi(islike:String,otherUserId:String,isBlock:String){
       // let userID:String = userInfo!.data!.id
        self.showActivity()
        let param : [String:Any] = [
            "user_id" : self.common.getUserId(),
               "other_user_id": otherUserId,
               "is_like" : islike,
               "is_block":isBlock,
               
               
            
        ]
        print(param)
        apiMapper.addTolikes(param, success: { (SucessDict) in
            let message = SucessDict["message"] as! String
            self.showToast(message: message)
            self.hideActivity()
            
            print(param)
           print(SucessDict)
            
        }) { (Failure) in
            print(Failure)
            self.hideActivity()
            self.showAlert(Failure)
        }
    }
    
    func CallsaveForlaterApi(otherUserId:String = ""){
        self.showActivity()
           
           let param : [String:Any] = [
            "user_id" : common.getUserId(),
                  "other_user_id": otherUserId,
                  "is_save": "save"
               
           ]
           
           apiMapper.saveForlater(param, success: { (SucessDict) in
            
               self.hideActivity()
            let message = SucessDict["message"] as! String
            self.showToast(message: message)
            
            
               print("Parametere:=----------\(param)")
               
           }) { (Failure) in
               self.hideActivity()
               self.showAlert(Failure)
           }
       }
    func checkSubscriptionApi(){
           let param : [String:Any] = [
               "user_id" : self.common.getUserId()
           ]
           
           apiMapper.CheckSubscription(param, success: { (SucessDict) in
               self.hideActivity()
            print(SucessDict)
            let message = SucessDict["message"] as! String
                self.showToast(message: message)
            self.saveUserData(userDetails: SucessDict)
            let status = self.common.getSubscription()
            print("CheckSubscription \(status)")
           }) { (Failure) in
               self.hideActivity()
               self.showAlert(Failure)
           }
       }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    
    override func viewDidLoad() {
      super.viewDidLoad()
        // CallcardDataApi()
    //    print("propertyArrayCount....   \(self.propertyArray.count)")
        checkSubscriptionApi()
          //CallcardDataApi()
        backgroundImage.makeBlurImage(targetImageView: backgroundImage)
       
        setUpView()
        
        navigationController?.navigationBar.isHidden = false
        
    }
    
    
     func passData(notification: NSNotification) {
        print("called fhodf======   ghdft====-=-=-=-=-=-=-=asfd")
        let dict = notification.userInfo! as NSDictionary
        print(dict)
      //  var propertyList = LoginRespnse(dictionary: dict as NSDictionary)
      //  self.userInfo = propertyList
  // print("NSNotification......       \(propertyList)")
    }
    
    
    func saveUserData(userDetails: NSDictionary) {
        
        if let userDetailsBody = userDetails as? NSDictionary {

               common.setSubscription(status:userDetailsBody.value(forKey: "status") as? String ?? "")
               common.setSubscriptionMessage(message: userDetailsBody.value(forKey: "message") as? String ?? "")
              

               
        }
    }
    
    func setUpView(){
        
        var customView = UIView(frame: CGRect(x: 0, y: 0, width: 100.0, height: 25))
        customView.backgroundColor = .clear
              
              let moreButton = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 23))
                moreButton.setBackgroundImage(UIImage(named: "ic_menu"), for: .normal)
                moreButton.addTarget(self, action: #selector(sideButtOnPreeed), for: .touchUpInside)
                moreButton.tintColor = .white
        customView.addSubview(moreButton)
              //  self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: moreButton)
              
             
                     
           
        
        let reloadButton = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        reloadButton.tintColor = .white
                          reloadButton.setBackgroundImage(UIImage(named: "reload_home"), for: .normal)
                          reloadButton.addTarget(self, action: #selector(ReloadTapped), for: .touchUpInside)
                  

       let notificationButton = UIButton(frame: CGRect(x: reloadButton.frame.origin.x + reloadButton.frame.size.width + 100, y: 0, width: 25, height: 25))
                         notificationButton.setBackgroundImage(UIImage(named: "notification_bell"), for: .normal)
                         notificationButton.addTarget(self, action: #selector(NotificationTapped), for: .touchUpInside)
        let MessageButton = UIButton(frame: CGRect(x: notificationButton.frame.origin.x + notificationButton.frame.size.width + 100, y: 0, width: 25, height: 25))
        MessageButton.setBackgroundImage(UIImage(named: "message_home"), for: .normal)
        MessageButton.addTarget(self, action: #selector(MessageTapped), for: .touchUpInside)
let menuBarItem1 = UIBarButtonItem(customView: reloadButton)
let menuBarItem2 = UIBarButtonItem(customView: notificationButton)
let menuBarItem3 = UIBarButtonItem(customView: MessageButton)

                     navigationItem.rightBarButtonItems = [menuBarItem1, menuBarItem2, menuBarItem3 ]
        
        
      
           
              var marginX = CGFloat(moreButton.frame.origin.x + moreButton.frame.size.width + 10)
              var label = UILabel(frame: CGRect(x: marginX, y: 0.0, width: 65.0, height: 25))
              label.text = "Home"
              label.font = UIFont.boldSystemFont(ofSize: 20)
              label.textColor = UIColor.white
              label.textAlignment = NSTextAlignment.right
           
              customView.addSubview(label)
        
        var leftButton = UIBarButtonItem(customView: customView)
           self.navigationItem.leftBarButtonItem = leftButton
        
               let navBar = self.navigationController?.navigationBar
               navBar?.barTintColor = UIColor(
                 red: 255 / 255.0,
                 green: 20 / 255.0,
                 blue: 0 / 255.0,
                 alpha: 1
             )
        
    }
    @IBAction func dislikePressed(_ sender: UIButton) {
        kolodaView.swipe(.left)
    }
    @IBAction func saveForLaterPressed(_ sender: UIButton) {
        let SubscriptionCheck = common.getSubscription()
        if SubscriptionCheck == "true"{
            for item in propertyArray{
                let user_id = item.id
                self.CallsaveForlaterApi(otherUserId: "\(user_id)")
            }
        }else{
                PresentTo(.Main, .SubscriptionPopUpVc){ (vc) in
            
                    }
        }
        
        

       // CallsaveForlaterApi(otherUserId: T##String)
    }
    @IBAction func likePressed(_ sender: UIButton) {
        kolodaView.swipe(.right)
    }
    @IBAction func RevertCard(_ sender: UIButton) {
        kolodaView.revertAction()
    }
    
    @objc func NotificationTapped(){
        self.showToast(message: "Notification tapped")
//        PresentTo(.Main, .MessageVc){ (vc) in
//
//        }
    }
    @objc func ReloadTapped(){
        kolodaView.revertAction()
    }
    @objc func MessageTapped(){
       
        PresentTo(.Main, .MessageVc, { (vc) in
            
        })
    }
    
    
    @IBAction func sideButtOnPreeed(_ sender: UIButton) {
       
          var menuViewController: MenuViewController = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
      
      
   
        menuViewController.modalPresentationStyle = .custom
       
            menuViewController.transitioningDelegate = self
      
          
            presentationAnimator.animationDelegate = menuViewController as GuillotineAnimationDelegate
            presentationAnimator.supportView = navigationController!.navigationBar
        
            presentationAnimator.presentButton = sender
       // menuViewController.ResponseData = self.userInfo
        self.present(menuViewController, animated: true, completion: nil)
        
        }
    override var preferredStatusBarStyle: UIStatusBarStyle {
          return .lightContent
    }
   
    }

 


extension HomeViewController: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        presentationAnimator.mode = .presentation
        return presentationAnimator
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        presentationAnimator.mode = .dismissal
        return presentationAnimator
    }
}

extension UIImageView
{
    func makeBlurImage(targetImageView:UIImageView?)
    {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = targetImageView!.bounds

        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
        targetImageView?.addSubview(blurEffectView)
    }
}
extension UIView
{
    func makeBlurView(targetView:UIView?)
    {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = targetView!.bounds

        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
        targetView?.addSubview(blurEffectView)
    }
}



struct PreferredProduct1 {
let  id: Int
let name : String
let email: String
   
let email_verified_at : String?
let api_token : String?
let contact_number : String?
let profile_image : String?
let status : String?
let trash : String?
let created_by : String?
let updated_by : String?
let created_at : String?
let updated_at : String?
    
let user_info : User_info1?

init(dictionary: [String:Any]) {
    id = dictionary["id"] as? Int ?? 0
    name = dictionary["name"] as? String ?? ""
    email = dictionary["email"] as! String

    email_verified_at = dictionary["email_verified_at"] as? String
    api_token = dictionary["api_token"] as? String
    contact_number = dictionary["contact_number"] as? String
    profile_image = dictionary["profile_image"] as? String
  status = dictionary["status"] as? String
  trash = dictionary["trash"] as? String
   created_by = dictionary["created_by"] as? String
   updated_by = dictionary["updated_by"] as? String
      created_at = dictionary["created_at"] as? String
      updated_at = dictionary["updated_at"] as? String
    user_info = User_info1(dictionary: (dictionary["user_info"] as? NSDictionary) as? [String : Any] ?? [:])
}
}
struct User_info1 {
    let  id: String
    let dob : String
    let email: String
    let city: String
    let profession: String
    let interest: String
    let commitment_type: String
    let aboutme: String
    
    
    let image1: String
       let image2: String
       let image3: String
       let image4: String
     let education: String
    
    
    
    
    init(dictionary: [String:Any]) {
    id = dictionary["status"] as? String ?? ""
    dob = dictionary["dob"] as? String ?? ""
    email = dictionary["email"] as? String ?? ""
    city = dictionary["city"] as? String ?? ""
    profession = dictionary["profession"] as? String ?? ""
    interest = dictionary["interest"] as? String ?? ""
    commitment_type = dictionary["commitment_type"] as? String ?? ""
    aboutme = dictionary["aboutme"] as? String ?? ""
    image1 = dictionary["image1"] as? String ?? ""
    image2 = dictionary["image2"] as? String ?? ""
    image3 = dictionary["image3"] as? String ?? ""
    image4 = dictionary["image4"] as? String ?? ""
    education = dictionary["education"] as? String ?? ""

}
}




struct ProfileUpdateModel {
let  id: String
let name : String
let dob : String
let email: String
let city: String
let profession: String
let interest: String
let commitment_type: String
let aboutme: String
let image1: String
let image2: String
let image3: String
let image4: String
let education: String
let cover_image: String
let profile_image: String


    

init(dictionary: [String:Any]) {
    id = dictionary["status"] as? String ?? ""
    dob = dictionary["dob"] as? String ?? ""
    name = dictionary["name"] as? String ?? ""
    email = dictionary["email"] as? String ?? ""
    city = dictionary["city"] as? String ?? ""
    profession = dictionary["profession"] as? String ?? ""
    interest = dictionary["interest"] as? String ?? ""
    commitment_type = dictionary["commitment_type"] as? String ?? ""
    aboutme = dictionary["aboutme"] as? String ?? ""
    image1 = dictionary["image1"] as? String ?? ""
    image2 = dictionary["image2"] as? String ?? ""
    image3 = dictionary["image3"] as? String ?? ""
    image4 = dictionary["image4"] as? String ?? ""
    education = dictionary["education"] as? String ?? ""
    cover_image = dictionary["cover_image"] as? String ?? ""
     profile_image = dictionary["profile_image"] as? String ?? ""

    }
    
}
