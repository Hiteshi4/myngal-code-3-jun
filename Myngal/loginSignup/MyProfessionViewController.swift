//
//  MyProfessionViewController.swift
//  Myngal
//
//  Created by SONU on 26/12/19.
//  Copyright © 2019 SONU. All rights reserved.
//

import UIKit

protocol MyProfessionDelegate {
    func getProfession(profession:String)
}

class MyProfessionViewController: BaseViewController ,UITableViewDataSource,UITableViewDelegate {
    
    
    var delegate:MyProfessionDelegate!
    var callType:String!
    var selectedProfession:String!
    
    
    @IBOutlet weak var tblView: UITableView!
    var arrProfession = ["Lawyer","Teacher","Entrepreneur","Engineer","Sportsperson","Writer","Singer","Actor","Musician"]
    let singleTon = UserParams()
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = true
    }
   
     

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrProfession.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfessionCell")  as! ProfessionTableViewCell
        cell.textLabel?.text = arrProfession[indexPath.row]
        cell.textLabel?.font = .boldSystemFont(ofSize: 18)
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
          let cell = tableView.dequeueReusableCell(withIdentifier: "ProfessionCell")  as! ProfessionTableViewCell
        cell.textLabel?.textColor = #colorLiteral(red: 1, green: 0.07843137255, blue: 0, alpha: 1)
        print("arrProfession=-=========\(self.arrProfession[indexPath.row])")
        selectedProfession = "\(self.arrProfession[indexPath.row])"
        singleTon.sharedInstance.Professions = self.arrProfession[indexPath.row]
    }
    
    @IBAction func nextPressed(_ sender: UIButton) {
        if singleTon.sharedInstance.Professions == ""{
            self.showAlert("Please Select your Profession")
        }else{
            if callType == "Edit"
            {
                delegate.getProfession(profession: selectedProfession)
                dismiss(animated: true, completion: nil)
            }
            else
            {
                let login:MyCommitmentViewController = storyboard?.instantiateViewController(withIdentifier: "CommitmentVc") as! MyCommitmentViewController
                navigationController?.pushViewController(login, animated: true)
            }
            
            
        }
      
        
        
    }
    @IBAction func backClicked(_ sender: UIButton){
         sender.pulsate(sender: sender)
        dismiss(animated: true, completion: nil)
        navigationController?.popViewController(animated: true)
    }
}

struct profession
{
  var name:String
  //  var otherDetails
  var isSelected:Bool! = false
  init(name:String) {
    self.name = name
  }
}
