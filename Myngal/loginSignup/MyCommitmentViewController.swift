//
//  MyCommitmentViewController.swift
//  Myngal
//
//  Created by SONU on 26/12/19.
//  Copyright © 2019 SONU. All rights reserved.
//

import UIKit

protocol MyCommitmentProtocol {
    func getCommitment(commitment:String)
}


class MyCommitmentViewController: BaseViewController ,UITableViewDataSource,UITableViewDelegate{
    
    var callType:String!
    var delegate:MyCommitmentProtocol!
    var seletedCommitment:String!
    let singleTon = UserParams()
    
    
var arrCommitment = ["Shortterm relationship","Longterm relationship","Friends","Casual relationship"]
   
    
    
    var allCharacters:[Character] = []
    var propertyArray:LoginRespnse?
     let apiMapper = APIMapper()
    override func viewDidLoad() {
        super.viewDidLoad()


       
    }
    func callSignUpAPI() {

        let singleTon = UserParams()
        let name = singleTon.sharedInstance.username
        let Email = singleTon.sharedInstance.userEmail
        let number = singleTon.sharedInstance.contact_No
        let intrest = singleTon.sharedInstance.intrest
        let gender  =  singleTon.sharedInstance.Selctedgender
        let dob = singleTon.sharedInstance.dob
        let profession = singleTon.sharedInstance.Professions
        let commitment = singleTon.sharedInstance.Commitments
        let ProfileImage = singleTon.sharedInstance.ProfileImage
        let ReffralCode = singleTon.sharedInstance.ReffralCode
    

     self.showActivity()

            let param : [String:Any] = [
               "name"  : name  as Any,
                "contact_number"  : number  as Any,
                "email"   : Email  as Any,
                "gender"  : gender  as Any,
                "commitment_type"   : commitment  as Any,
                "profession"  : profession  as Any,
                "interest"  : intrest  as Any,
               // "profile_image"  : strImg  as Any,
                "dob"  : dob  as Any,
                "referal_code" : ReffralCode  as Any


            ]
            
            apiMapper.RegisterNewUser(param, success: { (successDict) in
               var view: HomeViewController = self.storyboard?.instantiateViewController(withIdentifier: "HomeVc") as! HomeViewController
     
                
                if let dictionary = successDict as? [String : AnyObject]{
                   
                    var propertyList = LoginRespnse(dictionary: dictionary as NSDictionary)
                   self.propertyArray = propertyList
                    print("parameters............\(param)")
                       // self.propertyArray.append(propertyList)
                    if  propertyList.status == "true"{
                        view.userInfo = self.propertyArray
                              self.hideActivity()
                        print("status.......\(propertyList.status)")
                        self.navigationController?.pushViewController(view, animated: true)
                           // Router.loadMainInterface()
                        }else{
                       // self.showAlert((self.propertyArray?.message!)! )
                            self.hideActivity()
                        print((self.propertyArray?.message!)!)
                       

                        }
                    }
                    
                    
              
                
                
                }) { (errorDict) in
                    self.hideActivity()
                   // self.showAlert(errorDict)
                    print("ErrorDict==-----   \(errorDict)")
            }
            
   }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCommitment.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommitmentCell") as! CommitmentTableViewCell
        cell.textLabel?.text = arrCommitment[indexPath.row]
        cell.textLabel?.font = .boldSystemFont(ofSize: 18)
        cell.selectionStyle = .none
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        seletedCommitment = self.arrCommitment[indexPath.row]
        
        print("arrProfession=-=========\(self.arrCommitment[indexPath.row])")
        let singleTon = UserParams()
        singleTon.sharedInstance.Commitments = self.arrCommitment[indexPath.row]
    }
   
    @IBAction func nextPressed(_ sender: UIButton) {
        if singleTon.sharedInstance.Commitments == ""{
                   self.showAlert("Please Select your Commitment")
        }else{
        if callType == "Edit"
        {
            delegate.getCommitment(commitment: seletedCommitment)
            dismiss(animated: true) {
                
            }
        }
        else
        {
            navigateTo(.Main, .UplodProfile) { (vc) in
                               
                           }
        }
                
        }
    }
    @IBAction func backClicked(_ sender: UIButton) {
       
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
}


class UserParams {
    var sharedInstance: UserParams {
          struct Static {
               static let instance = UserParams()
          }
          return Static.instance
     }
     var username : String = ""
     var userID: Int = 0
     var userEmail: String = ""
     var contact_No: String = ""
     var dob: String = ""
    var cardData: NSArray = []
     var intrest: String = ""
     var profession: String = ""
    var Selctedgender: String = ""
    var Professions: String = ""
    var Commitments: String = ""
    var ProfileImage: String = ""
    var ReffralCode: String = ""
    var dicResponse      = Dictionary<String,Any>()
    var cardsname:Array = [""]
    
    
    
}
