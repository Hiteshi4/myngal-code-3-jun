//
//  LocationPermissionViewController.swift
//  Myngal
//
//  Created by GEETAM SHARMA on 02/03/20.
//  Copyright © 2020 SONU. All rights reserved.
//

import UIKit
import Foundation
import NVActivityIndicatorView
import MapKit
import CoreLocation

protocol LocationDelegate {
    func getLocation(city:String,country:String)
}



class LocationPermissionViewController: UIViewController {
    
    
    var delegate:LocationDelegate!
    
  // for user current location
    let locationManager = CLLocationManager()
    @IBOutlet weak var activityView: UIView!
    var backgroundView = UIView()
          var activity : NVActivityIndicatorView?
    @IBOutlet weak var actView: NVActivityIndicatorView!
    @IBOutlet weak var backGroundView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
                   // for user current location
//                   self.locationManager.requestAlwaysAuthorization()
//
//                   // For use in foreground
//                   self.locationManager.requestWhenInUseAuthorization()
//
//                   if CLLocationManager.locationServicesEnabled() {
//                       locationManager.delegate = self
//                       locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
//                       locationManager.startUpdatingLocation()
//                   }
//
       // NotificationCenter.default.addObserver(self, selector: #selector(self.noInterNet), name: internet, object: nil)
        actView.type = .ballScaleMultiple
        actView.startAnimating()
    }
    



    @IBAction func allowLocatioTapped(_ sender: UIButton) {
        self.locationManager.requestAlwaysAuthorization()
        
                   // For use in foreground
                   self.locationManager.requestWhenInUseAuthorization()
        
                   if CLLocationManager.locationServicesEnabled() {
                       locationManager.delegate = self
                       locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                       locationManager.startUpdatingLocation()
                   }
                
    }
    @IBAction func backButtonTapped(_ sender: UIButton) {
         sender.pulsate(sender: sender)
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: For user current location
extension LocationPermissionViewController:CLLocationManagerDelegate
{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }

        print("locations = \(locValue.latitude) \(locValue.longitude)")
        getAddressFromLatLon(pdblLatitude: "\(locValue.latitude)", withLongitude: "\(locValue.longitude)")

    }

    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon

        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)


        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]

                if pm.count > 0 {
                    let pm = placemarks![0]
                    print(pm.country)
                    print(pm.locality)
                    print(pm.subLocality)
                    print(pm.thoroughfare)
                    print(pm.postalCode)
                    print(pm.subThoroughfare)
                    var addressString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    
                    self.delegate.getLocation(city: pm.locality!, country: pm.country!)

                    print(addressString)
              }
        })

    }

}
