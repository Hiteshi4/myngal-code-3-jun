//
//  MyIntrestViewController.swift
//  Myngal
//
//  Created by SONU on 26/12/19.
//  Copyright © 2019 SONU. All rights reserved.
//

import UIKit

protocol MyInterestDelegate{
    func getInterest(interests:String)
}


class MyIntrestViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate {
    
    var delegate:MyInterestDelegate!
    var callType:String!
    
    @IBOutlet weak var tblView: UITableView!
    var arrIntrest = ["Travelling", "Exercise", "Watching" ,"Movies","Dancing", "Cooking", "Politics", "Photography" ,"Playoutdoors Games" ,"Art" ,"Blogging" ,"Gaming", "Gambling" ,"Swimming"]
     var allCharacters:[Characters] = []
     var callback : [String] = []
    var SeguSender =  ""
    var Intrest = ""

    override func viewDidLoad() {
        super.viewDidLoad()
 navigationController?.navigationBar.isHidden = true
        
        allCharacters = [Characters(name: "Travelling"),Characters(name: "Exercise"),Characters(name: "Watching Movies"),Characters(name: "Dancing"),Characters(name: "Cooking"),Characters(name: "Politics"),Characters(name: "Photography"),Characters(name: "Playoutdoorsgames"),Characters(name: "Art"),Characters(name: "Blogging"),Characters(name: "Gaming"),Characters(name: "Gambling"),Characters(name: "Swimming")]
      let singleTon = UserParams()
    print("ProfileUrl\(singleTon.sharedInstance.Selctedgender))")
        print("ProfileUrl\(singleTon.sharedInstance.dob))")
    }
    override func viewDidAppear(_ animated: Bool) {
          
       }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allCharacters.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "IntrestCell") as! IntrestTableViewCell
        cell.accessoryType = .checkmark
       // cell.textLabel?.text = arrIntrest[indexPath.row]
        cell.textLabel?.font = .boldSystemFont(ofSize: 18)
        cell.textLabel?.text = allCharacters[indexPath.row].name

        return cell
        }
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
             let cell = tableView.dequeueReusableCell(withIdentifier: "IntrestCell") as! IntrestTableViewCell
            print("Selected")
            cell.textLabel?.textColor = #colorLiteral(red: 1, green: 0.07843137255, blue: 0, alpha: 1)
          
            allCharacters[indexPath.row].isSelected = !allCharacters[indexPath.row].isSelected
            if allCharacters.dropFirst().filter({ $0.isSelected }).count == allCharacters.dropFirst().count
            {
              allCharacters[0].isSelected = true
               
            }
            else
            {
              allCharacters[0].isSelected = false
                
            }
          
          //  Intrest.append(",\(self.allCharacters[indexPath.row].name)")
            callback.append(self.allCharacters[indexPath.row].name)
            
            print(self.allCharacters[indexPath.row])
          //tableView.reloadData()
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        callback.removeLast()
        print("deselect")
    }
    @IBAction func nextPressed(_ sender: UIButton) {
        
        if callback == []{
            self.showAlert("Please Choose Your Interest ")
        }else{
            
            let joined = callback.joined(separator: ",")
                   print(joined)
            
            if callType == "Edit"
            {
                delegate.getInterest(interests: joined)
              dismiss(animated: true, completion: nil)
            }
            else
            {
                let singleTon = UserParams()
                              singleTon.sharedInstance.intrest = joined
                                 print(SeguSender)
                               
                                     
                                     let login:MyProfessionViewController = storyboard?.instantiateViewController(withIdentifier: "ProfessionVc") as! MyProfessionViewController
                                                  navigationController?.pushViewController(login, animated: true)
                                
                                     print("Edit Profile back")
                                     dismiss(animated: true, completion: nil)
            }
                
        }
      
      
       
        
    }
    @IBAction func backClicked(_ sender: UIButton) {
        sender.pulsate(sender: sender)
        dismiss(animated: true, completion: nil)
        navigationController?.popViewController(animated: true)
    }
}


struct Characters
{
  var name:String
  //  var otherDetails
  var isSelected:Bool! = false
  init(name:String) {
    self.name = name
  }
}
