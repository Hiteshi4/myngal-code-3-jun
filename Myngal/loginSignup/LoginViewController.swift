//
//  LoginViewController.swift
//  Myngal
//
//  Created by SONU on 25/12/19.
//  Copyright © 2019 SONU. All rights reserved.
//

import UIKit
import Alamofire
import Foundation

class LoginViewController:BaseViewController {
    
    
    
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var phoneImage: UIImageView!
    @IBOutlet weak var getOtpBtn: GradientButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // getOtpBtn.isEnabled = false
        phoneNumberTextField.addTarget(self, action: #selector(setUpView(_:)), for: .editingChanged)
        getOtpBtn.bottomGradientColor = .lightGray
        getOtpBtn.topGradientColor = .lightGray
        
    }
  
    @objc func setUpView(_ sender: UIButton){
        
        phoneNumberTextField.layer.borderColor =  UIColor.lightGray.cgColor
        phoneNumberTextField.layer.borderWidth = 1.0
        if phoneNumberTextField.text!.count >= 10{
            phoneImage.tintColor = #colorLiteral(red: 1, green: 0.07843137255, blue: 0, alpha: 1)
            getOtpBtn.bottomGradientColor = #colorLiteral(red: 1, green: 0.7590017915, blue: 0.1346663237, alpha: 1)
            getOtpBtn.topGradientColor = #colorLiteral(red: 1, green: 0.07843137255, blue: 0, alpha: 1)
            getOtpBtn.isEnabled = true
        }else{
           // getOtpBtn.isEnabled = false
            phoneImage.tintColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
            getOtpBtn.bottomGradientColor = .lightGray
            getOtpBtn.topGradientColor = .lightGray
        }
        
    }
    
    
   // MARK:- Action Methods
    
    @IBAction func getOtpClicked(_ sender: UIButton) {
        guard Reachability.isConnectedToNetwork() == true else {
            NotificationCenter.default.post(name: .noInternet, object: nil)
          
            return
        }
        if phoneNumberTextField.text!.count <= 9{
            self.showAlert("Please Enter 10 digit number")
        }else{
        let singleTon = UserParams()
        singleTon.sharedInstance.contact_No = phoneNumberTextField.text!
     
          var view: OtpViewController = self.storyboard?.instantiateViewController(withIdentifier: "OtpVc") as! OtpViewController
        
        self.showActivity()
                     //http://api.msg91.com/api/sendotp.php?authkey=306149Ao9WPWLJ5de0b501&mobile=919999999990&message=Your%20otp%20is%202786&sender=senderid&otp=2786
        let number = phoneNumberTextField.text



                     let updateParticipantURL = URL(string: "http://api.msg91.com/api/sendotp.php?authkey=306149Ao9WPWLJ5de0b501&mobile=\(number!)&sender=MYNGAL&otp_length=6")
              print(updateParticipantURL)
        
                     do {
                  var request = URLRequest(url: updateParticipantURL!)
                    //   request.httpBody = jsonData
                        request.httpMethod = HTTPMethod.get.rawValue
                     //   request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                         // request.setValue("Token 2034faf7a7e76fe8b3581814422b18f52205ef0b", forHTTPHeaderField: "Authorization")z
                        let jsonData = try JSONSerialization.data(withJSONObject: [:], options: .prettyPrinted)
                        AF.request(request).responseJSON { response in
                          // Loader.shared.hideHUDLoader()
                         self.hideActivity()

                            switch response.result {
                            case .success(let _):
                                print ("finish")
                                print(response)
                              
                                view.mobilenumber = self.phoneNumberTextField.text!
                                self.navigationController?.pushViewController(view, animated: true)
                               

//
                            case .failure(let error):
                                 let alert = UIAlertController(title: "", message: "Update Successfull", preferredStyle: UIAlertController.Style.alert)
                                                                          alert.addAction(UIAlertAction(title: "Text.Ok.uppercased()", style: UIAlertAction.Style.default, handler: nil))
                                                                          self.present(alert, animated: true, completion: nil)
                            }
                        }
                     }
                     catch {
                         print(error.localizedDescription)
                     }
        }
    }
    @IBAction func backClicked(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}
