//
//  UplodProfileViewController.swift
//  Myngal
//
//  Created by SONU on 30/12/19.
//  Copyright © 2019 SONU. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import Alamofire
import MapKit
import CoreLocation

class UplodProfileViewController: BaseViewController,UIImagePickerControllerDelegate , UINavigationControllerDelegate  ,UIActionSheetDelegate {

    var userCity:String = ""
    var userCountry:String = ""
    
    
    var imagePicker : UIImagePickerController! = UIImagePickerController()
    var actionSheet : UIActionSheet!           = UIActionSheet()
    var propertyArray:LoginRespnse?

    @IBOutlet weak var profileImage: UIImageView!
     var imageString:String = ""
        var imageStr = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // self.locationManager.requestAlwaysAuthorization()
       // self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
                              locationManager.delegate = self
                              locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                              locationManager.startUpdatingLocation()
                          }
            
                       // For use in foreground
                       
        profileImage.layer.borderWidth = 1.5
        let color = #colorLiteral(red: 1, green: 0.07843137255, blue: 0, alpha: 1)
        profileImage.layer.borderColor = color.cgColor
        profileImage.layer.cornerRadius = profileImage.frame.size.height/2
        profileImage.contentMode = .scaleToFill

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
        
    }
    

    @IBAction func backButtonTapped(_ sender: UIButton) {
        sender.pulsate(sender: sender)
        navigationController?.popViewController(animated: true)
    }
    @IBAction func nextBtnPressed(_ sender: UIButton) {
        
        if userCity != ""
        {
            registerUser()
        }
        else
        {
            let obj = storyboard?.instantiateViewController(withIdentifier: "LocationPermissionVc") as! LocationPermissionViewController
            obj.delegate = self
            navigationController?.pushViewController(obj, animated: true)
        }
        
        
        //        if CLLocationManager.locationServicesEnabled() {
        //            switch CLLocationManager.authorizationStatus() {
        //                case .notDetermined, .restricted, .denied:
        //
        //                    let obj = storyboard?.instantiateViewController(withIdentifier: "LocationPermissionVc") as! LocationPermissionViewController
        //                    obj.delegate = self
        //                    navigationController?.pushViewController(obj, animated: true)
        
        ////                    navigateTo(.Main, .LocationPermissionVc) { (vc) in
        ////
        ////                    }
        //                    print("No access")
        //                case .authorizedAlways, .authorizedWhenInUse:
        //                    if CLLocationManager.locationServicesEnabled() {
        //                        locationManager.delegate = self
        //                        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        //                        locationManager.startUpdatingLocation()
        //                    }
        //     PostUpdateProfile()
        //                    print("Access")
        //                @unknown default:
        //                break
        //            }
        //            } else {
        //                print("Location services are not enabled")
        //        }
        //          var view: MyCommitmentViewController = self.storyboard?.instantiateViewController(withIdentifier: "CommitmentVc") as! MyCommitmentViewController
        //        view.callSignUpAPI(strImg: imageStr)
        // Router.loadMainInterface()
        
        //   navigateTo(.Main, .LocationPermissionVc) { (UIViewController) in
        // PostUpdateProfile()
        //}
    }
    @IBAction func AddImageTapped(_ sender: UIButton) {

        
        let alert:UIAlertController=UIAlertController(title: "Select Image", message: nil, preferredStyle: .actionSheet)
      
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            
            self.openCamera()
        }
        let galleryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openGallery()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in
        }
        imagePicker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(galleryAction)

        alert.addAction(cancelAction)
        //You must add this line then only in iPad it will not crash
        alert.popoverPresentationController?.sourceView = self.view
        self.present(alert, animated: true, completion: nil)
       
    }
    func openCamera()
          {
            if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
              {
                self.imagePicker.sourceType = .camera
                self.imagePicker.allowsEditing = true
                self.imagePicker.delegate = self
                self.present(self.imagePicker, animated: true, completion: nil)
              }
          }
          
          func openGallery()
          {
              
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
                self.imagePicker.allowsEditing = true
                               self.imagePicker.sourceType = .photoLibrary
                               self.imagePicker.delegate = self
                               self.present(self.imagePicker, animated:true, completion: nil);
              }
          }

    //MARK: - ImagePicker Delegate
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
              self.dismiss(animated: true, completion: nil)
          }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
       guard let image = info[UIImagePickerController.InfoKey.originalImage]
        as? UIImage else {return}
            
            
            let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
            profileImage.image = pickedImage
           profileImage.contentMode = .scaleToFill
   
         
        dismiss(animated: true, completion: nil)
        }
    
    //MARK: - ActionSheet Delegate
       
    func registerUser()
      {
          //SwiftLoader.show(title: "Loading...", animated: true)

        if let imageData: Data = (self.profileImage.image!.jpegData(compressionQuality: 1.0) as Data?) {

               self.showActivity()
            let singleTon = UserParams()
            let name = singleTon.sharedInstance.username
            let Email = singleTon.sharedInstance.userEmail
            let number = singleTon.sharedInstance.contact_No
            let intrest = singleTon.sharedInstance.intrest
            let gender  =  singleTon.sharedInstance.Selctedgender
            let dob = singleTon.sharedInstance.dob
            let profession = singleTon.sharedInstance.Professions
            let commitment = singleTon.sharedInstance.Commitments
            let ProfileImage = singleTon.sharedInstance.ProfileImage
            let ReffralCode = singleTon.sharedInstance.ReffralCode

                         let param : [String:Any] = [
                            "name"  : name  as Any,
                             "contact_number"  : number  as Any,
                             "email"   : Email  as Any,
                             "gender"  : gender  as Any,
                             "commitment_type"   : commitment  as Any,
                             "profession"  : profession  as Any,
                             "interest"  : intrest  as Any,
                             "dob"  : dob  as Any,
                             "referal_code" : ReffralCode  as Any,
                            "city": userCity,
                            "country":userCountry

                         ]

              let URL = "\(AppUrls.baseURL+AppUrls.NewuserRegister)"  // @"https://....."
            let headers: HTTPHeaders = [
                               "Content-type": "multipart/form-data",
                           "Accept": "application/json"
                           ]
              AF.upload(multipartFormData: {
                  multipartFormData in
                  multipartFormData.append(imageData, withName: "profile_image", fileName: "abc.jpg", mimeType: "image/jpg")

                  for (key, value) in param {
                      multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                  }

              }, to: URL,method: .post,
              headers: headers)
                    .responseJSON { (resp) in
                        print(resp)
                          self.hideActivity()
                        guard  let dictionary = resp.value as? NSDictionary else{return}
                         let status = dictionary["status"] as? String ?? ""
                         let message = dictionary["message"] as? String ?? ""
                        if status == "true"{
                            if CoreDataManager.shared.entityIsEmpty(entity: "UserDetail")
                            {
                              CoreDataManager.shared.saveDatainUserDetail(data:  dictionary["user_data"] as! NSDictionary)
                                                 }
                                  
                                  if CoreDataManager.shared.entityIsEmpty(entity: "FilterData")
                                  {
                                      CoreDataManager.shared.saveDatainFilterData(data: dictionary["filter_data"] as! NSDictionary)
                                  }
                                  if CoreDataManager.shared.entityIsEmpty(entity: "Data2")
                                  {
                                      CoreDataManager.shared.saveDatainData2(data:  dictionary["data"] as! NSDictionary)
                                     
                                  }
                            Router.loadMainInterface()
                        }
                        else{
                            self.hideActivity()
                            self.showAlert(message)
                        }
                       
                }

          }
      }
    
}

//MARK: For user current location
extension UplodProfileViewController:CLLocationManagerDelegate
{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }

        print("locations = \(locValue.latitude) \(locValue.longitude)")
        getAddressFromLatLon(pdblLatitude: "\(locValue.latitude)", withLongitude: "\(locValue.longitude)")

    }

    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon

        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)


        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]

                if pm.count > 0 {
                    let pm = placemarks![0]
                    print(pm.country)
                    print(pm.locality)
                    print(pm.subLocality)
                    print(pm.thoroughfare)
                    print(pm.postalCode)
                    print(pm.subThoroughfare)
                    var addressString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    self.userCity = pm.locality ?? ""
                    self.userCountry = pm.country ?? ""
                        
                        print(addressString)
              }
        })

    }

}

extension UplodProfileViewController:LocationDelegate
{
    func getLocation(city: String, country: String) {
        self.userCity = city
        self.userCountry = country
        self.registerUser()
    }
    
    
}
