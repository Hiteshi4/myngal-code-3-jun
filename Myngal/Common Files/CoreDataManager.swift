//
//  CoreDataManager.swift
//  Myngal
//
//  Created by Gulshan Agrawal on 27/02/20.
//  Copyright © 2020 SONU. All rights reserved.
//

import Foundation
import UIKit
import CoreData


class CoreDataManager {
    static  let shared : CoreDataManager = CoreDataManager()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
   
    func saveDatainUserDetail(data : NSDictionary){
        let context = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "UserDetail", in: context)
        let newUser = NSManagedObject(entity: entity!, insertInto: context) as! UserDetail
        newUser.aboutMe = data["aboutme"] as? String ?? ""
        newUser.address = data["address"] as? String ?? ""
        newUser.city = data["city"] as? String ?? ""
        newUser.commitment = data["commitment_type"] as? String ?? ""
        newUser.company = data["company"] as? String ?? ""
        newUser.coverImage = data["cover_image"] as? String ?? ""
        newUser.dob = data["dob"] as? String ?? ""
        newUser.education = data["education"] as? String ?? ""
        newUser.gender = data["gender"] as? String ?? ""
        newUser.height = data["height"] as? String ?? ""
        newUser.id = data["id"] as? String ?? ""
        newUser.image1 = data["image1"] as? String ?? ""
        newUser.image2 = data["image2"] as? String ?? ""
        newUser.image3 = data["image3"] as? String ?? ""
        newUser.image4 = data["image4"] as? String ?? ""
        newUser.interest = data["interest"] as? String ?? ""
        newUser.jobTitle = data["job_title"] as? String ?? ""
        newUser.lat = data["latitude"] as? String ?? ""
        newUser.long = data["longitude"] as? String ?? ""
        newUser.profession = data["profession"] as? String ?? ""
        newUser.referBy = data["refer_by"] as? String ?? ""
        newUser.referralCode = data["referal_code"] as? String ?? ""
        newUser.referralPoints = data["referal_points"] as? String ?? ""
        newUser.school = data["school"] as? String ?? ""
        newUser.userID = data["user_id"] as? String ?? ""
        newUser.token = data["token"] as? String ?? ""
        newUser.viewLike = data["view_like"] as? String ?? ""
        do {
            try context.save()
        } catch {
            print("Failed saving")
        }
    }

    
    func fetchDataFromUserDetail()->[UserDetail]? {
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "UserDetail")
        //request.predicate = NSPredicate(format: "age = %@", "12")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            return result as! [UserDetail]
            //                   for data in result as! [LocalNotification] {
            //
            //                 }
            
        } catch {
            
            print("Failed")
        }
        
        return nil
    }
    
  
    func saveDatainFilterData(data : NSDictionary){
           let context = appDelegate.persistentContainer.viewContext
           let entity = NSEntityDescription.entity(forEntityName: "FilterData", in: context)
           let newUser = NSManagedObject(entity: entity!, insertInto: context) as! FilterData
           newUser.userID = data["user_id"] as? String ?? ""
           newUser.maxDistance = data["max_distance"] as? String ?? ""
           newUser.city = data["city"] as? String ?? ""
           newUser.status = data["status"] as? String ?? ""
           newUser.heightRangeStart = data["height_range_start"] as? String ?? ""
           newUser.heightRangeEnd = data["height_range_end"] as? String ?? ""
           newUser.ageRangeStart = data["age_range_start"] as? String ?? ""
           newUser.ageRangeEnd = data["age_range_end"] as? String ?? ""
           newUser.gender = data["gender"] as? String ?? ""
           newUser.id = data["id"] as? String ?? ""
           newUser.lat = data["latitude"] as? String ?? ""
           newUser.long = data["longitude"] as? String ?? ""
           newUser.trash = data["trash"] as? String ?? ""
        newUser.profession = data["profession"] as? String ?? ""
        newUser.commitment = data["commitment_type"] as? String ?? ""
           
           do {
               try context.save()
           } catch {
               print("Failed saving")
           }
       }

       
       func fetchDataFromFilterData()->[FilterData]? {
           let context = appDelegate.persistentContainer.viewContext
           let request = NSFetchRequest<NSFetchRequestResult>(entityName: "FilterData")
           //request.predicate = NSPredicate(format: "age = %@", "12")
           request.returnsObjectsAsFaults = false
           do {
               let result = try context.fetch(request)
               return result as! [FilterData]
               //                   for data in result as! [LocalNotification] {
               //
               //                 }
               
           } catch {
               
               print("Failed")
           }
           
           return nil
       }
       
    
    func saveDatainData2(data : NSDictionary){
           let context = appDelegate.persistentContainer.viewContext
           let entity = NSEntityDescription.entity(forEntityName: "Data2", in: context)
           let newUser = NSManagedObject(entity: entity!, insertInto: context) as! Data2
        newUser.email = data["email"] as? String ?? ""
        newUser.id = Int32(data["id"] as? Int ?? 0)
        newUser.name = data["name"] as? String ?? ""
        newUser.number = data["contact_number"] as? String ?? ""
        newUser.profileImage = data["profile_image"] as? String ?? ""
           do {
               try context.save()
           } catch {
               print("Failed saving")
           }
       }

    func saveDatainAppsetting(data : NSDictionary){
        let context = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "AppSetting", in: context)
        let newUser = NSManagedObject(entity: entity!, insertInto: context) as! AppSetting
     newUser.activeDate = data["active_date"] as? String ?? ""
     newUser.id = Int32(data["id"] as? Int ?? 0)
     newUser.adminMessage = data["admin_message"] as? String ?? ""
     newUser.chatMessageReceived = data["chat_message_received"] as? String ?? ""
     newUser.hide_my_age = data["hide_my_age"] as? String ?? ""
        newUser.makeDistanceInvisible = data["make_distance_invisible"] as? String ?? ""
        newUser.newMatches = data["new_matches"] as? String ?? ""
        newUser.onBeingLiked = data["on_being_liked"] as? String ?? ""
        newUser.onBeingShortlisted = data["on_being_shortlisted"] as? String ?? ""
        newUser.user_id = "\(data["user_id"] as? Int ?? 0)"
        do {
            try context.save()
        } catch {
            print("Failed saving")
        }
    }
    
    func fetchDataFromAppSetting()->[AppSetting]? {
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "AppSetting")
        //request.predicate = NSPredicate(format: "age = %@", "12")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            return result as! [AppSetting]
            //                   for data in result as! [LocalNotification] {
            //
            //                 }
            
        } catch {
            
            print("Failed")
        }
        
        return nil
    }
    
    
       func fetchDataFromData2()->[Data2]? {
           let context = appDelegate.persistentContainer.viewContext
           let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Data2")
           //request.predicate = NSPredicate(format: "age = %@", "12")
           request.returnsObjectsAsFaults = false
           do {
               let result = try context.fetch(request)
               return result as! [Data2]
               //                   for data in result as! [LocalNotification] {
               //
               //                 }
               
           } catch {
               
               print("Failed")
           }
           
           return nil
       }
    
    func deleteDataFromAppSetting(){
         let context = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "AppSetting")

        let result = try? context.fetch(fetchRequest)
           let resultData = result as! [AppSetting]

          for object in resultData {
           
                context.delete(object)
        
          }

           do {
               try context.save()
               print("saved!")
           } catch let error as NSError  {
               print("Could not save \(error), \(error.userInfo)")
           } catch {

           }

    }
    
    
    func deleteDataFromUserDetail(){
         let context = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "UserDetail")

        let result = try? context.fetch(fetchRequest)
           let resultData = result as! [UserDetail]

          for object in resultData {
           
                context.delete(object)
        
          }

           do {
               try context.save()
               print("saved!")
           } catch let error as NSError  {
               print("Could not save \(error), \(error.userInfo)")
           } catch {

           }

    }
    func deleteDataFromData2(){
            let context = appDelegate.persistentContainer.viewContext
           let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Data2")

           let result = try? context.fetch(fetchRequest)
              let resultData = result as! [Data2]

             for object in resultData {
              
                   context.delete(object)
           
             }

              do {
                  try context.save()
                  print("saved!")
              } catch let error as NSError  {
                  print("Could not save \(error), \(error.userInfo)")
              } catch {

              }

       }
    func deleteDataFromFilterData(){
            let context = appDelegate.persistentContainer.viewContext
           let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "FilterData")

           let result = try? context.fetch(fetchRequest)
              let resultData = result as! [FilterData]

             for object in resultData {
              
                   context.delete(object)
           
             }

              do {
                  try context.save()
                  print("saved!")
              } catch let error as NSError  {
                  print("Could not save \(error), \(error.userInfo)")
              } catch {

              }

       }
    func entityIsEmpty(entity: String) -> Bool
    {

      
        let context = appDelegate.persistentContainer.viewContext

        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        var error = NSError()
        
        let results:NSArray? = try? context.fetch(request) as NSArray

        if let res = results
        {
            if res.count == 0
            {
                return true
            }
            else
            {
                return false
            }
        }
        else
        {
            //println("Error: \(error.debugDescription)")
            return true
        }
        do {
            try context.save()
            print("saved!")
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        } catch {

        }

    }
}
