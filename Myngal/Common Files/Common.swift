//
//  Common.swift
//  Myngal
//
//  Created by SONU on 27/12/19.
//  Copyright © 2019 SONU. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration

class Common: NSObject {

    //MARK:- Validate Email address
    func isValidEmail(emailString:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: emailString)
    }

    //MARK:- Validatingg the format of website address, url

    func isValidUrl (urlString: String) -> Bool {
        let urlRegEx = "((?:http|https)://)?(?:www\\.)?[\\w\\d\\-_]+\\.\\w{2,3}(\\.\\w{2})?(/(?<=/)(?:[\\w\\d\\-./_]+)?)?"
        return NSPredicate(format: "SELF MATCHES %@", urlRegEx).evaluate(with: urlString)
    }
    //MARK:- Check for Valid url
    func verifyUrl (urlString: String?) -> Bool {
        if let urlString = urlString {
            if let url  = URL(string: urlString) {
                return UIApplication.shared.canOpenURL(url)
            }
        }
        return false
    }
    
    func setName(name: String) {
            UserDefaults.standard.set( name,forKey: "name")
        }
        func getName() -> String {
            return UserDefaults.standard.string(forKey: "name") ?? ""
        }
        func setUserLocation(location: String) {
            UserDefaults.standard.set( location,forKey: "location")
        }
        func getUserLocation() -> String {
            return UserDefaults.standard.string(forKey: "location") ?? ""
        }
    func setSubscription(status: String) {
              UserDefaults.standard.set( status,forKey: "status")
          }
          func getSubscription() -> String {
              return UserDefaults.standard.string(forKey: "status") ?? ""
          }
       func setSubscriptionMessage(message: String) {
                    UserDefaults.standard.set( message,forKey: "message")
                }
                func geSubscriptionMessage() -> String {
                    return UserDefaults.standard.string(forKey: "message") ?? ""
                }
      
        
        func setEmail(email: String) {
            if email.count != 0{
                UserDefaults.standard.set( email,forKey: "email")
            }
        }
        func getEmail() -> String {
            
            return UserDefaults.standard.string(forKey: "email") ?? ""
        }
        
        func setUserId(userId: Int) {
            if userId != 0 {
                UserDefaults.standard.set( userId,forKey: "id")
            }
        }
        func getUserId() -> Int {
            
            return UserDefaults.standard.integer(forKey: "id")
        }
    func setImage(email: String) {
        if email.count != 0{
            UserDefaults.standard.set( email,forKey: "image")
        }
    }
    func getImage() -> String {
        
        return UserDefaults.standard.string(forKey: "image") ?? ""
    }
    }




struct DateConstants {
    static let dateDefaultFormat = "yyyy-MM-dd HH:mm:ss"
    static let dateFormat = "yyyy-MM-dd"
    static let billdateFormat = "dd/MM/yyyy"
    static let timeFormat = "HH"
    static let fullTimeFormat = "hh:mm a"
}


extension UIViewController
{
    func showToast(message : String){

        
        
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 150, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    
        func performSegueToReturnBack()  {
            if let nav = self.navigationController {
                nav.popViewController(animated: true)
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
    
    
}



extension UIView {
    
    
    func applyViewShadow(shadowOffset : CGSize? , shadowColor : UIColor?, shadowOpacity : Float?) {
        

        if shadowOffset != nil {
            self.layer.shadowOffset = shadowOffset!
        }
        else {
            self.layer.shadowOffset = CGSize(width: 0, height: 0)
        }
        

        if shadowColor != nil {
            self.layer.shadowColor = shadowColor?.cgColor
        } else {
            self.layer.shadowColor = UIColor.clear.cgColor
        }
        
        //For button border width
        if shadowOpacity != nil {
            self.layer.shadowOpacity = shadowOpacity!
        }
        else {
            self.layer.shadowOpacity = 0
        }
        
        self.layer.masksToBounds = false
    }
    
    
    func applyViewCornerRadius(cornerRadius : CGFloat? , borderColor : UIColor?, borderWidth : CGFloat?) {
        
        if cornerRadius != nil {
            self.layer.cornerRadius = cornerRadius!
        }
        else {
            self.layer.cornerRadius = 0
        }
        
        if borderColor != nil {
            self.layer.borderColor = borderColor?.cgColor
        } else {
            self.layer.borderColor = UIColor.clear.cgColor
        }
        
        if borderWidth != nil {
            self.layer.borderWidth = borderWidth!
        }
        else {
            self.layer.borderWidth = 0
        }
    }
    
//    func curruntFirstResponder() -> UIResponder? {
//        
//        if self.isFirstResponder {
//            return self
//        }
//        
//        for view in self.subviews {
//            if let responder  = view.curruntFirstResponder() {
//                return responder
//            }
//        }
//        return nil;
//    }
    
    func fadeOut(duration: TimeInterval) {
        UIView.animate(withDuration: duration) {
            self.alpha = 0.0
        }
    }
    
    func fadeIn(duration: TimeInterval) {
        UIView.animate(withDuration: duration) {
            self.alpha = 1.0
        }
    }
    
    class func viewFromNibName(name: String) -> UIView? {
        let views = Bundle.main.loadNibNamed(name, owner: nil, options: nil)
        return views!.first as? UIView
    }
}
