


//  Router.swift
//  Sekho
//
//  Created by SONU on 21/10/19.
//  Copyright © 2019 SONU. All rights reserved.
//

import Foundation
import UIKit

class Router {
    
    /**
     Present the onboarding controller if needed
     */
    static func loadOnboardingInterface() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let nvc = storyboard.instantiateViewController(withIdentifier: "PreLoginVc") as? PreLoginVc else {
            return
        }
        let navigationController = UINavigationController(rootViewController: nvc)

        UIApplication.shared.windows.first?.rootViewController = navigationController
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    
    
////        UIView.transition(with: self.window()!, duration: 0.5, options: .transitionCrossDissolve, animations: { () -> Void in
//            self.window()?.rootViewController = nvc
//        })
    }
  
    /**
     Present the main interface
     */
   static func loadMainInterface() {
    
           guard let rootVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeVc") as? HomeViewController else {
            print("aborting the process")
                          return
                      }
                      let navigationController = UINavigationController(rootViewController: rootVC)

                      UIApplication.shared.windows.first?.rootViewController = navigationController
                      UIApplication.shared.windows.first?.makeKeyAndVisible()
       }
       
       static private func window() -> UIWindow? {
           return AppDelegate.shared?.window
       }
}


